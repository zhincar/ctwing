package com.link510.ctwing.plugin.nbiot.controller;

import com.link510.ctwing.web.framework.controller.BaseApiController;
import com.link510.ctwing.web.framework.controller.BaseNBIoTController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "NBIoTHomeController")
public class HomeController extends BaseNBIoTController {

    @RequestMapping(value = "index")
    public String index() {
        return "hello,nbiot";
    }


}
