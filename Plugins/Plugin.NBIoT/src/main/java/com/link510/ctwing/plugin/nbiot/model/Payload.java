package com.link510.ctwing.plugin.nbiot.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Copyright 2019 bejson.com
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payload implements Serializable {


    @JsonProperty(value = "APPdata")
    private String appdata = "";


}