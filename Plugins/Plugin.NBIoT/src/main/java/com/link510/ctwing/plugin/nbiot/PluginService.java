package com.link510.ctwing.plugin.nbiot;

import com.link510.ctwing.core.plugin.interface2.IParserPlugin;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


/**
 * @author cqnews
 */
@Component(value = "PluginNBIoTService")
public class PluginService implements IParserPlugin {


    @Override
    public String topic() {
        return "nbiot";
    }

    @Override
    public Integer pluginId() {
        return 1;
    }

    /**
     * 发送广播
     *
     * @param msg 消息内容
     */
    @Override
    public void broadcast(String msg) {

    }

    /**
     * 发送消息
     *
     * @param deviceSN 通道id
     * @param msg      消息内容
     */
    @Override
    public void message(String deviceSN, String msg) {

    }

    /**
     * 名称
     *
     * @return
     */
    @Override
    public String name() {
        return "ODeviceInGateway";
    }

    /**
     * 协议描述
     *
     * @return
     */
    @Override
    public String desc() {
        return "英卡电子NBIoT网关";
    }

    @Override
    public String version() {
        return "0.1";
    }


    /**
     * 初始化插件
     */
    @PostConstruct
    @Override
    public void initPlugin() {


    }

}
