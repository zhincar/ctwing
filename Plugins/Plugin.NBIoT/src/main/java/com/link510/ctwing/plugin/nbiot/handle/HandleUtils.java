package com.link510.ctwing.plugin.nbiot.handle;


import com.alibaba.fastjson.JSON;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Streams;
import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.domain.message.ProtocolRecord;
import com.link510.ctwing.core.enums.message.MsgLevel;
import com.link510.ctwing.core.helper.ByteHelper;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.services.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;


@Service(value = "HandleUtils")
public class HandleUtils {


    private static final String FULL_DATA_REGEX = "^[A-Za-z0-9]{0,26}(594B4E42)[A-Za-z0-9]+";

    @Autowired
    private Logs logs;

    @Autowired
    private Messages messages;

    /**
     * 数据解析
     *
     * @param token
     * @param s     十六进制串
     */
    public void hand(String token, String s) {


        try {

            if (!s.matches(FULL_DATA_REGEX)) {
                return;
            }


            String data = s.substring(s.indexOf("594B4E42"));

            System.out.println("数据原串:" + data);

            Map<String, Object> infoParams = Maps.newLinkedHashMap();

            String infoArea = StringHelper.substring(data, 0, 32);

            if (Strings.isNullOrEmpty(infoArea)) {
                System.out.println("信息域解析失败");
                return;
            }


            infoParams.put("infoPacketHeader", StringHelper.substring(infoArea, 0, 8)); //0-4
            infoParams.put("infoPacketSender", StringHelper.substring(infoArea, 8, 2)); //
            infoParams.put("infoPacketReceiver", StringHelper.substring(infoArea, 10, 2));
            infoParams.put("infoPacketLen", Integer.parseInt(StringHelper.substring(infoArea, 12, 4), 16));
            infoParams.put("infoCrcType", StringHelper.substring(infoArea, 16, 2));
            infoParams.put("infoCrcValue", StringHelper.substring(infoArea, 18, 8));
            infoParams.put("infoBatteryVoltage", StringHelper.substring(infoArea, 26, 2));
            infoParams.put("infoCSQ", StringHelper.substring(infoArea, 28, 2));
            infoParams.put("infoMsgRsp", StringHelper.substring(infoArea, 30, 2));


            int infoPacketLen = Integer.parseInt(infoParams.get("infoPacketLen").toString()) * 2;


            System.out.println("data:" + data);
            System.out.println("infoPacketLen:" + infoPacketLen);
            System.out.println("infoArea.length():" + infoArea.length());

            String dataArea = data.substring(infoArea.length(), infoPacketLen);


            if (Strings.isNullOrEmpty(dataArea)) {
                System.out.println("数据域解析失败");
                return;
            }

            for (String key : infoParams.keySet()) {
                System.out.println(key + ":" + infoParams.getOrDefault(key, ""));
            }

            System.out.println("数据域:" + dataArea);

            Map<String, Object> dataParams = Maps.newLinkedHashMap();


            String deviceSN = String.valueOf(Integer.parseInt(StringHelper.substring(dataArea, 0, 8), 16));

            dataParams.put("dataTargetId", deviceSN);
            dataParams.put("dataEncryptionType", StringHelper.substring(dataArea, 8, 2));
            dataParams.put("dataEncryptionValue", StringHelper.substring(dataArea, 10, 2));
            dataParams.put("dataConditionCode", StringHelper.substring(dataArea, 12, 4));
            dataParams.put("dataLoadCommand", StringHelper.substring(dataArea, 16, 4));

            String dataLoadCommand = StringHelper.substring(dataArea, 16, 4).toUpperCase();
            dataParams.put("dataLoadData", StringHelper.substring(dataArea, 20));

            String dataLoadData = StringHelper.substring(dataArea, 20);

            for (String key : dataParams.keySet()) {
                System.out.println(key + ":" + dataParams.getOrDefault(key, ""));
            }

            String methName = "handFire" + dataLoadCommand;

            System.out.println("methName:" + methName);

            Method method = ReflectionUtils.findMethod(this.getClass(), methName, String.class, String.class, String.class);


            Object[] params = new Object[3];
            params[0] = deviceSN;
            params[1] = token;
            params[2] = dataLoadData;

            if (method != null) {
                ReflectionUtils.invokeMethod(method, this, params);
            }

        } catch (Exception ex) {
            logs.write(ex, "数据解析");
            ex.printStackTrace();
        }

    }

    /**
     * 解析服务   ---火灾探测器   ---00A0-设备注册-上电报文(ASCII编码)
     */
    public void handFire00A0(String deviceSN, String token, String dataLoadData) throws ParseException {

        try {

            byte[] bytes = ByteHelper.hexStringToByte(dataLoadData);

            if (bytes.length <= 0) {
                return;
            }

            String ascii = new String(bytes, Charsets.US_ASCII);
            String[] split = ascii.split(",");

            List<ProtocolRecord> recordList = Lists.newArrayList();

            recordList.add(new ProtocolRecord("deviceModel", split[0]));
            recordList.add(new ProtocolRecord("hardwareVersion", split[1]));
            recordList.add(new ProtocolRecord("softwareVersion", split[2]));
            recordList.add(new ProtocolRecord("nbModel", split[3]));
            recordList.add(new ProtocolRecord("imei", split[4]));
            recordList.add(new ProtocolRecord("imsi", split[5]));
            recordList.add(new ProtocolRecord("iccid", split[6]));
            recordList.add(new ProtocolRecord("firmwareVersion", split[7]));
            recordList.add(new ProtocolRecord("nuestatsMsg", split[8]));
            recordList.add(new ProtocolRecord("bsDate", split[9]));
            recordList.add(new ProtocolRecord("bsTime", split[10]));

            createMessage(deviceSN, token, "00A0", "设备注册", MsgLevel.Heart.getLevel(), dataLoadData, recordList);

        } catch (Exception ex) {
            logs.write(ex, "电子树牌上电报文解析异常，请检查");
        }
    }


    /**
     * 解析服务   ---火灾探测器   ---00A1-设备运行上传-模式切换报文
     */
    public void handFire00A1(String deviceSN, String token, String dataLoadData) throws ParseException {

        try {
            String dataTime = dataLoadData.substring(0, 14);
            dataTime = trans2timestamp(dataTime);
            List<ProtocolRecord> recordList = Lists.newArrayList();
            recordList.add(new ProtocolRecord("dataTime", dataTime));
            recordList.add(new ProtocolRecord("targetModel", Integer.parseInt(dataLoadData.substring(14, 16), 16) + ""));
            recordList.add(new ProtocolRecord("switchSource", Integer.parseInt(dataLoadData.substring(16, 18), 16) + ""));
            recordList.add(new ProtocolRecord("thresholdVoltage", Integer.parseInt(dataLoadData.substring(18, 22), 16) + ""));
            recordList.add(new ProtocolRecord("curBatteryVoltage", Integer.parseInt(dataLoadData.substring(22, 26), 16) + ""));
            recordList.add(new ProtocolRecord("curCapacitanceVoltage", Integer.parseInt(dataLoadData.substring(26, 30), 16) + ""));


            createMessage(deviceSN, token, "00A1", "设备运行上传", MsgLevel.Heart.getLevel(), dataLoadData, recordList);

        } catch (Exception ex) {

        }
    }

    /**
     * 解析服务   ---火灾探测器   ---00A1-设备运行上传-模式切换报文
     */
    public void handFire00B0(String deviceSN, String token, String dataLoadData) throws ParseException {

        try {

            String dataTime = dataLoadData.substring(0, 14);
            dataTime = trans2timestamp(dataTime);
            List<ProtocolRecord> recordList = Lists.newArrayList();
            recordList.add(new ProtocolRecord("dataTime", dataTime));
            recordList.add(new ProtocolRecord("targetModel", Integer.parseInt(dataLoadData.substring(14, 16), 16) + ""));
            recordList.add(new ProtocolRecord("switchSource", Integer.parseInt(dataLoadData.substring(16, 18), 16) + ""));
            recordList.add(new ProtocolRecord("thresholdVoltage", Integer.parseInt(dataLoadData.substring(18, 22), 16) + ""));
            recordList.add(new ProtocolRecord("curBatteryVoltage", Integer.parseInt(dataLoadData.substring(22, 26), 16) + ""));
            recordList.add(new ProtocolRecord("curCapacitanceVoltage", Integer.parseInt(dataLoadData.substring(26, 30), 16) + ""));

            createMessage(deviceSN, token, "00B0", "火灾探测器", MsgLevel.Warning.getLevel(), dataLoadData, recordList);

        } catch (Exception ex) {

        }
    }

    /**
     * TODO --未设计
     * 解析服务   ---火灾探测器   ---0000-最新数据上传-
     */
    private void hand0000(String dataloadData, String deviceSN) {
        List<Map<String, String>> result = Lists.newArrayList();
        Map<String, String> resMap = Maps.newHashMap();

    }

    /**
     * 解析服务   ---火灾探测器   ---0010-历史数据上传-心跳报文
     */
    public void handFire0010(String deviceSN, String token, String dataLoadData) throws ParseException {

        int i = 0;
        int hex = 16;
        String dataTime = dataLoadData.substring(0, 14);
        dataTime = trans2timestamp(dataTime);
        String status = Integer.toBinaryString(Integer.parseInt(dataLoadData.substring(32, 36), 16));
        StringBuilder sb = new StringBuilder();
        sb.append(status);
        while (sb.length() < hex) {
            sb.append("0");
        }
        status = sb.toString();
        List<ProtocolRecord> recordList = Lists.newArrayList();
        recordList.add(new ProtocolRecord("dataTime", dataTime));
        recordList.add(new ProtocolRecord("resetTime", Integer.parseInt(dataLoadData.substring(14, 18), 16) + ""));
        recordList.add(new ProtocolRecord("ECL", Integer.parseInt(dataLoadData.substring(18, 20), 16) + ""));
        recordList.add(new ProtocolRecord("SNR", Integer.parseInt(dataLoadData.substring(20, 24), 16) + ""));
        recordList.add(new ProtocolRecord("EARFCN", Integer.parseInt(dataLoadData.substring(24, 28), 16) + ""));
        recordList.add(new ProtocolRecord("PCI", Integer.parseInt(dataLoadData.substring(28, 32), 16) + ""));
        recordList.add(new ProtocolRecord("status", status));
        recordList.add(new ProtocolRecord("curModel", Integer.parseInt(dataLoadData.substring(36, 38), 16) + ""));
        recordList.add(new ProtocolRecord("imgSize", Integer.parseInt(dataLoadData.substring(38, 40), 16) + ""));
        recordList.add(new ProtocolRecord("imgCompression", Integer.parseInt(dataLoadData.substring(40, 42), 16) + ""));
        recordList.add(new ProtocolRecord("lockChannel", Integer.parseInt(dataLoadData.substring(42, 46), 16) + ""));
        recordList.add(new ProtocolRecord("lockCell", Integer.parseInt(dataLoadData.substring(46, 50), 16) + ""));
        recordList.add(new ProtocolRecord("ip", Integer.parseInt(dataLoadData.substring(50, 58), 16) + ""));
        recordList.add(new ProtocolRecord("port", Integer.parseInt(dataLoadData.substring(58, 62), 16) + ""));
        recordList.add(new ProtocolRecord("waveEnble", Integer.parseInt(dataLoadData.substring(62, 64), 16) + ""));
        recordList.add(new ProtocolRecord("photoEnble", Integer.parseInt(dataLoadData.substring(64, 66), 16) + ""));
        int hisNum = Integer.parseInt(dataLoadData.substring(66, 68), 16);
        recordList.add(new ProtocolRecord("hisNum", hisNum + ""));

        List<Map<String, String>> hisDataList = Lists.newArrayList();

        for (int k = 0; k < hisNum; k++) {
            Map<String, String> res = Maps.newHashMap();
            res.put("temperature", (Integer.parseInt(dataLoadData.substring(i * 12 + 68, i * 12 + 72), 16) - 400) * 0.1f + "");
            res.put("humidity", Integer.parseInt(dataLoadData.substring(i * 12 + 72, i * 12 + 76), 16) * 0.1f + "");
            res.put("capacitance", Integer.parseInt(dataLoadData.substring(i * 12 + 76, i * 12 + 80), 16) + "");
            res.put("battery", Integer.parseInt(dataLoadData.substring(i * 12 + 80, i * 12 + 84), 16) + "");
            res.put("test1", Integer.parseInt(dataLoadData.substring(i * 12 + 84, i * 12 + 88), 16) + "");
            res.put("test2", Integer.parseInt(dataLoadData.substring(i * 12 + 88, i * 12 + 92), 16) + "");
            hisDataList.add(res);
        }

        recordList.add(new ProtocolRecord("hisData", JSON.toJSONString(hisDataList)));
        createMessage(deviceSN, token, "0010", "心跳报文", MsgLevel.Warning.getLevel(), dataLoadData, recordList);

    }


    /**
     * 解析服务   ---火灾探测器   ---0011-配置数据回复上传
     */
    public void handFire0011(String deviceSN, String token, String dataLoadData) throws ParseException {

        List<ProtocolRecord> recordList = Lists.newArrayList();
        String dataTime = dataLoadData.substring(0, 14);
        dataTime = trans2timestamp(dataTime);
        recordList.add(new ProtocolRecord("dataTime", dataTime));
        recordList.add(new ProtocolRecord("curModel", Integer.parseInt(dataLoadData.substring(14, 16), 16) + ""));
        recordList.add(new ProtocolRecord("resetTime", Integer.parseInt(dataLoadData.substring(16, 20), 16) + ""));
        recordList.add(new ProtocolRecord("ECL", Integer.parseInt(dataLoadData.substring(20, 22), 16) + ""));
        recordList.add(new ProtocolRecord("SNR", Integer.parseInt(dataLoadData.substring(22, 26), 16) + ""));
        recordList.add(new ProtocolRecord("EARFCN", Integer.parseInt(dataLoadData.substring(26, 30), 16) + ""));
        recordList.add(new ProtocolRecord("PCI", Integer.parseInt(dataLoadData.substring(30, 34), 16) + ""));
        recordList.add(new ProtocolRecord("imgSize", Integer.parseInt(dataLoadData.substring(34, 36), 16) + ""));
        recordList.add(new ProtocolRecord("imgCompression", Integer.parseInt(dataLoadData.substring(36, 38), 16) + ""));
        recordList.add(new ProtocolRecord("lockChannel", Integer.parseInt(dataLoadData.substring(38, 42), 16) + ""));
        recordList.add(new ProtocolRecord("lockCell", Integer.parseInt(dataLoadData.substring(42, 46), 16) + ""));
        recordList.add(new ProtocolRecord("ip", Integer.parseInt(dataLoadData.substring(46, 54), 16) + ""));
        recordList.add(new ProtocolRecord("port", Integer.parseInt(dataLoadData.substring(54, 58), 16) + ""));
        recordList.add(new ProtocolRecord("waveEnable", Integer.parseInt(dataLoadData.substring(58, 60), 16) + ""));
        recordList.add(new ProtocolRecord("firePhotoEnable", Integer.parseInt(dataLoadData.substring(60, 62), 16) + ""));
        recordList.add(new ProtocolRecord("guardPhotoEnable", Integer.parseInt(dataLoadData.substring(62, 64), 16) + ""));
        recordList.add(new ProtocolRecord("insureEnable", Integer.parseInt(dataLoadData.substring(64, 66), 16) + ""));

        createMessage(deviceSN, token, "0011", "配置数据回复", MsgLevel.Warning.getLevel(), dataLoadData, recordList);

    }


    /**
     * 解析服务   ---火灾探测器   ---00A1-设备运行上传-模式切换报文
     */
    public void handFire0020(String deviceSN, String token, String dataLoadData) throws ParseException {

        List<ProtocolRecord> recordList = Lists.newArrayList();
        String shotTime = dataLoadData.substring(0, 14);
        shotTime = trans2timestamp(shotTime);
        recordList.add(new ProtocolRecord("deviceSN", deviceSN));
        recordList.add(new ProtocolRecord("shotTime", shotTime));
        recordList.add(new ProtocolRecord("packageTotalNum", Integer.parseInt(dataLoadData.substring(14, 18), 16) + ""));
        recordList.add(new ProtocolRecord("packageSortNum", Integer.parseInt(dataLoadData.substring(18, 22), 16) + ""));
        recordList.add(new ProtocolRecord("imgData", dataLoadData.substring(22, dataLoadData.length())));

        createMessage(deviceSN, token, "0020", "切换报文", MsgLevel.Warning.getLevel(), dataLoadData, recordList);

        //FIXME 暂丢弃单条消息，只保存合成后的消息

    }


    /**
     * 解析服务   ---火灾探测器   ---0021-传感数据传-火焰波形
     */
    public void hand0021(String deviceSN, String token, String dataLoadData) throws ParseException {

        // Map<String, String> paramMap = Maps.newHashMap();

        List<ProtocolRecord> recordList = Lists.newArrayList();

        String dataTime = dataLoadData.substring(0, 14);
        dataTime = trans2timestamp(dataTime);


        recordList.add(new ProtocolRecord("dataTime", dataTime + "上传时间"));

        int channelNumber = Integer.parseInt(dataLoadData.substring(14, 16), 16);

        recordList.add(new ProtocolRecord("channelNum", channelNumber + ""));
        int packageNumber = Integer.parseInt(dataLoadData.substring(16, 20), 16);
        recordList.add(new ProtocolRecord("packageNum", packageNumber + ""));
        int sortNumber = Integer.parseInt(dataLoadData.substring(20, 24), 16);
        recordList.add(new ProtocolRecord("sortNum", sortNumber + ""));
        String waveData = Joiner.on(",").join(Streams.stream(Splitter.fixedLength(4).split(dataLoadData.substring(24, dataLoadData.length() - 1)).iterator()).map(i -> Integer.valueOf(i, 16)).collect(toList()));
        recordList.add(new ProtocolRecord("waveData", waveData));
        recordList.add(new ProtocolRecord("DataTargetId", deviceSN));


        createMessage(deviceSN, token, "0021", "火焰波形", MsgLevel.Warning.getLevel(), dataLoadData, recordList);
    }


    /**
     * 7字节hexStr to 时间戳
     *
     * @param dataTime 时间解析
     * @return
     */
    private String trans2timestamp(String dataTime) throws ParseException {
        String year = Integer.parseInt(dataTime.substring(0, 4), 16) + "";
        String month = Integer.parseInt(dataTime.substring(4, 6), 16) > 10 ? Integer.parseInt(dataTime.substring(4, 6), 16) + "" : "0" + Integer.parseInt(dataTime.substring(4, 6), 16);
        String day = Integer.parseInt(dataTime.substring(6, 8), 16) > 10 ? Integer.parseInt(dataTime.substring(6, 8), 16) + "" : "0" + Integer.parseInt(dataTime.substring(6, 8), 16);
        String hour = Integer.parseInt(dataTime.substring(8, 10), 16) > 10 ? Integer.parseInt(dataTime.substring(8, 10), 16) + "" : "0" + Integer.parseInt(dataTime.substring(8, 10), 16);
        String min = Integer.parseInt(dataTime.substring(10, 12), 16) > 10 ? Integer.parseInt(dataTime.substring(10, 12), 16) + "" : "0" + Integer.parseInt(dataTime.substring(10, 12), 16);
        String sec = Integer.parseInt(dataTime.substring(12, 14), 16) > 10 ? Integer.parseInt(dataTime.substring(12, 14), 16) + "" : "0" + Integer.parseInt(dataTime.substring(12, 14), 16);
        dataTime = year + month + day + hour + min + sec;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        dataTime = simpleDateFormat.parse(dataTime).getTime() / 1000 + "";
        return dataTime;
    }


    /**
     * 创建消息
     *
     * @param deviceSN   设备编号
     * @param protocol   协议信息
     * @param protodesc  协议描述
     * @param level      消息等级
     * @param dataStr    消息原串
     * @param recordList 解析串
     * @return
     */
    private boolean createMessage(String deviceSN, String token, String protocol, String protodesc, Integer level, String dataStr, List<ProtocolRecord> recordList) {

        try {

            MessageInfo messageInfo = new MessageInfo();
            messageInfo.setDeviceSN(deviceSN);
            messageInfo.setToken(token);
            messageInfo.setProductId(0);
            messageInfo.setProductType(0);

            messageInfo.setProtocol(protocol);
            messageInfo.setProtodesc(protodesc);

            messageInfo.setMsgLevel(level);

            messageInfo.setData(dataStr);
            messageInfo.setContent(dataStr);

            messageInfo.setTimestamp(UnixTimeHelper.getUnixTimeStamp());

            messageInfo.setState(0);

            return messages.createMessageAndRecordList(messageInfo, recordList);

        } catch (Exception ex) {
            logs.write(ex, "创建消息");
        }


        return false;
    }

    public void main(String[] args) {
        String s3 = "01FD1300320000040019560041594B4E4202FE002903196F1C25240D0000014C3A0000000500A107E305150B1F0702010FA00E260D45";

        hand("1", s3);
    }
}
