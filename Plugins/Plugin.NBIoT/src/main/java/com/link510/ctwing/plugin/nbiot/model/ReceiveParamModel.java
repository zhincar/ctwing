package com.link510.ctwing.plugin.nbiot.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReceiveParamModel implements Serializable {

    private static final long serialVersionUID = 7657867788709868628L;
    /**
     * 租户ID
     */
    private String tenantId = "";

    /**
     * 产品ID
     */
    private String productId = "";

    /**
     * 设备ID
     */
    private String deviceId = "";

    /**
     * 消息类型=dataReport
     */
    private String messageType = "";

    /**
     * NB终端设备识别号
     */
    @JsonProperty(value = "IMEI")
    private String imei = "";

    /**
     * NB终端sim卡标识
     */
    @JsonProperty(value = "IMSI")
    private String imsi = "";

    /**
     * 设备标识
     */
    private String deviceType = "";

    /**
     * 数据上报主题
     */
    private String topic = "";

    /**
     * 合作伙伴ID
     */
    private String assocAssetId = "";

    /**
     * 时间戳
     */
    private long timestamp = 0;

    /**
     * 上行报文序号
     */
    private String upPacketSN = "";

    /**
     * 数据上报报文序号
     */
    private String upDataSN = "";

    /**
     * 服务ID
     */
    private String datasetId = "";

    private String serviceId = "";

    /**
     * 协议类型
     */
    private String protocol = "";

    /**
     * 消息负载
     */
    private Payload payload;
}
