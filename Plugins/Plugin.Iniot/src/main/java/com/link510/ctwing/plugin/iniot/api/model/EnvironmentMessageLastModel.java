package com.link510.ctwing.plugin.iniot.api.model;

import com.link510.ctwing.core.domain.producrs.EnvironmentMessageInfo;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 设备最后一包数据
 *
 * @author cqnews
 */
@Data
@Builder
public class EnvironmentMessageLastModel implements Serializable {

    private static final long serialVersionUID = 3219303291192147920L;

    /**
     * 设备编号
     */
    @Builder.Default
    private String deviceSN = "";

    /**
     * 环境信息
     */
    private EnvironmentMessageInfo environmentMessageInfo;
}
