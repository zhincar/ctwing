package com.link510.ctwing.plugin.iniot.api.controller;


import com.link510.ctwing.plugin.iniot.api.controller.BaseIniotApiController;
import com.link510.ctwing.services.IniotUtils;
import com.link510.iniot.sdk.domain.subscribes.SubscribeOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.link510.ctwing.core.errors.SateCollect.SUCCESS;

@RestController(value = "SubscribeController")
public class SubscribeController extends BaseIniotApiController {

    @Autowired
    private IniotUtils iniotUtils;

    /**
     * 消息订阅列表
     *
     * @return 消息订阅列表
     */
    @RequestMapping(value = "subscribe/list")
    public String list() {

        try {

            List<SubscribeOutDTO> list = iniotUtils.getSubscribeManagement().list();

            if (list == null || list.size() <= 0) {
                return JsonView("没有更多的消息列表");
            }

            return JsonView(SUCCESS, "消息列表获取成功");

        } catch (Exception ex) {
            logs.write(ex, "消息订阅列表");
        }

        return JsonView("没有更多的消息列表");
    }

//    /**
//     * 消息订阅申请
//     *
//     * @return
//     */
//    @RequestMapping(value = "subscribe/send")
//    public String send() {
//
//        Map<String, Object> params = Maps.newHashMap();
//        params.put("title", "测试消息订阅");
//        params.put("productId", 1);
//        params.put("protocol", "0XB0");
//        params.put("httpUrl", "http://192.168.1.188:8026/receive");
//
//        return iniotUtils.dpGet("subscribe/send", params);
//    }
//
//    /**
//     * 取消消息订阅
//     *
//     * @return
//     */
//    @RequestMapping(value = "subscribe/cancel")
//    public String cannel() {
//
//        Map<String, Object> params = Maps.newHashMap();
//        params.put("productId", 1);
//        params.put("protocol", "0XB0");
//
//        return iniotUtils.dpGet("subscribe/cancel", params);
//    }
//
//    /**
//     * 测试消息订阅
//     *
//     * @return
//     */
//    @RequestMapping(value = "subscribe/test")
//    public String test() {
//
//        Map<String, Object> params = new HashMap<>();
//        params.put("deviceSN", "75001");
//        params.put("protocol", "0000");
//
//        return iniotUtils.dpGet("subscribe/test", params);
//    }


}
