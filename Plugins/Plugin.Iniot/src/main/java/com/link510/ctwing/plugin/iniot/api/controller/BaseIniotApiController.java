package com.link510.ctwing.plugin.iniot.api.controller;

import com.link510.ctwing.web.framework.controller.BaseApiController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 机器人API主类
 */
@RequestMapping(value = "api/iniot")
public class BaseIniotApiController extends BaseApiController {


}
