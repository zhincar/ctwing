package com.link510.ctwing.plugin.iniot.api.controller;


import com.link510.ctwing.core.domain.producrs.EnvironmentMessageInfo;
import com.link510.ctwing.core.errors.SateCollect;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.plugin.iniot.api.model.EnvironmentMessageLastModel;
import com.link510.ctwing.services.IniotUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.NotSupportedException;


/**
 *
 */
@RestController(value = "CtwingEnvironmentController")
public class EnvironmentController extends BaseIniotApiController {

    @Autowired
    protected Logs logs;

    @Autowired
    private IniotUtils iniotUtils;


    /**
     * 获取最后一包信息
     *
     * @return String
     */
    @RequestMapping(value = "environment/last")
    public String list(@RequestParam(defaultValue = "75001") String deviceSN) {

        try {

            EnvironmentMessageInfo environmentMessageInfo = iniotUtils.lastEnvironmentMessage(deviceSN);

            EnvironmentMessageLastModel model = EnvironmentMessageLastModel.builder()
                    .deviceSN(deviceSN)
                    .environmentMessageInfo(environmentMessageInfo)
                    .build();

            return JsonView(SateCollect.SUCCESS, model, "获取最后一包信息");

        } catch (NotSupportedException e) {
            logs.write(e, "设备不支持");
        }

        return JsonView("数据加载失败");

    }


}
