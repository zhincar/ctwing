package com.link510.ctwing.plugin.iniot.api.controller;

import com.link510.ctwing.plugin.iniot.api.controller.BaseIniotApiController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController extends BaseIniotApiController {

    @RequestMapping(value = "/iniot")
    public String index() {
        return "英卡电子欢迎您!";
    }
}
