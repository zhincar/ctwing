package com.link510.ctwing.plugin.iniot.controller;


import com.google.common.collect.Lists;
import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.domain.message.ProtocolRecord;
import com.link510.ctwing.plugin.iniot.services.MessageUtils;
import com.link510.iniot.sdk.model.BaseReceiveModel;
import com.link510.iniot.sdk.model.NBIoTAlarmMessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


@RestController(value = "PluginIniotFlameReceiveReceiveController")
public class FlameReceiveReceiveController extends BaseIniotReceiveController {

    @Autowired
    private MessageUtils messageUtils;

    private Lock lock = new ReentrantLock();


    /**
     * 环境数据订阅
     *
     * @return String
     */
    /**
     * POST接收
     *
     * @return String
     */
    @RequestMapping(value = "alarmb0")
    public String nbiotAlarmB0(NBIoTAlarmMessageModel model) {

        lock.lock();

        try {

            if (model == null) {
                return JsonView("消息接收异常");
            }

            MessageInfo messageInfo = new MessageInfo();

            messageInfo.setToken(model.getToken());
            messageInfo.setDeviceSN(model.getDevicesn());
            messageInfo.setTimestamp(model.getTimestamp());
            messageInfo.setProtocol(model.getProtocol());

            List<BaseReceiveModel.ProtocolRecordModel> list2 = model.toProtocolRecord();

            List<ProtocolRecord> list = Lists.newArrayList();

            for (BaseReceiveModel.ProtocolRecordModel recordModel : list2) {
                list.add(new ProtocolRecord(recordModel.getKey(), recordModel.getValue(), recordModel.getUnit(), recordModel.getDesc()));
            }

            messageInfo = messageUtils.createMessageAndRecordList(messageInfo, list);

            if (messageInfo == null) {
                return JsonView("数据接收异常");
            }

            return JsonView("数据接收成功");

        } catch (Exception ex) {
            logs.write(ex, "数据接收异常");
        } finally {
            lock.unlock();
        }

        return JsonView("数据接收异常");

    }


}
