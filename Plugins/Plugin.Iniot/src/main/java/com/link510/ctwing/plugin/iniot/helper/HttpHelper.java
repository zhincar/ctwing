/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.link510.ctwing.plugin.iniot.helper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by cqnews on 2017/12/8.
 */
public class HttpHelper {


    //region 数据请求

    /**
     * 发送 GET 请求（HTTP），不带输入数据
     *
     * @param url
     * @return
     */
    public static String doGet(String url) {
        return doGet(url, new HashMap<String, Object>(), new HashMap<String, String>());
    }

    /**
     * 发送 GET 请求（HTTP），不带输入数据
     *
     * @param url
     * @return
     */
    public static String doGet(String url, Map<String, String> headers) {
        return doGet(url, new HashMap<String, Object>(), headers);
    }


    /**
     * 发送 GET 请求（HTTP），带输入数据
     *
     * @param url    请求地址
     * @param params 输入参数
     * @return
     */
    public static String doGet(String url, Map<String, Object> params, Map<String, String> headers) {

        String apiUrl = url;
        StringBuffer param = new StringBuffer();
        int i = 0;
        if (params != null && params.size() >= 1) {

            Set<String> arr = params.keySet();
            for (String key : arr) {
                if (i == 0) {
                    param.append("?");
                } else {
                    param.append("&");
                }
                param.append(key).append("=").append(params.get(key));
                i++;
            }
            apiUrl += param;
        }
        String result = null;

        CloseableHttpClient httpClient = HttpClients.createDefault();

        try {

            HttpGet httpGet = new HttpGet(apiUrl);

            if (headers != null && headers.size() >= 1) {
                for (String key : headers.keySet()) {
                    httpGet.setHeader(key, headers.get(key));
                }
            }

            System.out.println("HttpGet:" + JSON.toJSONString(httpGet.getAllHeaders()));

            CloseableHttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == 200) {


                HttpEntity entity = response.getEntity();
                result = EntityUtils.toString(entity, "utf-8");

            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();//释放资源
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;

    }


    @Test
    public void test() {
        String s = doGet("https://cdn.cqwo.com/");
        System.out.println(s);
    }


    //endregion
}
