package com.link510.ctwing.plugin.iniot.services;


import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.domain.message.ProtocolRecord;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.services.Messages;
import com.link510.iniot.sdk.model.BaseReceiveModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "PluginIniotCustomMessageUtils")
public class MessageUtils {

    @Autowired
    private Messages messages;

    @Autowired
    private Logs logs;

    /**
     * model转消息
     *
     * @return List<MessageRecordInfo>
     */
    public MessageInfo createMessageAndRecordList(MessageInfo messageInfo, List<ProtocolRecord> recordInfoList) {

        try {
//            MessageInfo messageInfo = new MessageInfo();
//
//            messageInfo.setToken(model.getToken());
//            messageInfo.setDeviceSN(model.getDevicesn());
//            messageInfo.setTimestamp(model.getTimestamp());
//            messageInfo.setProtocol(model.getProtocol());
//
//            List<ProtocolRecord> list = Lists.newArrayList();

            if (!messages.createMessageAndRecordList(messageInfo, recordInfoList)) {
                return messageInfo;
            }


        } catch (Exception ex) {
            logs.write(ex, "model转消息");
        }

        return null;
    }
}
