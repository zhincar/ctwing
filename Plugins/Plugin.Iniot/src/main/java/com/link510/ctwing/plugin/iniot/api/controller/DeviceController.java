package com.link510.ctwing.plugin.iniot.api.controller;


import com.link510.ctwing.plugin.iniot.api.controller.BaseIniotApiController;
import com.link510.ctwing.services.IniotUtils;
import com.link510.iniot.sdk.domain.devices.DeviceOutDTO;
import com.link510.iniot.sdk.domain.devices.ProductOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.NotSupportedException;
import java.util.List;

import static com.link510.ctwing.core.errors.SateCollect.SUCCESS;

@RestController(value = "DeviceController")
public class DeviceController extends BaseIniotApiController {

    @Autowired
    private IniotUtils iniotUtils;

    /**
     * 列表
     *
     * @return 设备列表信息
     */
    @RequestMapping(value = {"device/list"})
    public String list(Integer pageSize, Integer pageNumber) {


        try {
            List<DeviceOutDTO> list = iniotUtils.getDeviceManagement().list();


            if (list == null || list.size() <= 0) {
                return JsonView("暂无更多设备");
            }

            return JsonView(SUCCESS, list, "设备列表信息获取成功")
                    ;
        } catch (NotSupportedException ex) {
            logs.write(ex);
        }
        return JsonView("设备列表获取失败");
    }

    /**
     * 设备信息
     *
     * @return String
     */
    @RequestMapping(value = {"device/show"})
    public String show(@RequestParam(defaultValue = "75001") String deviceSN) {

        try {

            DeviceOutDTO deviceOutDTO = iniotUtils.getDeviceManagement().find(deviceSN);

            if (deviceOutDTO == null || deviceOutDTO.getProductId() <= 0) {
                return JsonView("暂未查到相关设备信息");
            }

            return JsonView(SUCCESS, deviceOutDTO, "设备列表信息获取成功");

        } catch (NotSupportedException ex) {
            logs.write(ex);
        }

        return JsonView("设备信息获取失败");
    }

    /**
     * 列表
     *
     * @return String
     */
    @RequestMapping(value = {"device/product"})
    public String product(@RequestParam(defaultValue = "23380") String deviceSN) {
        try {

            ProductOutDTO productOutDTO = iniotUtils.getDeviceManagement().product(deviceSN);

            if (productOutDTO == null || productOutDTO.getProductInfo() == null || productOutDTO.getProductInfo().getProductId() <= 0) {
                return JsonView("暂未查到相关设备产品信息");
            }

            return JsonView(SUCCESS, productOutDTO, "设备产品信息获取成功");

        } catch (NotSupportedException ex) {
            logs.write(ex);
        }

        return JsonView("设备信息获取失败");
    }

}
