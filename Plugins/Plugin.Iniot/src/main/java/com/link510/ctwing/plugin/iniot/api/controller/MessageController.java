package com.link510.ctwing.plugin.iniot.api.controller;

import com.google.common.collect.Maps;
import com.link510.ctwing.core.domain.message.MessageRecordInfo;
import com.link510.ctwing.plugin.iniot.api.controller.BaseIniotApiController;
import com.link510.ctwing.services.IniotUtils;
import com.link510.ctwing.services.Messages;
import com.link510.iniot.sdk.domain.messages.ListMessageOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.NotSupportedException;
import java.util.List;
import java.util.Map;

import static com.link510.ctwing.core.errors.SateCollect.SUCCESS;

@RestController(value = "MessageController")
public class MessageController extends BaseIniotApiController {

    @Autowired
    private IniotUtils iniotUtils;

    @Autowired
    private Messages messages;

    /**
     * 消息列表
     *
     * @return
     */
    @RequestMapping(value = {"message/list"})
    public String list(@RequestParam(defaultValue = "23380") String deviceSN,
                       @RequestParam(defaultValue = "20") Integer pageSize,
                       @RequestParam(defaultValue = "1") Integer pageNumber) {

        try {
            ListMessageOutDTO list = iniotUtils.getMessageManagement().list(deviceSN, pageSize, pageNumber);

            if (list == null) {
                return JsonView("没有更多消息了");
            }

            return JsonView(SUCCESS, list, "获取设备消息列表");
        } catch (NotSupportedException e) {
            e.printStackTrace();
        }

        return JsonView("获取消息列表失败");
    }

    /**
     * 消息列表
     *
     * @return
     */
    @RequestMapping(value = {"message/show"})
    public String show(@RequestParam(defaultValue = "319502") Integer msgId) {

        try {
            Map<String, Object> maps = iniotUtils.getMessageManagement().show(4590375, Maps.newHashMap().getClass());

            if (maps == null) {

                return JsonView("消息获取失败");
            }

            return JsonView(SUCCESS, maps, "消息获取成功");

        } catch (NotSupportedException ex) {
            logs.write(ex, "消息获取失败");
        }

        return JsonView("消息获取失败");
    }

    /**
     * 消息记录列表
     *
     * @return
     */
    @RequestMapping(value = {"msgrecord/list"})
    public String messageRecordList(@RequestParam(defaultValue = "23380") String deviceSN,
                       @RequestParam("key2")  String key2,
                       @RequestParam(value = "value",required = false) String value   ) {

        try {
            if (key2 == null) {
                return JsonView("获取消息列表失败");
            }
            Specification<MessageRecordInfo> condition = messages.getMessageRecordListCondition(deviceSN,key2,value);

            Sort sort = new Sort(Sort.Direction.DESC, "addTime");

            List<MessageRecordInfo> list = messages.getMessageRecordList(condition,sort);

            if (list == null) {
                return JsonView("没有更多消息了");
            }

            return JsonView(SUCCESS, list, "获取设备消息列表");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return JsonView("获取消息列表失败");
    }

}
