package com.link510.ctwing.plugin.airobot.tcp.impl;

import com.cqwo.tools.netty.server.NettyTcpServerImpl;
import com.link510.ctwing.plugin.airobot.tcp.TcpServer;
import org.springframework.stereotype.Service;

/*
 * @Author wangdi 上发信息处理
 * @Description //TODO
 * @Date 2019/5/28 10:26
 * @Param @param null:
 * @return
 **/
@Service(value = "TcpServerImpl")
public class TcpServerImpl extends NettyTcpServerImpl implements TcpServer {

}
