package com.link510.ctwing.plugin.airobot.domain;

import com.link510.ctwing.core.helper.HexHelper;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.plugin.airobot.helper.ModbusHelper;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Modbus协议
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ModBusCommandInfo implements BaseModBusInfo {

    private static final long serialVersionUID = -8250535188638049550L;

    /**
     * 地址域名
     */
    @Builder.Default
    public Integer address = 0x11;

    /**
     * 功能码
     */
    @Builder.Default
    public Integer function = 0x00;


    @Builder.Default
    public Integer crcHeight = 0x00;

    /**
     * crc校验低
     */
    @Builder.Default
    public Integer crcLow = 0x00;

    /**
     * 寄存器地址
     */
    @Builder.Default
    public Integer registerAddress = 0x0000;

    /**
     * 参数值
     */
    @Builder.Default
    public String valueStr = "";


    /**
     * 生成modbus协议
     *
     * @return 返回组装modbus串
     */
    @Override
    public String toHexString() {

        StringBuilder sb = new StringBuilder();
        String addressStr = HexHelper.toHexString(address, 2);
        sb.append(addressStr);
        String functionStr = HexHelper.toHexString(function, 2);
        sb.append(functionStr);
        String registerAddressStr = HexHelper.toHexString(registerAddress, 4);
        sb.append(registerAddressStr);
        sb.append(valueStr);
        System.out.println(sb);

        String crc16 = ModbusHelper.getCRC(sb.toString());

        sb.append(crc16);
        System.out.println(sb);

        return sb.toString();
    }

    /**
     * 数据转码
     *
     * @param hexStr 十六进制串
     * @return BaseModBusInfo
     */
    public static ModBusCommandInfo of(String hexStr) {


        try {

            ModBusCommandInfo info = new ModBusCommandInfo();

            //地址码
            String addressStr = StringHelper.substring(hexStr, 0, 2);
            Integer address = HexHelper.hexadecimalToDecimalism(addressStr);
            info.setAddress(address);


            //功能码
            String functionStr = StringHelper.substring(hexStr, 2, 2);
            Integer function = HexHelper.hexadecimalToDecimalism(functionStr);
            info.setFunction(function);

            String registerAddressStr = StringHelper.substring(hexStr, 4, 4);
            Integer registerAddress = HexHelper.hexadecimalToDecimalism(registerAddressStr);
            info.setRegisterAddress(registerAddress);

            String valueStr = StringHelper.substring(hexStr, 8, hexStr.length() - 12);
            info.setValueStr(valueStr);

            String crcHeightStr = StringHelper.substring(hexStr, hexStr.length() - 4, 2);
            Integer crcHeight = HexHelper.hexadecimalToDecimalism(crcHeightStr);
            info.setCrcHeight(crcHeight);

            String crcLowStr = StringHelper.substring(hexStr, hexStr.length() - 2, 2);
            Integer crcLow = HexHelper.hexadecimalToDecimalism(crcLowStr);
            info.setCrcLow(crcLow);

            return info;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }
}
