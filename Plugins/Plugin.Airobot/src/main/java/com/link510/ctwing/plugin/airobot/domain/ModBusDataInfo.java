package com.link510.ctwing.plugin.airobot.domain;

import com.link510.ctwing.core.helper.HexHelper;
import com.link510.ctwing.core.helper.StringHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据
 *
 * @author cqnews
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ModBusDataInfo implements BaseModBusInfo {


    /**
     * 地址域名
     */
    @Builder.Default
    public Integer address = 0x11;

    /**
     * 功能码
     */
    @Builder.Default
    public Integer function = 0x00;


    @Builder.Default
    public Integer crcHeight = 0x00;

    /**
     * crc校验低
     */
    @Builder.Default
    public Integer crcLow = 0x00;

    /**
     * 数据长度地址
     */
    @Builder.Default
    public Integer valueLength = 0x00;

    /**
     * 参数值
     */
    @Builder.Default
    public String valueStr = "";


    /**
     * 十六进制数据
     *
     * @param hexStr 十六进制数据
     * @return ModBusDataInfo
     */
    public static ModBusDataInfo of(String hexStr) {


        try {


            ModBusDataInfo dataInfo = new ModBusDataInfo();

            //地址码
            String addressStr = StringHelper.substring(hexStr, 0, 2);
            Integer address = HexHelper.hexadecimalToDecimalism(addressStr);
            dataInfo.setAddress(address);


            //功能码
            String functionStr = StringHelper.substring(hexStr, 2, 2);
            Integer function = HexHelper.hexadecimalToDecimalism(functionStr);
            dataInfo.setFunction(function);

            String valueLengthStr = StringHelper.substring(hexStr, 4, 2);

            Integer valueLength = HexHelper.hexadecimalToDecimalism(valueLengthStr);
            dataInfo.setValueLength(valueLength);

            String valueStr = "";

            if (valueLength >= 1) {
                valueStr = StringHelper.substring(hexStr, 6, valueLength);
            }

            dataInfo.setValueStr(valueStr);

            String crcHeightStr = StringHelper.substring(hexStr, hexStr.length() - 4, 2);
            Integer crcHeight = HexHelper.hexadecimalToDecimalism(crcHeightStr);
            dataInfo.setCrcHeight(crcHeight);

            String crcLowStr = StringHelper.substring(hexStr, hexStr.length() - 2, 2);
            Integer crcLow = HexHelper.hexadecimalToDecimalism(crcLowStr);
            dataInfo.setCrcLow(crcLow);

            return dataInfo;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;

    }

}
