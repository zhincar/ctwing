package com.link510.ctwing.plugin.airobot;

import com.cqwo.tools.netty.config.ServerTransportConfig;
import com.link510.ctwing.core.envent.IEvent;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.core.plugin.interface2.IParserPlugin;
import com.link510.ctwing.plugin.airobot.domain.ModBusCommandInfo;
import com.link510.ctwing.plugin.airobot.handlers.*;
import com.link510.ctwing.plugin.airobot.tcp.TcpServer;
import com.link510.ctwing.services.Devices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;


@Component(value = "PluginAirobotService")
public class PluginService implements IParserPlugin, IEvent {

    @Autowired
    private Logs logs;

    @Autowired
    private TcpServer tcpServer;

    @Autowired
    private MessageUtils messageUtils;

    @Autowired
    private Devices devices;


    private ServerTransportConfig config;

    @Override
    public String topic() {
        return "airobot";
    }

    @Override
    public Integer pluginId() {
        return 2;
    }

    /**
     * 发送广播
     *
     * @param msg 消息内容
     */
    @Override
    public void broadcast(String msg) {

    }

    /**
     * 发送消息
     *
     * @param deviceSN 通道id
     * @param msg      消息内容
     */
    @Override
    public void message(String deviceSN, String msg) {
        messageUtils.sendMessage(deviceSN, msg);
    }

    /**
     * 名称
     *
     * @return
     */
    @Override
    public String name() {
        return "ODeviceInGateway";
    }

    /**
     * 协议描述
     *
     * @return
     */
    @Override
    public String desc() {
        return "英卡电子智能机器人网关";
    }

    @Override
    public String version() {
        return "0.1";
    }


    /**
     * 初始化插件
     */
    @PostConstruct
    @Override
    public void initPlugin() {


        try {

            config = new ServerTransportConfig(6080, "0.0.0.0");

            TcpServerHandler tcpRegisterHandler = new TcpServerHandler(messageUtils,
                    devices);

            config.getHandlerList().put("tcpHandler", tcpRegisterHandler);
            config.getHandlerList().put("ping", new MyIdleStateHandler(60, 20, 60 * 10, TimeUnit.SECONDS));

            config.setDecoder(new MessageDecoder());
            config.setEncoder(new MessageEncoder());
            config.setSoKeepalive(true);
            tcpServer.start(config);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    //@Scheduled(cron = "0/20 * * * * ?")
    public void execute() {

        //List<Integer> cList = Lists.newLinkedList();

//        Integer heightChar = 0x11;
//        Integer lowChar = 50;
//
//        cList.add(heightChar);
//        cList.add(lowChar);

        ModBusCommandInfo modBusCommandInfo = new ModBusCommandInfo();
        modBusCommandInfo.setAddress(0x11);
        modBusCommandInfo.setFunction(0x00);
        modBusCommandInfo.setValueStr("00FF");

        message("000", modBusCommandInfo.toHexString());
    }
}
