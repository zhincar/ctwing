package com.link510.ctwing.plugin.airobot.handlers;

import com.cqwo.tools.netty.channel.ChannelInfo;
import com.google.common.collect.Maps;
import com.link510.ctwing.core.helper.HexHelper;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.plugin.airobot.domain.ModBusCommandInfo;
import com.link510.ctwing.plugin.airobot.domain.ModBusDataInfo;
import com.link510.ctwing.plugin.airobot.helper.ModbusHelper;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

@ChannelHandler.Sharable
@Service(value = "MessageUtils")
public class MessageUtils {


    private static final Logger logger = LoggerFactory.getLogger(MessageUtils.class);

    private Map<String, ChannelInfo> channelInfoList = Maps.newHashMap();

    public Map<String, ChannelInfo> getChannelInfoList() {
        return channelInfoList;
    }

    /**
     * 添加 通道
     *
     * @param channel 通道
     */
    public void addChannelList(Channel channel, String deviceSN) {

        try {

            if (channel == null) {
                return;
            }

            channelInfoList.put(deviceSN, new ChannelInfo(deviceSN, channel.id(), channel));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 移除通道
     *
     * @param channel 通道
     */
    public void removeChannelList(Channel channel) {

        try {

            if (channel == null) {
                return;
            }

            if (channelInfoList == null || channelInfoList.size() <= 0) {
                return;
            }
            for (Map.Entry<String, ChannelInfo> map : channelInfoList.entrySet()) {

                if (map == null) {
                    continue;
                }
                logger.info("删除下线设备for：-----" + channel.id().asLongText());
                if (map.getValue().getChannelId().equals(channel.id())) {
                    channelInfoList.remove(map.getKey());
                    logger.info("确定删除下线设备：-----" + channel.id().asLongText());
                    break;
                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    /**
     * 检查是否已经注册
     *
     * @param channel 通道
     * @return
     */
    public boolean checkChannelList(Channel channel) {
        return true;
    }

    /**
     * 发送消息
     *
     * @param deviceSN 通道id
     * @param msg      msg
     * @return
     */
    public boolean sendMessage(String deviceSN, String msg) {

        try {

            ChannelInfo channelInfo = channelInfoList.getOrDefault(deviceSN, null);

            if (channelInfo == null || !channelInfo.getChannel().isOpen()) {
                return false;
            }

            return sendMessage(channelInfo.getChannel(), msg);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    /**
     * 发送消息
     *
     * @param channel 通道id
     * @param msg     hexStr
     * @return
     */
    public boolean sendMessage(Channel channel, String msg) {


        try {

            if (channel == null || !channel.isOpen()) {
                return false;
            }

            channel.writeAndFlush(msg);
            return true;

        } catch (Exception ex) {

            logger.warn("命令下发异常", ex);

        }

        return false;
    }

    /**
     * 设备是否注册
     *
     * @param channelId
     * @return
     */
    public boolean isRegistered(ChannelId channelId) {


        for (Map.Entry<String, ChannelInfo> map : channelInfoList.entrySet()) {

            if (map.getValue().getChannelId().asLongText().equals(channelId.asLongText())) {
                return true;
            }

        }

        return false;
    }

    /**
     * 数据解析
     *
     * @param ctx 通道
     * @param msg 消息
     */
    public void handle(ChannelHandlerContext ctx, String msg) {

        Integer function = getFunctionStr(msg);

        switch (function) {
            case 0x00: //设备心跳或设备注册类信息,原样返回
                //System.out.println("心跳包处理");
                handleHeart(ctx, msg);
                break;
            case 0x01://读取线圈状态,读内部比特量输出状态；如告警、变位、故障、异常等,这类消息是返回状态,不用返回
                //System.out.println("数据查询返回");
                handleReadFunction01(ctx, msg);
                break;
            case 0x02: //读取输入状态,读外部开关量输入状态；如光耦输入、继电器输入等
                handleReadFunction02(ctx, msg);
                System.out.println("数据输入查询返回");
                break;
            case 0x03: //读取保持寄存器,读内部输出量的寄存器；如一些配置参数、定值的读取等
                handleReadFunction03(ctx, msg);
                System.out.println("数据输入查询返回");
                break;
            case 0x04: //读取输入寄存器,读外部输入量的寄存器；如外部电压电流模拟量，温湿度等
                handleReadFunction04(ctx, msg);
                System.out.println("数据输入查询返回");
                break;
            case 0x05: //强制单线圈写入,为写内部单个开关量状态；如光耦输出、继电器输出等
                handleReadFunction05(ctx, msg);
                System.out.println("数据输入查询返回");
                break;
            case 0x06: //内部单个寄存器的值；如更改配置参数、定值等。
                handleReadFunction06(ctx, msg);
                System.out.println("数据输入查询返回");
                break;
            case 0x07: //读异常状态
                handleReadFunction07(ctx, msg);
                System.out.println("数据输入查询返回");
                break;
            case 0x08: //设备诊断
                handleReadFunction08(ctx, msg);
                System.out.println("数据输入查询返回");
                break;
            default:
                System.out.println("异常的解析");


        }

    }

    /**
     * 心跳包处理
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleHeart(ChannelHandlerContext ctx, String msg) {

        ModBusCommandInfo commandInfo = ModBusCommandInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        //sendMessage(ctx.channel(), msg);

        //System.out.println(msg);
        //System.out.println(commandInfo.toString());
    }

    /**
     * 读取线圈状态,读内部比特量输出状态；如告警、变位、故障、异常等,这类消息是返回状态,不用返回
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction01(ChannelHandlerContext ctx, String msg) {

        ModBusCommandInfo commandInfo = ModBusCommandInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }

    /**
     * 读取输入状态,读外部开关量输入状态；如光耦输入、继电器输入等,这类消息是返回状态,不用返回
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction02(ChannelHandlerContext ctx, String msg) {

        ModBusCommandInfo commandInfo = ModBusCommandInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }

    /**
     * 读取保持寄存器,读内部输出量的寄存器；如一些配置参数、定值的读取等,不需要返回
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction03(ChannelHandlerContext ctx, String msg) {

        ModBusDataInfo commandInfo = ModBusDataInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        //剩下的各人处理

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }

    /**
     * 读取输入寄存器,读外部输入量的寄存器；如外部电压电流模拟量，温湿度等,不需要返回
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction04(ChannelHandlerContext ctx, String msg) {

        ModBusDataInfo commandInfo = ModBusDataInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        //剩下的各人处理

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }

    /**
     * 内部单个寄存器的值返回；如更改配置参数、定值等,不需要返回
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction06(ChannelHandlerContext ctx, String msg) {

        ModBusCommandInfo commandInfo = ModBusCommandInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        //剩下的各人处理

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }


    /**
     * 读异常状态
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction07(ChannelHandlerContext ctx, String msg) {

        ModBusDataInfo commandInfo = ModBusDataInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        //剩下的各人处理

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }

    /**
     * 设备诊断信息返回
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction08(ChannelHandlerContext ctx, String msg) {

        ModBusDataInfo commandInfo = ModBusDataInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        //剩下的各人处理

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }

    /**
     * 强制单线圈写入返回,为写内部单个开关量状态；如光耦输出、继电器输出等,不需要返回
     *
     * @param ctx 上下文
     * @param msg 消息
     */
    private void handleReadFunction05(ChannelHandlerContext ctx, String msg) {

        ModBusCommandInfo commandInfo = ModBusCommandInfo.of(msg);

        if (commandInfo == null) {
            return;
        }

        //剩下的各人处理

        System.out.println(msg);
        System.out.println(commandInfo.toString());
    }


    /**
     * 获取功能码
     *
     * @param msg 消息
     * @return String
     */
    private Integer getFunctionStr(String msg) {

        try {

            String functionStr = StringHelper.substring(msg, 2, 2);
            return HexHelper.hexadecimalToDecimalism(functionStr);


        } catch (Exception ex) {
            logger.warn("获取功能码异常");
        }

        return -1;
    }

    /**
     * 校验消息CRC
     *
     * @param msg 消息
     * @return 校验是否成功
     */
    public static boolean checkMessageCRC(String msg) {

        try {

            String data = msg.substring(0, msg.length() - 4);
            String crc = msg.substring(msg.length() - 4);

            return checkMessageCRC(data, crc);

        } catch (Exception ex) {
            logger.warn("校验异常", ex);
        }

        return false;
    }

    /**
     * 校验消息CRC
     *
     * @param data 数据
     * @param crc  crc检验
     * @return 校验是否成功
     */
    public static boolean checkMessageCRC(String data, String crc) {

        try {

            String crc2 = ModbusHelper.getCRC(data);

            return crc.equals(crc2);

        } catch (Exception ex) {
            logger.warn("校验异常", ex);
        }

        return false;
    }

    public static void main(String[] args) {
        System.out.println(checkMessageCRC("11000001353035303030303030310085"));
    }
}
