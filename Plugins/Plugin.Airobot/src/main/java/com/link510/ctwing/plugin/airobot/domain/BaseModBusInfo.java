package com.link510.ctwing.plugin.airobot.domain;

import lombok.Builder;

import java.io.Serializable;


/**
 * modbus定义
 *
 * @author cqnews
 */
public interface BaseModBusInfo extends Serializable {


    /**
     * 地址域名
     */
    Integer getAddress();

    /**
     * 地址域名
     */
    void setAddress(Integer address);

    /**
     * 功能码
     */
    Integer getFunction();

    /**
     * 功能码
     */
    void setFunction(Integer function);

    /**
     * CRC校验高位
     */
    Integer getCrcHeight();

    /**
     * CRC校验高位
     */
    void setCrcHeight(Integer crcHeight);

    /**
     * CRC校验高位
     */
    Integer getCrcLow();

    /**
     * CRC校验高位
     */
    void setCrcLow(Integer crcLow);


    /**
     * modbus转十六进制
     *
     * @return
     */
    default String toHexString() {
        return "";
    }

    /**
     * 数据转码
     *
     * @param hexStr 十六进制串
     * @return BaseModBusInfo
     */
    static BaseModBusInfo of(String hexStr) {
        return null;
    }
}
