package com.link510.ctwing.plugin.airobot.api.controller;

import com.link510.ctwing.core.helper.HexHelper;
import com.link510.ctwing.plugin.airobot.domain.ModBusCommandInfo;
import com.link510.ctwing.plugin.airobot.handlers.MessageUtils;
import com.link510.ctwing.web.framework.controller.BaseApiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.link510.ctwing.core.errors.SateCollect.SUCCESS;


/**
 * 智能机器人
 *
 * @author cqnews
 */
@RestController(value = "AirobotPluginRobotApiCameraController")
public class CameraController extends BaseRabotApiController {

    @Autowired
    private MessageUtils messageUtils;

    /**
     * 摄像机水平旋转
     *
     * @return String
     */
    @RequestMapping(value = "camera/rotate/pan")
    public String panRotate(@RequestParam(defaultValue = "5050000001") String deviceSN,
                            @RequestParam(defaultValue = "50") Integer speed) {

        if (speed > 100) {
            speed = 100;
        } else if (speed < -100) {
            speed = -100;
        }

        StringBuilder valueStr = new StringBuilder();

        if (speed > 0) {
            valueStr.append("FF");
        } else {
            valueStr.append("00");
        }



        String speedStr = HexHelper.toHexString(Math.abs(speed));

        valueStr.append(speedStr);

        ModBusCommandInfo modBusCommandInfo = ModBusCommandInfo.builder()
                .address(0x11)
                .function(0x01)
                .registerAddress(0x0201)
                .valueStr(valueStr.toString())
                .build();

        if (messageUtils.sendMessage(deviceSN, modBusCommandInfo.toString())) {
            return JsonView(SUCCESS, "命令下发成功");
        }

        return JsonView("命令下发失败");
    }

    /**
     * 摄像机倾斜旋转
     *
     * @return String
     */
    @RequestMapping(value = "camera/rotate/tilt")
    public String tiltRotate(@RequestParam(defaultValue = "5050000001") String deviceSN,
                             @RequestParam(defaultValue = "50") Integer speed) {

        if (speed > 100) {
            speed = 100;
        } else if (speed < -100) {
            speed = -100;
        }
        StringBuilder valueStr = new StringBuilder();

        if (speed > 0) {
            valueStr.append("FF");
        } else {
            valueStr.append("00");
        }

        String speedStr = HexHelper.toHexString(Math.abs(speed));

        valueStr.append(speedStr);

        ModBusCommandInfo modBusCommandInfo = ModBusCommandInfo.builder()
                .address(0x11)
                .function(0x01)
                .registerAddress(0x0202)
                .valueStr(valueStr.toString())
                .build();

        if (messageUtils.sendMessage(deviceSN, modBusCommandInfo.toString())) {
            return JsonView(SUCCESS, "命令下发成功");
        }

        return JsonView("命令下发失败");
    }

    /**
     * 摄像机缩放
     *
     * @return String
     */
    @RequestMapping(value = "camera/rotate/zoom")
    public String zoomRotate(@RequestParam(defaultValue = "5050000001") String deviceSN,
                             @RequestParam(defaultValue = "12") Integer speed) {

        if (speed > 100) {
            speed = 100;
        } else if (speed < -100) {
            speed = -100;
        }
        StringBuilder valueStr = new StringBuilder();

        if (speed > 0) {
            valueStr.append("FF");
        } else {
            valueStr.append("00");
        }

        String speedStr = HexHelper.toHexString(Math.abs(speed));

        valueStr.append(speedStr);

        ModBusCommandInfo modBusCommandInfo = ModBusCommandInfo.builder()
                .address(0x11)
                .function(0x01)
                .registerAddress(0x0203)
                .valueStr(valueStr.toString())
                .build();

        if (messageUtils.sendMessage(deviceSN, modBusCommandInfo.toString())) {
            return JsonView(SUCCESS, "命令下发成功");
        }

        return JsonView("命令下发失败");
    }


}
