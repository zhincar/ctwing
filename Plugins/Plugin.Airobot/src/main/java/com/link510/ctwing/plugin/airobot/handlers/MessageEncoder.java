package com.link510.ctwing.plugin.airobot.handlers;

import com.link510.ctwing.core.helper.ByteHelper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.List;

@ChannelHandler.Sharable
public class MessageEncoder extends MessageToMessageEncoder<String> {

    private Charset charset = CharsetUtil.ISO_8859_1;

    private static final Logger log = LoggerFactory.getLogger(MessageEncoder.class);

    /*private Charset charset = CharsetUtil.UTF_8;
     */

    @Override
    protected void encode(ChannelHandlerContext ctx, String msg, List<Object> out) throws Exception {

        if (msg.length() == 0) {
            return;
        }


        try {

            byte[] bytes = ByteHelper.hexStringToByte(msg);

            log.info("=====================================================命令组装成功=====================================================");
            log.info("====================================================msg:" + msg + "====================================================");

            for (int i = 0; i < bytes.length; i++) {
                System.out.println("bytes[" + i + "]:" + bytes[i]);
            }

            ByteBuf byteBuf = ctx.alloc().buffer(Integer.BYTES);
            byteBuf.writeBytes(bytes);
            out.add(byteBuf);
            log.info("=====================================================命令下发成功:" + msg + "=====================================================");


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
