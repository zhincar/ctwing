package com.link510.ctwing.plugin.airobot.api.controller;

import com.google.common.base.Strings;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import com.link510.ctwing.core.domain.device.DeviceToken;
import com.link510.ctwing.core.exption.CWMAPIException;
import com.link510.ctwing.core.exption.CWMException;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import com.link510.ctwing.services.Devices;
import com.link510.ctwing.web.framework.controller.BaseApiController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * 机器人API主类
 */
@RequestMapping(value = "api/robot")
public class BaseRabotApiController extends BaseApiController {

    @Autowired
    protected Devices devices;


    /**
     * 设备信息
     */
    protected DeviceInfo deviceInfo;

    @Override
    @ModelAttribute
    public void setInitialize(HttpServletResponse response) throws CWMException {
        super.setInitialize(response);


        try {

            String token = cwmUtils.getApiTokenHeader();

            if (Strings.isNullOrEmpty(token)) {
                throw new CWMAPIException(HttpStatus.ACCEPTED, "token异常丢失");
            }

            DeviceToken deviceToken = devices.decryptDeviceToken(token);

            if (deviceToken == null || Strings.isNullOrEmpty(deviceToken.getDeviceSN())) {
                throw new CWMAPIException(HttpStatus.ACCEPTED, "token解析失败");
            }

            Integer nowTime = UnixTimeHelper.getUnixTimeStamp();

            if (deviceToken.getLimitTime() < nowTime) {
                throw new CWMAPIException(HttpStatus.ACCEPTED, "token过期");
            }

            deviceInfo = devices.getDeviceByDeviceSN(deviceToken.getDeviceSN());

        } catch (CWMAPIException ex) {
            throw ex;
        } catch (Exception ex) {

            throw new CWMAPIException(HttpStatus.ACCEPTED, "异常退出");
        }

    }
}
