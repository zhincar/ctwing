package com.link510.ctwing.plugin.airobot.handlers;

import com.cqwo.tools.netty.utils.NetUtils;
import com.google.common.base.Strings;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import com.link510.ctwing.core.helper.HexHelper;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.core.helper.ValidateHelper;
import com.link510.ctwing.plugin.airobot.helper.ModbusHelper;
import com.link510.ctwing.services.Devices;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tcp注册解析
 *
 * @author cqnews
 */
@ChannelHandler.Sharable
public class TcpServerHandler extends SimpleChannelInboundHandler<String> {

    private Logger logger = LoggerFactory.getLogger(TcpServerHandler.class);

    private Devices devices;

    private MessageUtils messageUtils;

    /**
     * 禁止无参构造
     */
    private TcpServerHandler() {

    }

    public TcpServerHandler(
            MessageUtils messageUtils, Devices devices) {
        this.devices = devices;
        this.messageUtils = messageUtils;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {


        //System.out.println(msg);

        //CRC校验异常,不处理
        if (!MessageUtils.checkMessageCRC(msg)) {
            return;
        }


        //检测是否注册tcp链接
        if (messageUtils.isRegistered(ctx.channel().id())) {


            //空串不处理
            if (Strings.isNullOrEmpty(msg)) {
                return;
            }

            messageUtils.handle(ctx, msg);

        } else {

            if (Strings.isNullOrEmpty(msg)) {
                ctx.close();
                return;
            }

            String register = "^[A-Fa-f0-9]{2}000001[A-Fa-f0-9]{2,}$";

            if (ValidateHelper.match(register, msg)) {

                String deviceHex = msg.substring(8, msg.length() - 4);

                if (Strings.isNullOrEmpty(deviceHex)) {
                    ctx.close();
                    return;
                }

                String deviceSN = HexHelper.convertHexToString(deviceHex);

                if (Strings.isNullOrEmpty(deviceSN)) {
                    ctx.close();
                    return;
                }

                DeviceInfo deviceInfo = devices.getDeviceByDeviceSN(deviceSN);

                if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
                    ctx.close();
                    return;
                }

                System.out.println("deviceSN:" + deviceSN);

                messageUtils.addChannelList(ctx.channel(), deviceInfo.getDeviceSN());
                return;
            }

            ctx.close();
        }


    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {

        logger.debug("TcpServerHandler Connected from {" +
                NetUtils.channelToString(ctx.channel().remoteAddress(), ctx.channel().localAddress()) + "}");
        System.out.println("当前通道id:" + ctx.channel().id());
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        logger.debug("TcpServerHandler Disconnected from {" +
                NetUtils.channelToString(ctx.channel().remoteAddress(), ctx.channel().localAddress()) + "}");

        if (messageUtils == null) {
            return;
        }

        messageUtils.removeChannelList(ctx.channel());
    }

}
