package com.link510.ctwing.plugin.airobot.handlers;


import io.netty.util.AttributeKey;

/**
 * 常量
 * @author cqnews
 */
public class NettyConts {

    public static AttributeKey<String> KEY_CHARSET = AttributeKey.valueOf("charset");

}
