package com.link510.ctwing.plugin.airobot.api.controller;

import com.link510.ctwing.plugin.airobot.handlers.MessageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cqnews
 */
@RestController(value = "AirobotPluginRobotApiMessageController")
public class MessageController extends BaseRabotApiController {

    @Autowired
    private MessageUtils messageUtils;

    /**
     * 发送消息
     *
     * @param deviceSN 设备编号
     * @param message  消息
     * @return 返回
     */
    @RequestMapping(value = "message/send")
    public String sendMessage(@RequestParam(defaultValue = "50501") String deviceSN,
                              @RequestParam(defaultValue = "") String message) {

        if (messageUtils.sendMessage(deviceSN, message)) {
            return JsonView("下发成功");
        }

        return JsonView("下发失败");
    }
}
