package com.link510.ctwing.plugin.airobot.api.model;

import com.link510.ctwing.core.domain.device.DeviceInfo;
import com.link510.ctwing.core.helper.ValidateHelper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 设备登录模型
 *
 * @author cqnews
 */
@Data
@Builder
public class DeviceLoginModel implements Serializable {

    /**
     * 设备编号
     */
    @Builder.Default
    private String deviceSN = "";

    /**
     * 设备密码
     */
    @Builder.Default
    private String token = "";

    /**
     * 刷新token
     */
    @Builder.Default
    private String refreshToken = "";

    /**
     * 设备信息
     */
    private DeviceInfo deviceInfo;

}
