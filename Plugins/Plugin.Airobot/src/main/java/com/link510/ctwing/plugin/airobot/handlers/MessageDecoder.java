package com.link510.ctwing.plugin.airobot.handlers;

import com.link510.ctwing.core.helper.ByteHelper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.util.CharsetUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.List;

@ChannelHandler.Sharable
public class MessageDecoder extends MessageToMessageDecoder<ByteBuf> {
    private Charset charset = CharsetUtil.ISO_8859_1;

    //private Charset charset = CharsetUtil.UTF_8;
    private static final Logger log = LoggerFactory.getLogger(MessageEncoder.class);

    public MessageDecoder() {
    }

    public MessageDecoder(Charset charset) {
        this.charset = charset;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        if (msg.readableBytes() > 0) {
            try {

                String content = null;
                String keycharset = ctx.channel().attr(NettyConts.KEY_CHARSET).get();

                if (StringUtils.isNotBlank(keycharset)) {
                    content = msg.toString(Charset.forName(keycharset));
                } else {
                    content = msg.toString(charset);
                    //System.out.println("-------------------------------------------华丽的分割线-------------------------------------------------");
                }

                log.info(" content : " + content);
                content = ByteHelper.stringToHexString(content);
                if (StringUtils.isNotBlank(content)) {
                    out.add(content);
                } else {
                    msg.resetReaderIndex();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
