package com.link510.ctwing.strategy.rdbs.service;

import com.link510.ctwing.core.data.rdbs.IMessageStrategy;
import com.link510.ctwing.core.data.rdbs.repository.messages.*;
import com.link510.ctwing.core.domain.message.*;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * @author cqnews
 */
@Component(value = "MessageStrategy")
public class MessageStrategy extends RDBSService implements IMessageStrategy {

    @Autowired
    private MessageRepository messageRepository;

    @PersistenceContext
    private EntityManager entityManager;

    private Logger logger = LoggerFactory.getLogger(MessageStrategy.class);


    //region 消息

    /**
     * 获得消息数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override
    public long getMessageCount(Specification<MessageInfo> condition) throws IOException {
        return messageRepository.count();
    }


    /**
     * 创建一条消息数据
     *
     * @param messageInfo 消息模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override
    public MessageInfo createMessage(MessageInfo messageInfo) throws IOException {

        return messageRepository.save(messageInfo);
    }


    /**
     * 更新一条消息数据
     *
     * @param messageInfo 消息模型
     **/
    @Override
    public MessageInfo updateMessage(MessageInfo messageInfo) throws IOException {

        if (messageInfo.getMsgId() >= 1) {
            return messageRepository.save(messageInfo);
        }

        return messageInfo;

    }


    /**
     * 删除一条消息数据
     *
     * @param id 消息模型
     **/
    @Override
    public void deleteMessageById(int id) throws IOException {

        messageRepository.deleteById(id);
    }

    /**
     * 批量删除一批消息数据
     **/
    @Override
    public void deleteMessageByIdList(String idlist) throws IOException {


    }

    /**
     * 获得消息一条记录
     *
     * @param id deviceId
     * @return 返回一条MessageInfo
     **/
    @Override
    public MessageInfo getMessageById(int id) throws IOException {
        return messageRepository.findById(id).orElse(null);
    }

    /**
     * 获取最后一条数据
     *
     * @param deviceSN 设备编号
     * @param token    token
     * @return
     */
    @Override
    public MessageInfo getLastMessageByTokenAndDeviceSN(String token, String deviceSN) throws IOException {

        Specification<MessageInfo> condition = (Specification<MessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();


            list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
            list.add(cb.equal(root.get("token").as(String.class), token));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };

        Sort sort = new Sort(Sort.Direction.DESC, "msgId");

        return messageRepository.findAll(condition, sort).get(0);
    }

    /**
     * 获取最后一条数据
     *
     * @param deviceSN  设备编号
     * @param protocols 协议名称
     * @return
     */
    @Override
    public MessageInfo getLastMessageByDeviceSNAndProtocol(String deviceSN, Integer timestamp, String... protocols) throws IOException {

        Specification<MessageInfo> condition = (Specification<MessageInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();


            CriteriaBuilder.In<String> in = cb.in(root.get("protocol"));

            for (String protocol : protocols) {
                in.value(protocol.trim());
            }

            if (timestamp >= 0) {
                list.add(cb.lessThan(root.get("timestamp").as(Integer.class), timestamp));
            }

            list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)), in);

            return query.getGroupRestriction();

        };

        Sort sort = new Sort(Sort.Direction.DESC, "msgId");

        return messageRepository.findAll(condition, sort).get(0);
    }

    /**
     * 获得消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageInfo
     **/
    @Override
    public List<MessageInfo> getMessageList(Specification<MessageInfo> condition, Sort sort) throws IOException {

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "msgId");
        }

        return messageRepository.findAll(condition, sort);

    }


    /**
     * 获得消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageInfo
     **/
    @Override
    public Page<MessageInfo> getMessageList(Integer pageSize, Integer pageNumber, Specification<MessageInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "msgId");
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        return messageRepository.findAll(condition, pageable);


    }


    //endregion


    @Autowired
    private HeartMessageRepository heartmessageRepository;


    //region 心跳消息

    /**
     * 获得心跳消息数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override
    public long getHeartMessageCount(Specification<HeartMessageInfo> condition) throws IOException {

        return heartmessageRepository.count();
    }


    /**
     * 创建一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override
    public HeartMessageInfo createHeartMessage(HeartMessageInfo heartmessageInfo) throws IOException {

        return heartmessageRepository.save(heartmessageInfo);
    }


    /**
     * 更新一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     **/
    @Override
    public HeartMessageInfo updateHeartMessage(HeartMessageInfo heartmessageInfo) throws IOException {

        if (heartmessageInfo.getMsgId() >= 1) {
            return heartmessageRepository.save(heartmessageInfo);
        }

        return heartmessageInfo;

    }


    /**
     * 删除一条心跳消息数据
     *
     * @param msgId 心跳消息模型
     **/
    @Override
    public void deleteHeartMessageByMsgId(Integer msgId) throws IOException {
        heartmessageRepository.deleteById(msgId);
    }

    /**
     * 批量删除一批心跳消息数据
     **/
    @Override
    public void deleteHeartMessageByMsgIdList(String msgIdlist) throws IOException {

    }

    /**
     * 获得心跳消息一条记录
     *
     * @param msgId msgid
     * @return 返回一条HeartMessageInfo
     **/
    @Override
    public HeartMessageInfo getHeartMessageByMsgId(Integer msgId) throws IOException {

        Optional<HeartMessageInfo> heartmessageInfo = heartmessageRepository.findById(msgId);
        return heartmessageInfo.orElse(null);
    }


    /**
     * 获得心跳消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回HeartMessageInfo
     **/
    @Override
    public List<HeartMessageInfo> getHeartMessageList(Specification<HeartMessageInfo> condition, Sort sort) throws IOException {

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "msgId");
        }
        return heartmessageRepository.findAll(condition, sort);
    }


    /**
     * 获得心跳消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回HeartMessageInfo
     **/
    @Override
    public Page<HeartMessageInfo> getHeartMessageList(Integer pageSize, Integer pageNumber, Specification<HeartMessageInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "msgId");
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        return heartmessageRepository.findAll(condition, pageable);


    }


    //endregion


    @Autowired
    private MessageRecordRepository messageRecordRepository;


    //region 消息解析记录

    /**
     * 获得消息解析记录数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override
    public long getMessageRecordCount(Specification<MessageRecordInfo> condition) throws IOException {

        return messageRecordRepository.count();
    }


    /**
     * 创建一条消息解析记录数据
     *
     * @param messagerecordInfo 消息解析记录模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override
    public MessageRecordInfo createMessageRecord(MessageRecordInfo messagerecordInfo) throws IOException {

        return messageRecordRepository.save(messagerecordInfo);
    }


    /**
     * 更新一条消息解析记录数据
     *
     * @param messagerecordInfo 消息解析记录模型
     **/
    @Override
    public MessageRecordInfo updateMessageRecord(MessageRecordInfo messagerecordInfo) throws IOException {

        if (messagerecordInfo.getRecordId() >= 1) {
            return messageRecordRepository.save(messagerecordInfo);
        }

        return messagerecordInfo;

    }


    /**
     * 删除一条消息解析记录数据
     *
     * @param recordId 消息解析记录模型
     **/
    @Override
    public void deleteMessageRecordByRecordId(int recordId) throws IOException {

        messageRecordRepository.deleteById(recordId);
    }

    /**
     * 批量删除一批消息解析记录数据
     **/
    @Override
    public void deleteMessageRecordByRecordIdList(String recordIdlist) throws IOException {


    }

    /**
     * 获得消息解析记录一条记录
     *
     * @param recordId recordid
     * @return 返回一条MessageRecordInfo
     **/
    @Override
    public MessageRecordInfo getMessageRecordByRecordId(int recordId) throws IOException {
        return messageRecordRepository.findById(recordId).orElse(null);
    }


    /**
     * 获得消息解析记录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageRecordInfo
     **/
    @Override
    public List<MessageRecordInfo> getMessageRecordList(Specification<MessageRecordInfo> condition, Sort sort) throws IOException {

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "recordId");
        }

        return messageRecordRepository.findAll(condition, sort);

    }


    /**
     * 获得消息解析记录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageRecordInfo
     **/
    @Override
    public Page<MessageRecordInfo> getMessageRecordList(Integer pageSize, Integer pageNumber, Specification<MessageRecordInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "recordId");
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        return messageRecordRepository.findAll(condition, pageable);


    }

    /**
     * 获取消息的解析列表
     *
     * @param msgId 消息id
     * @return
     */
    @Override
    public List<MessageRecordInfo> getMessageRecordList(Integer msgId) throws IOException {
        return messageRecordRepository.findByMsgId(msgId);
    }


    /**
     * 统计在指定的时间内的消息数目
     *
     * @param deviceSN     设备id
     * @param intervalTime 时间段
     * @return
     */
    @Override
    public long countAllIntervalMessage(String deviceSN, Integer productId, String protocol, Integer intervalTime) throws IOException {

        Specification<MessageInfo> condition = new Specification<MessageInfo>() {
            @Override
            public Predicate toPredicate(Root<MessageInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));

                list.add(cb.equal(root.get("productId").as(Integer.class), productId));

                list.add(cb.equal(root.get("protocol").as(String.class), protocol));

                Integer timestamp = UnixTimeHelper.getUnixTimeStamp() - intervalTime;

                list.add(cb.ge(root.get("timestamp").as(Integer.class), timestamp));

                list.add(cb.equal(root.get("lose").as(Integer.class), 0));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();
            }
        };

        return messageRepository.count(condition);
    }

    /**
     * 统计token的心跳次数
     *
     * @param token        网关
     * @param type         消息类型
     * @param intervalTime 时间段
     */
    @Override
    public long countTokenIntervalMessage(String token, Integer type, Integer intervalTime) throws IOException {
        Specification<MessageInfo> condition = new Specification<MessageInfo>() {
            @Override
            public Predicate toPredicate(Root<MessageInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.equal(root.get("token").as(String.class), token));

                list.add(cb.equal(root.get("type").as(Integer.class), type));

                Integer timestamp = UnixTimeHelper.getUnixTimeStamp() - intervalTime;

                list.add(cb.ge(root.get("timestamp").as(Integer.class), timestamp));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();
            }
        };

        return messageRepository.count(condition);
    }

    /**
     * 统计在指定的时间内的消息数目
     *
     * @param deviceSN     设备编号
     * @param intervalTime 时间段
     * @return
     */
    @Override
    public long countAllIntervalMessage(Integer msgId, Integer productId, String deviceSN, String protocol, Integer intervalTime) throws IOException {

        Specification<MessageInfo> condition = new Specification<MessageInfo>() {
            @Override
            public Predicate toPredicate(Root<MessageInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> list = new ArrayList<>();

                list.add(cb.notEqual(root.get("msgId").as(Integer.class), msgId));

                list.add(cb.equal(root.get("productId").as(Integer.class), productId));

                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));

                list.add(cb.equal(root.get("protocol").as(String.class), protocol));

                Integer timestamp = UnixTimeHelper.getUnixTimeStamp() - intervalTime;

                list.add(cb.ge(root.get("timestamp").as(Integer.class), timestamp));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();
            }
        };

        return messageRepository.count(condition);
    }

    /**
     * 统计警告设备消息
     *
     * @return long
     */
    @Override
    public long countWarningAllMessage() throws IOException {
        return messageRepository.countWarningAllMessage().size();
    }

    /**
     * 让消息失效
     *
     * @param msgId 消息id
     */
    @Override
    public void lose(Integer msgId) throws IOException {
        messageRepository.lose(msgId);
    }

    /**
     * 通过tokem查询消息列表
     *
     * @param token
     * @return
     */
    @Override
    public List<MessageInfo> getMessageByToken(String token) throws IOException {

        return messageRepository.findByToken(token);
    }

    /**
     * 更新消息图片
     *
     * @param msgId  消息id
     * @param litpic 图片
     */
    @Override
    public void updateMessageLitpic(Integer msgId, String litpic) throws IOException {

        messageRepository.updateMessageLitpic(msgId, litpic);
    }

    /**
     * 获取最后一包心跳包时间
     *
     * @param token
     * @return
     */
    @Override
    public Integer getLastHeartMessageTimeByToken(String token) throws IOException {
        return heartmessageRepository.getLastHeartMessageTimeByToken(token);
    }

    /**
     * 获取最后一包数据包时间
     *
     * @param deviceSN 设备编号
     * @return
     */
    @Override
    public Integer getLastDataMessageTimeByDeviceSN(String deviceSN) throws IOException {
        return messageRepository.getLastDataMessageTimeByDeviceSN(deviceSN);
    }

    //endregion


    @Autowired
    private MessageImageRepository messageImageRepository;

    //region 图片消息

    /**
     * 获得图片消息数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override
    public long getMessageImageCount(Specification<MessageImageInfo> condition) throws IOException {

        return messageImageRepository.count();
    }


    /**
     * 创建一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override
    public MessageImageInfo createMessageImage(MessageImageInfo messageimageInfo) throws IOException {

        return messageImageRepository.save(messageimageInfo);
    }


    /**
     * 更新一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     **/
    @Override
    public MessageImageInfo updateMessageImage(MessageImageInfo messageimageInfo) throws IOException {

        if (messageimageInfo.getImgId() >= 1) {
            return messageImageRepository.save(messageimageInfo);
        }

        return messageimageInfo;

    }


    /**
     * 删除一条图片消息数据
     *
     * @param imgId 图片消息模型
     **/
    @Override
    public void deleteMessageImageByImgId(Integer imgId) throws IOException {

        messageImageRepository.deleteById(imgId);
    }

    /**
     * 批量删除一批图片消息数据
     **/
    @Override
    public void deleteMessageImageByImgIdList(String imgIdlist) throws IOException {


    }

    /**
     * 获得图片消息一条记录
     *
     * @param imgId imgid
     * @return 返回一条MessageImageInfo
     **/
    @Override
    public MessageImageInfo getMessageImageByImgId(Integer imgId) throws IOException {


        Optional<MessageImageInfo> messageimageInfo = messageImageRepository.findById(imgId);

        return messageimageInfo.orElse(null);
    }


    /**
     * 获得图片消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageImageInfo
     **/
    @Override
    public List<MessageImageInfo> getMessageImageList(Specification<MessageImageInfo> condition, Sort sort) throws IOException {

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "imgId");
        }

        return messageImageRepository.findAll(condition, sort);

    }


    /**
     * 获得图片消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageImageInfo
     **/
    @Override
    public Page<MessageImageInfo> getMessageImageList(Integer pageSize, Integer pageNumber, Specification<MessageImageInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "imgId");
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        return messageImageRepository.findAll(condition, pageable);


    }

    /**
     * 更新图片库状态
     *
     * @param state     类型
     * @param type      类型
     * @param deviceSN  设备编号
     * @param shortTime 分组时间
     */
    @Override
    public void updateMessageImageSate(Integer state, Integer type, String deviceSN, String shortTime) throws IOException {
        messageImageRepository.updateMessageImageSate(state, type, deviceSN, shortTime);
    }


    @Autowired
    private OutputRepository outputRepository;
    /**
     * 获得输出日志数量
     *
     * @param condition 条件
     * @return 返回数量
     * @throws IOException
     **/
    @Override
    public long getOutputCount(Specification<OutputInfo> condition) throws IOException {

        return outputRepository.count();
    }


    /**
     * 创建一条输出日志数据
     *
     * @param outputInfo 输出日志模型
     * @return 返回创建信息
     * @throws IOException
     **/
    @Override
    public OutputInfo createOutput(OutputInfo outputInfo) throws IOException {

        return outputRepository.save(outputInfo);
    }

    /**
     * 更新一条输出日志数据
     *
     * @param outputInfo 输出日志模型
     **/
    @Override
    public OutputInfo updateOutput(OutputInfo outputInfo) throws IOException {

        if (outputInfo.getLogId() >= 1) {
            return outputRepository.save(outputInfo);
        }

        return outputInfo;

    }

    /**
     * 删除一条输出日志数据
     *
     * @param logId 输出日志模型
     **/
    @Override
    public void deleteOutputByLogId(Integer logId) throws IOException {

        outputRepository.deleteById(logId);
    }


    /**
     * 批量删除一批输出日志数据
     **/
    @Override
    public void deleteOutputByLogIdList(String logIdlist) throws IOException {


    }


    /**
     * 获得输出日志一条记录
     *
     * @param logId logid
     * @return 返回一条OutputInfo
     **/
    @Override
    public OutputInfo getOutputByLogId(Integer logId) throws IOException {
        return outputRepository.findById(logId).orElse(null);
    }


    /**
     * 获得输出日志数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OutputInfo
     **/
    @Override
    public List<OutputInfo> getOutputList(Specification<OutputInfo> condition, Sort sort) throws IOException {

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "logId");
        }

        return outputRepository.findAll(condition, sort);

    }



    /**
     * 获得输出日志数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OutputInfo
     **/
    @Override
    public Page<OutputInfo> getOutputList(Integer pageSize, Integer pageNumber, Specification<OutputInfo> condition, Sort sort) throws IOException {

        if (pageNumber >= 1) {
            pageNumber--;
        }

        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "logId");
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        return outputRepository.findAll(condition, pageable);


    }
    //endregion

}


