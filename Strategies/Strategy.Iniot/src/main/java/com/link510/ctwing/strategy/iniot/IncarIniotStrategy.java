package com.link510.ctwing.strategy.iniot;

import com.alibaba.fastjson.TypeReference;
import com.link510.ctwing.core.domain.producrs.EnvironmentMessageInfo;
import com.link510.ctwing.core.iniots.IIniotStrategy;
import com.link510.ctwing.strategy.iniot.config.IniotConfig;
import com.link510.iniot.sdk.InIoTClient;
import com.link510.iniot.sdk.domain.messages.Environment11MessageOutDTO;
import com.link510.iniot.sdk.impl.AbstractIniotClientImpl;
import com.link510.iniot.sdk.management.DeviceManagement;
import com.link510.iniot.sdk.management.MessageManagement;
import com.link510.iniot.sdk.management.SubscribeManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.transaction.NotSupportedException;


/**
 * 英卡环境探测器设备
 *
 * @author cqnews
 */
@Component(value = "IncarEnvironmentProduct")
public class IncarIniotStrategy implements IIniotStrategy {

    private static final long serialVersionUID = -4290527545570958215L;

    /**
     * 设备管理器
     */
    private DeviceManagement deviceManagement;

    /**
     * 消息管理器
     */
    private MessageManagement messageManagement;

    /**
     * 订阅管理器
     */
    private SubscribeManagement subscribeManagement;


    @Autowired
    private IniotConfig config;

    /**
     * 设备管理器
     *
     * @return 设备管理器
     */
    @Override
    public DeviceManagement getDeviceManagement() {
        return deviceManagement;
    }

    /**
     * 消息管理器
     *
     * @return 消息管理器
     */
    @Override
    public MessageManagement getMessageManagement() {
        return messageManagement;
    }

    /**
     * 订阅管理器
     *
     * @return 订阅管理器
     */
    @Override
    public SubscribeManagement getSubscribeManagement() {
        return subscribeManagement;
    }

    @PostConstruct
    public void init() {

        /*
         * ioTClient 客户端
         */
        InIoTClient ioTClient = new AbstractIniotClientImpl() {

            private static final long serialVersionUID = 5527196152642058766L;

            @Override
            public String getApiKey() {
                return config.getApiKey();
            }

            @Override
            public String getApiSecret() {
                return config.getApiSecret();
            }
        };

        deviceManagement = new DeviceManagement(ioTClient);
        messageManagement = new MessageManagement(ioTClient);
        subscribeManagement = new SubscribeManagement(ioTClient);
    }


    /**
     * 获取最近一条数据
     *
     * @param deviceSN 设备信息
     * @return EnvironmentMessageInfo
     * @throws NotSupportedException NotSupportedException
     */
    @Override
    public EnvironmentMessageInfo lastEnvironmentMessage(String deviceSN) throws NotSupportedException {

        try {

            Environment11MessageOutDTO environment11MessageOutDTI = messageManagement.last(deviceSN, "0000", Environment11MessageOutDTO.class);

            return EnvironmentMessageInfo.builder()
                    .temperature(environment11MessageOutDTI.getTemperature())
                    .humidity(environment11MessageOutDTI.getHumidity())
                    .light(environment11MessageOutDTI.getLight())
                    .uvPower(environment11MessageOutDTI.getUvPower())
                    .uv(environment11MessageOutDTI.getUv())
                    .windSpeed(environment11MessageOutDTI.getWindSpeed())
                    .windDirection(environment11MessageOutDTI.getWindDirection())
                    .windPower(environment11MessageOutDTI.getWindPower())
                    .rain(environment11MessageOutDTI.getRain())
                    .pressure(environment11MessageOutDTI.getPressure())
                    .pressureTemperature(environment11MessageOutDTI.getPressureTemperature())
                    .soilTemperature(environment11MessageOutDTI.getSoilTemperature())
                    .soilHumidity(environment11MessageOutDTI.getSoilHumidity())
                    .batteryVoltage(environment11MessageOutDTI.getBatteryVoltage())
                    .dewPoint(environment11MessageOutDTI.getDewPoint())
                    .capacitanceVoltage(environment11MessageOutDTI.getCapacitanceVoltage())
                    .build();


        } catch (Exception ex) {
            ex.printStackTrace();
            throw new NotSupportedException("异常,暂时不支持");
        }

    }
}
