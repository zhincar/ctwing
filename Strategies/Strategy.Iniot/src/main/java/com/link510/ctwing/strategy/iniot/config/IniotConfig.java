package com.link510.ctwing.strategy.iniot.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 环境探测器配置
 *
 * @author cqnews
 */
@Component
@ConfigurationProperties(prefix = "com.link510.ctwing.strategy.iniot.config")
@PropertySource("classpath:iniotconfig.properties")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class IniotConfig implements Serializable {


    private static final long serialVersionUID = -5151401681592180758L;

    /**
     * 平台地址appKey
     */
    private String apiKey = "";
    /**
     * 平台地址appSecret
     */
    private String apiSecret = "";
}
