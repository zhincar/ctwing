/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.web.framework.workcontext;

import lombok.*;

/**
 * 接口上下文
 *
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiWorkContext extends BaseWorkContext {


    private static final long serialVersionUID = -3586372588079126207L;

    /**
     * api账号
     */
    private String apiKey = "";

    /**
     * 密钥
     */
    private String apiSecret = "";

    /**
     * 单位编号
     */
    private String unitCode = "";


}
