package com.link510.ctwing.web.framework.controller;


import com.link510.ctwing.core.exption.CWMAPIException;
import com.link510.ctwing.core.message.MessageInfo;
import org.apache.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.link510.ctwing.core.errors.SateCollect.AUTHOR_FAILED;
import static com.link510.ctwing.core.errors.SateCollect.SUCCESS;

/**
 * 全局自定义异常处理
 *
 * @author cqnews
 */
@ControllerAdvice
public class BaseAdviceController extends BaseWebController {


    /**
     * 拦截捕捉自定义异常 MyException.class
     *
     * @param ex CWMAPIException
     * @return CWMAPIException
     */
    @ResponseBody
    @ExceptionHandler(value = CWMAPIException.class)
    public String apiErrorHandler(CWMAPIException ex) {
        response.setStatus(HttpStatus.SC_FORBIDDEN);
        return JsonView(AUTHOR_FAILED, ex.getMessage());
    }


}
