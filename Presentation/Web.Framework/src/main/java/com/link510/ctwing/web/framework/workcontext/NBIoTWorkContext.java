package com.link510.ctwing.web.framework.workcontext;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NBIoTWorkContext extends BaseWorkContext {
}
