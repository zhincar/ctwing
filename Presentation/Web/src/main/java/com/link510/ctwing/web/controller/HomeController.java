package com.link510.ctwing.web.controller;


import com.link510.ctwing.core.cache.CWMCache;
import com.link510.ctwing.core.config.CWMConfig;
import com.link510.ctwing.services.Authors;
import com.link510.ctwing.services.SMSes;
import com.link510.ctwing.services.Users;
import com.link510.ctwing.web.framework.controller.BaseWebController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller(value = "HomeController")
public class HomeController extends BaseWebController {

    @Autowired
    SMSes smSes;


    @Autowired
    CWMCache cwmCache;

    @Autowired
    Users users;

    @Autowired
    Authors authors;

    @Autowired
    CWMConfig cwmConfig;

    @RequestMapping(value = "/")
    public ModelAndView index() {

        return PromptView("/login", "正在跳转");

    }


    @RequestMapping(value = "/403")
    public ModelAndView err403() {
        return View("403");
    }



}
