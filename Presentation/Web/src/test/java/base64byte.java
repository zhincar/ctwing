import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

public class base64byte {
    public static void main(String args[]) throws IOException {

        StringBuilder sb = new StringBuilder();

        //定义一个BASE64Encoder

        BASE64Encoder encode = new BASE64Encoder();

        //要转换的base64

        String base64 = "WUtOQgL+ABABAAAAACEKAAI=";

        //新建一个BASE64Decoder

        BASE64Decoder decode = new BASE64Decoder();

        //将base64转换为byte[]

        byte[] b = decode.decodeBuffer(base64);

        //打印转换后的byte[]

        System.out.println(new String(b));

        int a = 1;

        // System.out.print( " +++++++++++++++++++++++++++");


        for (byte b1 : b) {

            String hex = Integer.toHexString(b1 & 0xFF);

            if (hex.length() == 1) {

                hex = '0' + hex;

            }

            sb.append(hex.toUpperCase());

            System.out.println(a++ + ":" + hex.toUpperCase());

        }

        System.out.println(sb.toString());


    }

}

