package com.link510.ctwing.web.admin.controller;

import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.domain.message.MessageRecordInfo;
import com.link510.ctwing.core.model.PageModel;
import com.link510.ctwing.services.Messages;
import com.link510.ctwing.web.admin.model.MessageListModel;
import com.link510.ctwing.web.admin.model.MessageShowModel;
import com.link510.ctwing.web.framework.controller.BaseAdminController;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.text.MessageFormat;
import java.util.List;

@Controller(value = "AdminMessageController")
public class MessageController extends BaseAdminController {


    @Autowired
    private Messages messages;


    /**
     * 消息首页
     *
     * @return
     */
    @RequestMapping(value = "message/index")
    public ModelAndView index() {

        return View();

    }

    /**
     * 消息列表
     *
     * @param pageSize   每页条数
     * @param pageNumber 当前页数
     * @param msgId      当前页数
     * @param token      当前页数
     * @param keyword    当前页数
     * @return
     */
    @RequestMapping(value = "message/list")
    public ModelAndView list(@RequestParam(defaultValue = "12") Integer pageSize,
                                @RequestParam(defaultValue = "1") Integer pageNumber,
                                @RequestParam(defaultValue = "0") Integer msgId,
                                @RequestParam(defaultValue = "") String token,
                                @RequestParam(defaultValue = "") String deviceSN,
                                @RequestParam(defaultValue = "") String keyword) { //System.out.println("pageSize:" + pageSize); //System.out.println("pageNumber:" + pageNumber);

        Specification<MessageInfo> condition = messages.getMessageListCondition(msgId, token, deviceSN, keyword);
        Sort sort = new Sort(Sort.Direction.DESC, "msgId");

        Page<MessageInfo> infoPage = messages.getMessageList(pageSize, pageNumber, condition, sort);

        if (infoPage == null) {
            return PromptView("没有更多的数据了");
        }

        List<MessageInfo> messageList = infoPage.getContent();

        PageModel pageModel = new PageModel(pageSize, pageNumber, infoPage.getTotalElements());

        MessageListModel model = new MessageListModel(msgId, token, deviceSN, keyword, messageList, pageModel);

        cwmUtils.setAdminRefererCookie(response, MessageFormat.format("{0}?pageSize={1}&pageNumber={2}&msgId={3}&token={4}&deviceSN={5}&keyword={6}",
                cwmUtils.getRawUrl(), pageSize, pageNumber, msgId, token, deviceSN, keyword));


        return View(model);
    }

    /**
     * 消息详情
     *
     * @return
     */
    @RequestMapping(value = "message/show")
    public ModelAndView show(@RequestParam(defaultValue = "0") Integer msgId) {


        if (msgId <= 0) {
            return PromptView("消息Id不正确");
        }


        MessageInfo messageInfo = messages.getMessageById(msgId);

        if (messageInfo == null || messageInfo.getMsgId() <= 0) {
            return PromptView("消息不正确");
        }

        List<MessageRecordInfo> recordInfoList = messages.getMessageRecordList(msgId);


        MessageShowModel model = new MessageShowModel(messageInfo, recordInfoList);

        return View(model);
    }


}
