/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.web.api.model;

import com.link510.ctwing.core.helper.ValidateHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * 消息订阅提交
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class  UnitSubscribeSendModel implements Serializable {

    private static final long serialVersionUID = -425886854294745862L;
    /**
     * 订阅名称
     */
    @NotNull(message = "请认真填写订阅名称")
    @Length(min = 2, max = 120, message = "请认真填写订阅名称")
    private String title = "";

    /**
     * 产品Id
     */
    @NotNull(message = "请认真填写产品Id")
    @Range(min = 1, message = "请认真填写产品Id")
    private Integer productId = 0;

    /**
     * 协议
     */
    @NotNull(message = "请认真填写订阅协议")
    @Length(min = 2, max = 30, message = "请认真填写订阅协议")
    @Pattern(regexp = ValidateHelper.PROTOCOL_CODE_REGEX, message = "请认真填写订阅协议")
    private String protocol = "";

    /**
     * 协议
     */
    @NotNull(message = "请认真填写订阅地址")
    @Length(min = 2, max = 350, message = "请认真填写订阅地址")
    @Pattern(regexp = ValidateHelper.URL_REGEX, message = "订阅地址不正确")
    private String httpUrl = "";


    /**
     * 数据格式
     */
    @NotNull(message = "请认真填写消息格式")
    @Range(min = 0, max = 1, message = "请认真填写消息格式")
    private Integer format = 0;

}
