/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.web.api.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 订阅删除
 *
 * @author cqnews
 */
@Data
public class UnitSubscribeDeleteModel implements Serializable {

    private static final long serialVersionUID = 3163649957776833058L;
    /**
     * 产品Id
     */
    @NotNull(message = "请认真填写产品Id")
    @Range(min = 1, message = "请认真填写产品Id")
    private Integer productId = 0;

    /**
     * 协议
     */
    @NotNull(message = "请认真填写订阅协议")
    @Length(min = 2, max = 30, message = "请认真填写订阅协议")
    private String protocol = "";

}
