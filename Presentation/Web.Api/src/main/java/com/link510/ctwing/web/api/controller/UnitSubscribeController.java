/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.web.api.controller;

import com.google.common.base.Strings;
import com.link510.ctwing.web.framework.controller.BaseApiController;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.domain.message.MessageRecordInfo;
import com.link510.ctwing.core.domain.product.ProductInfo;
import com.link510.ctwing.core.domain.product.ProductProtocolInfo;
import com.link510.ctwing.core.domain.unit.UnitSubscribeInfo;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import com.link510.ctwing.services.Devices;
import com.link510.ctwing.services.Messages;
import com.link510.ctwing.services.Products;
import com.link510.ctwing.services.Units;
import com.link510.ctwing.web.api.model.UnitSubscribeDeleteModel;
import com.link510.ctwing.web.api.model.UnitSubscribeSendModel;
import com.link510.ctwing.web.framework.controller.BaseApiController;
import com.link510.ctwing.web.framework.validate.ValidateModel;
import com.link510.ctwing.web.framework.validate.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.link510.ctwing.core.errors.SateCollect.SUCCESS;
import static com.link510.ctwing.core.errors.SateCollect.VALIDATION_FAILED;

/**
 * 单位消息订单
 */
@RestController(value = "ApiUnitSubscriptionController")
public class UnitSubscribeController extends BaseApiController {

    @Autowired
    private Units units;

    @Autowired
    private Products products;

    @Autowired
    private Messages messages;

    @Autowired
    private Devices devices;


    /**
     * 消息订阅列表
     *
     * @return
     */
    @RequestMapping(value = "subscribe/list")
    public String list() {

        Specification<UnitSubscribeInfo> condition = units.getUnitSubscribeListCondition(workContext.getUnitCode());

        Sort sort = new Sort(Sort.Direction.DESC, "subId");

        List<UnitSubscribeInfo> subscribeInfoList = units.getUnitSubscribeList(condition, sort);


        if (subscribeInfoList == null || subscribeInfoList.size() <= 0) {
            return JsonView("暂无订阅队列");
        }

        return JsonView(SUCCESS, subscribeInfoList, "列表加载成功");

    }

    /**
     * 消息订阅申请
     *
     * @return
     */
    @RequestMapping(value = "subscribe/send")
    public String send(UnitSubscribeSendModel model) {

        ValidationResult result = ValidateModel.validateEntity(model);

        if (result.isNotErrors()) {

            UnitSubscribeInfo subscribeInfo = units.getUnitSubscribeByProductIdAndProtocol(workContext.getUnitCode(), model.getProductId(), model.getProtocol());

            if (subscribeInfo == null || subscribeInfo.getSubId() < 1) {
                subscribeInfo = new UnitSubscribeInfo();
            }

            ProductInfo productInfo = products.getProductByProductId(model.getProductId());

            if (productInfo == null || productInfo.getProductId() <= 0) {
                return JsonView("产品信息不正确");
            }

            if(Strings.isNullOrEmpty(model.getProtocol())){
                ProductProtocolInfo protocolInfo = products.getDefaultProductProtocolByProductId(productInfo.getProductId());

                if(protocolInfo == null ){
                    return JsonView("没有配置默认的协议");
                }

                model.setProtocol(protocolInfo.getProtocol());
            }

            ProductProtocolInfo protocolInfo = products.getProductProtocolByProductIdAndProtocol(productInfo.getProductId(), model.getProtocol());

            if (protocolInfo == null || protocolInfo.getProtocolId() <= 0) {
                return JsonView("产品协议不正确");
            }


            subscribeInfo.setUnitCode(workContext.getUnitCode());
            subscribeInfo.setTitle(model.getTitle());
            subscribeInfo.setProductId(model.getProductId());
            subscribeInfo.setProtocol(model.getProtocol());
            subscribeInfo.setHttpUrl(model.getHttpUrl());
            subscribeInfo.setFormat(model.getFormat());

            subscribeInfo = units.createUnitSubscribe(subscribeInfo);

            if (subscribeInfo == null || subscribeInfo.getSubId() <= 0) {
                return JsonView("消息订阅失败");
            }

            return JsonView(SUCCESS, "消息订阅成功");

        }

        return JsonView(VALIDATION_FAILED, "订阅失败," + result.toString());

    }

    /**
     * 消息订阅取消申请
     *
     * @return
     */
    @RequestMapping(value = "subscribe/cancel")
    public String cancel(UnitSubscribeDeleteModel model) {


        ValidationResult result = ValidateModel.validateEntity(model);

        if (result.isNotErrors()) {

            UnitSubscribeInfo subscribeInfo = units.getUnitSubscribeByProductIdAndProtocol(workContext.getUnitCode(), model.getProductId(), model.getProtocol());

            if (subscribeInfo == null || subscribeInfo.getSubId() <= 1) {
                return JsonView("无效的订阅消息");
            }

            units.deleteUnitSubscribeBySubId(subscribeInfo.getSubId());

            return JsonView(SUCCESS, "订阅取消息成功");
        }

        return JsonView(VALIDATION_FAILED, "订阅取消失败," + result.toString());
    }


    /**
     * 消息订阅申请
     *
     * @return
     */
    @RequestMapping(value = "subscribe/test")
    public String test(@RequestParam(defaultValue = "") String deviceSN,
                       @RequestParam(defaultValue = "0XB0") String protocol) {

        if (StringHelper.isNullOrWhiteSpace(deviceSN)) {
            return JsonView("设备编号不正确");
        }

        DeviceInfo deviceInfo = devices.getDeviceByDeviceSN(deviceSN);

        if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
            return JsonView("设备不存在");
        }


        MessageInfo messageInfo = messages.getLastMessageByDeviceSNAndProtocol(deviceSN, protocol);

        List<MessageRecordInfo> recordInfoList = new ArrayList<>();

        String successMsg = "发送成功";

        if (messageInfo == null || messageInfo.getMsgId() <= 0) {

            successMsg = "发送成功,模拟消息";
            messageInfo = new MessageInfo();

            messageInfo.setUnitCode(deviceInfo.getUnitCode());
            messageInfo.setDeviceSN(deviceInfo.getDeviceSN());
            messageInfo.setDeviceName(deviceInfo.getName());
            messageInfo.setProductId(deviceInfo.getProductId());
            messageInfo.setProductType(deviceInfo.getProductType());
            messageInfo.setProtocol(protocol);
            messageInfo.setProtodesc("模拟消息");
            messageInfo.setTimestamp(UnixTimeHelper.getUnixTimeStamp());


        } else {
            recordInfoList = messages.getMessageRecordList(messageInfo.getMsgId());
        }

        messages.outUnitSubscribe(deviceInfo, messageInfo, recordInfoList);

        return JsonView(SUCCESS, successMsg);

    }
}
