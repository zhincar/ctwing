package com.link510.ctwing.web.api.controller;

import com.google.common.base.Strings;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import com.link510.ctwing.core.errors.SateCollect;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import com.link510.ctwing.services.Devices;
import com.link510.ctwing.web.api.model.DeviceLoginModel;
import com.link510.ctwing.web.framework.controller.BaseApiController;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 设备管理
 *
 * @author cqnews
 */
@RestController(value = "ApiDeviceController")
public class DeviceController extends BaseApiController {

    @Autowired
    private Devices devices;

    /**
     * 摄像机登录
     *
     * @return
     */
    @RequestMapping(value = "device/login")
    @RequiresGuest
    public String login(@RequestParam(defaultValue = "5050000001") String deviceSN,
                        @RequestParam(defaultValue = "123456") String password) {


        if (Strings.isNullOrEmpty(deviceSN)) {
            return JsonView("设备编号异常");
        }

        DeviceInfo deviceInfo = devices.getDeviceByDeviceSN(deviceSN);

        if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
            return JsonView("设备信息异常");
        }

        String newPassword = devices.createDevicePassword(password, deviceInfo.getSalt());

        if (newPassword.equals(deviceInfo.getPassword())) {

            Integer limitTime = UnixTimeHelper.getUnixTimeStamp() + 3600 / 2;


            String token = devices.createDeviceToken(deviceSN, deviceInfo.getSalt(), limitTime);

            String refreshToken = devices.createRefreshToken(token);

            deviceInfo.setSalt("*******");
            deviceInfo.setPassword("*******");

            DeviceLoginModel model = DeviceLoginModel
                    .builder()
                    .limitTime(limitTime)
                    .deviceSN(deviceSN)
                    .token(token)
                    .refreshToken(refreshToken)
                    .deviceInfo(deviceInfo)
                    .build();

            return JsonView(SateCollect.SUCCESS, model, "摄像机登录成功");
        }


        return JsonView("设备登录失败");

    }


}
