package com.link510.ctwing.web.api.model;

import com.link510.ctwing.core.domain.device.DeviceInfo;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 设备登录模型
 *
 * @author cqnews
 */
@Data
@Builder
public class DeviceLoginModel implements Serializable {

    /**
     * 设备编号
     */
    @Builder.Default
    private String deviceSN = "";

    /**
     * 超时时间
     */
    @Builder.Default
    private Integer limitTime = 0;

    /**
     * 设备密码
     */
    @Builder.Default
    private String token = "";

    /**
     * 刷新token
     */
    @Builder.Default
    private String refreshToken = "";

    /**
     * 设备信息
     */
    private DeviceInfo deviceInfo;

}
