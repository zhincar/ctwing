package com.link510.ctwing.data;

import com.link510.ctwing.core.ucenter.CWMUCenter;
import com.cqwo.ucenter.client.domain.OauthInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *第三方登录
 * @author cqnews
 * @date 2017/4/11
 */
@Service(value = "OauthsData")
public class Oauthds extends DataService {

    @Autowired
    CWMUCenter cwmuCenter;

    /**
     * 通过appid和uid查询第三方登录信息
     *
     * @param uid uid
     * @return
     */
    @Cacheable(value = "findOauthByUid", key = "#uid")
    public OauthInfo findOauthByUid(String uid) throws Exception {


        return cwmuCenter.getIUCenterStrategy().findOauthByUid(uid);

    }

}
