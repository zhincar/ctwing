package com.link510.ctwing.data;

import com.link510.ctwing.core.cache.CWMCache;
import com.link510.ctwing.core.config.CWMConfig;
import com.link510.ctwing.core.data.CWMData;
import com.link510.ctwing.core.sms.CWMSMS;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;


@Data
public class DataService {

    @Autowired
    private CWMCache cwmCache;

    @Autowired
    private CWMData cwmData;

    @Autowired
    private CWMSMS cwmSMS;

    @Autowired
    private CWMConfig cwmConfig;


}
