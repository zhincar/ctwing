package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.device.DeviceInfo;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//设备
@Service(value = "DevicesData")
public class Deviceds extends DataService {


    //region  设备方法

    /**
     * 获得设备数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getDeviceCount(Specification<DeviceInfo> condition) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceCount(condition);
    }

    /**
     * 创建一条设备数据
     *
     * @param deviceInfo 设备模型
     * @return 返回创建信息
     **/
    public DeviceInfo createDevice(DeviceInfo deviceInfo) throws IOException {
        return getCwmData().getIDeviceStrategy().createDevice(deviceInfo);
    }

    /**
     * 校验主要的数据是否存在
     *
     * @param productId 产品id
     * @param deviceSN  设备编号
     * @return
     */
    public boolean isDeviceExists(Integer productId, String deviceSN) throws IOException {
        return getCwmData().getIDeviceStrategy().isDeviceExists(productId, deviceSN);
    }

    /**
     * 更新一条设备数据
     *
     * @param deviceInfo 设备模型
     **/

    @CachePut(value = "getDeviceById", key = "#deviceInfo.deviceId")
    public DeviceInfo updateDevice(DeviceInfo deviceInfo) throws IOException {
        return getCwmData().getIDeviceStrategy().updateDevice(deviceInfo);
    }

    /**
     * 删除一条设备数据
     *
     * @param deviceId 设备模型
     **/
    public boolean deleteDeviceByDeviceId(int deviceId) throws IOException {
        return getCwmData().getIDeviceStrategy().deleteDeviceByDeviceId(deviceId);
    }

    /**
     * 设备恢复
     *
     * @param deviceId 设备Id
     * @return
     */
    public boolean recoverDeviceByDeviceId(Integer deviceId) throws IOException {
        return getCwmData().getIDeviceStrategy().recoverDeviceByDeviceId(deviceId);
    }

    /**
     * 批量删除一批设备数据
     **/
    public void deleteDeviceByIdList(String idList) throws IOException {
        getCwmData().getIDeviceStrategy().deleteDeviceByIdList(idList);
    }

    /**
     * 获取一条设备数据
     *
     * @param deviceId 设备模型
     **/
    @Cacheable(value = "getDeviceById", key = "#deviceId")
    public DeviceInfo getDeviceById(int deviceId) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceById(deviceId);
    }

    /**
     * 获取一条无缓存设备信息
     *
     * @param deviceId 设备Id
     * @return
     */
    public DeviceInfo getNoCacheDeviceById(Integer deviceId) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceById(deviceId);
    }


    /**
     * 通过产品id和设备编号获取设备信息
     *
     * @param productId 产品id
     * @param deviceSN  产品编号
     * @return 返回设备信息
     */
    public DeviceInfo getDeviceByProductIdAndDeviceSN(Integer productId, String deviceSN) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceByProductIdAndDeviceSN(productId, deviceSN);
    }

    /**
     * 获得设备数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回DeviceInfo
     **/
    public List<DeviceInfo> getDeviceList(Specification<DeviceInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceList(condition, sort);
    }


    /**
     * 获得设备数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回DeviceInfo
     **/
    public Page<DeviceInfo> getDeviceList(Integer pageSize, Integer pageNumber, Specification<DeviceInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceList(pageSize, pageNumber, condition, sort);
    }




    /**
     * 查询设备
     *
     * @param deviceSN
     * @return
     * @throws IOException
     */
    @Cacheable(value = "getDeviceByDeviceSN", key = "#deviceSN")
    public DeviceInfo getDeviceByDeviceSN(String deviceSN) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceByDeviceSN(deviceSN);
    }


    /**
     * 查询NB设备信息
     *
     * @param token
     * @throws IOException
     */
    public DeviceInfo getNbDeviceByToken(String token) throws IOException {
        return getCwmData().getIDeviceStrategy().getNbDeviceByToken(token);
    }

    /**
     * 查询设备信息
     *
     * @param imsi
     * @return
     * @throws IOException
     */
    public DeviceInfo getDeviceByImsi(String imsi) throws IOException {
        return getCwmData().getIDeviceStrategy().getDeviceByImsi(imsi);
    }

    /**
     * 物理删除设备
     *
     * @param deviceId
     * @throws IOException
     */
    public void deletePhysicsDeviceByDeviceId(Integer deviceId) throws IOException {
        getCwmData().getIDeviceStrategy().deletePhysicsDeviceByDeviceId(deviceId);
    }



    //endregion

}
