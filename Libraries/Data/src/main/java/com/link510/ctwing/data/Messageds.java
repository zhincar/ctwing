package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.message.MessageInfo;
import com.link510.ctwing.core.domain.message.MessageRecordInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 *消息
 * @author cqnews
 * @date 2017/4/11
 */
@Service(value = "MessagesData")
public class Messageds extends DataService {


    //region  消息方法

    /**
     * 获得消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getMessageCount(Specification<MessageInfo> condition) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageCount(condition);
    }

    /**
     * 创建一条消息数据
     *
     * @param messageInfo 消息模型
     * @return 返回创建信息
     **/
    public MessageInfo createMessage(MessageInfo messageInfo) throws IOException {
        return getCwmData().getIMessageStrategy().createMessage(messageInfo);
    }

    /**
     * 更新一条消息数据
     *
     * @param messageInfo 消息模型
     **/
    public MessageInfo updateMessage(MessageInfo messageInfo) throws IOException {
        return getCwmData().getIMessageStrategy().updateMessage(messageInfo);
    }

    /**
     * 删除一条消息数据
     *
     * @param id 消息模型
     **/
    public void deleteMessageById(int id) throws IOException {
        getCwmData().getIMessageStrategy().deleteMessageById(id);
    }

    /**
     * 批量删除一批消息数据
     **/
    public void deleteMessageByIdList(String idList) throws IOException {
        getCwmData().getIMessageStrategy().deleteMessageByIdList(idList);
    }

    /**
     * 获取一条消息数据
     *
     * @param id 消息模型
     **/
    public MessageInfo getMessageById(int id) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageById(id);
    }

    /**
     * 获取最后一条数据
     *
     * @param deviceSN 设备编号
     * @param token    token
     * @return 返回
     */
    public MessageInfo getLastMessageByTokenAndDeviceSN(String token, String deviceSN) throws IOException {
        return getCwmData().getIMessageStrategy().getLastMessageByTokenAndDeviceSN(token, deviceSN);
    }

    /**
     * 获取最后一条数据
     *
     * @param deviceSN  设备编号
     * @param protocols 协议名称
     * @return 返回
     */
    public MessageInfo getLastMessageByDeviceSNAndProtocol(String deviceSN, Integer timestamp, String... protocols) throws IOException {
        return getCwmData().getIMessageStrategy().getLastMessageByDeviceSNAndProtocol(deviceSN, timestamp, protocols);
    }


    /**
     * 获得消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageInfo
     **/
    public List<MessageInfo> getMessageList(Specification<MessageInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageList(condition, sort);
    }


    /**
     * 获得消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageInfo
     **/
    public Page<MessageInfo> getMessageList(Integer pageSize, Integer pageNumber, Specification<MessageInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageList(pageSize, pageNumber, condition, sort);
    }


    /**
     * 统计在指定的时间内的消息数目
     *
     * @param deviceSN     设备id
     * @param productId
     * @param intervalTime 时间段
     * @return 返回
     */
    public long countIntervalMessage(String deviceSN, Integer productId, String protocol, Integer intervalTime) throws IOException {
        return getCwmData().getIMessageStrategy().countAllIntervalMessage(deviceSN, productId, protocol, intervalTime);
    }

    /**
     * 统计token的心跳次数
     *
     * @param token        网关
     * @param type         消息类型
     * @param intervalTime 时间段
     */
    public long countTokenIntervalMessage(String token, Integer type, Integer intervalTime) throws IOException {
        return getCwmData().getIMessageStrategy().countTokenIntervalMessage(token, type, intervalTime);
    }

    /**
     * 统计在指定的时间内的消息数目
     *
     * @param msgId
     * @param productId
     * @param deviceSN     设备编号
     * @param intervalTime 时间段
     * @return 返回
     */
    public long countIntervalMessage(Integer msgId, Integer productId, String deviceSN, String protocol, Integer intervalTime) throws IOException {
        return getCwmData().getIMessageStrategy().countAllIntervalMessage(msgId, productId, deviceSN, protocol, intervalTime);
    }

    /**
     * 统计警告设备消息
     *
     * @return 返回 long
     */
    public long countWarningAllMessage() throws IOException {
        return getCwmData().getIMessageStrategy().countWarningAllMessage();
    }

    /**
     * 让消息失效
     *
     * @param msgId 消息id
     */
    public void lose(Integer msgId) throws IOException {
        getCwmData().getIMessageStrategy().lose(msgId);
    }

    /**
     * 通过tokem查询消息列表
     *
     * @param token
     * @return 返回
     */
    public List<MessageInfo> getMessageByToken(String token) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageByToken(token);
    }

    /**
     * 更新消息图片
     *
     * @param msgId  消息id
     * @param litpic 图片
     */
    public void updateMessageLitpic(Integer msgId, String litpic) throws IOException {
        getCwmData().getIMessageStrategy().updateMessageLitpic(msgId, litpic);
    }

    /**
     * 获取最后一包心跳包时间
     *
     * @param token
     * @return 返回
     */
    public Integer getLastHeartMessageTimeByToken(String token) throws IOException {
        return getCwmData().getIMessageStrategy().getLastHeartMessageTimeByToken(token);
    }

    /**
     * 获取最后一包数据包时间
     *
     * @param deviceSN 设备编号
     * @return 返回
     */
    public Integer getLastDataMessageTimeByDeviceSN(String deviceSN) throws IOException {
        return getCwmData().getIMessageStrategy().getLastDataMessageTimeByDeviceSN(deviceSN);
    }


    /**
     * 获得消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageRecordInfo
     **/
    public List<MessageRecordInfo> getMessageRecordList(Specification<MessageRecordInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageRecordList(condition, sort);
    }
    //endregion

}
