package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.message.MessageImageInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//图片消息
@Service(value = "MessageImagesData")
public class MessageImageds extends DataService {


    //region  图片消息方法

    /**
     * 获得图片消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getMessageImageCount(Specification<MessageImageInfo> condition) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageImageCount(condition);
    }

    /**
     * 创建一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     * @return 返回创建信息
     **/
    public MessageImageInfo createMessageImage(MessageImageInfo messageimageInfo) throws IOException {
        return getCwmData().getIMessageStrategy().createMessageImage(messageimageInfo);
    }

    /**
     * 更新一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     **/
    public MessageImageInfo updateMessageImage(MessageImageInfo messageimageInfo) throws IOException {
        return getCwmData().getIMessageStrategy().updateMessageImage(messageimageInfo);
    }

    /**
     * 删除一条图片消息数据
     *
     * @param imgId 图片消息模型
     **/
    public void deleteMessageImageByImgId(Integer imgId) throws IOException {
        getCwmData().getIMessageStrategy().deleteMessageImageByImgId(imgId);
    }

    /**
     * 批量删除一批图片消息数据
     **/
    public void deleteMessageImageByImgIdList(String imgIdList) throws IOException {
        getCwmData().getIMessageStrategy().deleteMessageImageByImgIdList(imgIdList);
    }

    /**
     * 获取一条图片消息数据
     *
     * @param imgId 图片消息模型
     **/
    public MessageImageInfo getMessageImageByImgId(Integer imgId) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageImageByImgId(imgId);
    }


    /**
     * 获得图片消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageImageInfo
     **/
    public List<MessageImageInfo> getMessageImageList(Specification<MessageImageInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageImageList(condition, sort);
    }


    /**
     * 获得图片消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageImageInfo
     **/
    public Page<MessageImageInfo> getMessageImageList(Integer pageSize, Integer pageNumber, Specification<MessageImageInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageImageList(pageSize, pageNumber, condition, sort);
    }

    /**
     * 更新图片库状态
     *
     * @param state     类型
     * @param type      类型
     * @param deviceSN  设备编号
     * @param shortTime 分组时间
     */
    public void updateMessageImageSate(Integer state, Integer type, String deviceSN, String shortTime) throws IOException {
        getCwmData().getIMessageStrategy().updateMessageImageSate(state, type, deviceSN, shortTime);
    }


    //endregion

}
