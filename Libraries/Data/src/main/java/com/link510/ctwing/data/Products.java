package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.product.ProductInfo;
import com.link510.ctwing.core.domain.product.ProductProtocolInfo;
import com.link510.ctwing.core.domain.product.ProductProtocolParamInfo;
import com.link510.ctwing.core.enums.message.MsgLevel;
import com.link510.ctwing.core.helper.TypeHelper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//产品库
@Service(value = "ProductsData")
public class Products extends DataService {


    //region  产品库方法

    /**
     * 获得产品库数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getProductCount(Specification<ProductInfo> condition) throws IOException {
        return getCwmData().getIProductStrategy().getProductCount(condition);
    }

    /**
     * 创建一条产品库数据
     *
     * @param productInfo 产品库模型
     * @return 返回创建信息
     **/
    public ProductInfo createProduct(ProductInfo productInfo) throws IOException {
        return getCwmData().getIProductStrategy().createProduct(productInfo);
    }

    /**
     * 更新一条产品库数据
     *
     * @param productInfo 产品库模型
     **/
    @CacheEvict(value = "product", key = "#productInfo.productId")
    public ProductInfo updateProduct(ProductInfo productInfo) throws IOException {
        return getCwmData().getIProductStrategy().updateProduct(productInfo);
    }

    /**
     * 删除一条产品库数据
     *
     * @param productId 产品库模型
     **/
    public void deleteProductByProductId(int productId) throws IOException {
        getCwmData().getIProductStrategy().deleteProductByProductId(productId);
    }

    /**
     * 批量删除一批产品库数据
     **/
    public void deleteProductByProductIdList(String productIdList) throws IOException {
        getCwmData().getIProductStrategy().deleteProductByProductIdList(productIdList);
    }

    /**
     * 获取一条产品库数据
     *
     * @param productId 产品库模型
     **/

    @Cacheable(value = "product", key = "#productId")
    public ProductInfo getProductByProductId(int productId) throws IOException {
        return getCwmData().getIProductStrategy().getProductByProductId(productId);
    }


    /**
     * 获得产品库数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductInfo
     **/
    public List<ProductInfo> getProductInfoList(Specification<ProductInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIProductStrategy().getProductInfoList(condition, sort);
    }


    /**
     * 获得产品库数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductInfo
     **/
    public Page<ProductInfo> getProductInfoList(Integer pageSize, Integer pageNumber, Specification<ProductInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIProductStrategy().getProductInfoList(pageSize, pageNumber, condition, sort);
    }


    /**
     * 获取产品的协议列表
     *
     * @param productId 产品id
     * @return
     */
    public List<ProductProtocolInfo> getProductProtocolList(Integer productId) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolList(productId);
    }
    //endregion

    //region  产品协议方法

    /**
     * 获得产品协议数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getProductProtocolCount(Specification<ProductProtocolInfo> condition) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolCount(condition);
    }

    /**
     * 创建一条产品协议数据
     *
     * @param productprotoclInfo 产品协议模型
     * @return 返回创建信息
     **/
    public ProductProtocolInfo createProductProtocol(ProductProtocolInfo productprotoclInfo) throws IOException {
        return getCwmData().getIProductStrategy().createProductProtocol(productprotoclInfo);
    }

    /**
     * 更新一条产品协议数据
     *
     * @param productprotoclInfo 产品协议模型
     **/
    public ProductProtocolInfo updateProductProtocol(ProductProtocolInfo productprotoclInfo) throws IOException {
        return getCwmData().getIProductStrategy().updateProductProtocol(productprotoclInfo);
    }

    /**
     * 删除一条产品协议数据
     *
     * @param protoclId 产品协议模型
     **/
    public void deleteProductProtocolByProtoclId(int protoclId) throws IOException {
        getCwmData().getIProductStrategy().deleteProductProtocolByProtoclId(protoclId);
    }

    /**
     * 批量删除一批产品协议数据
     **/
    public void deleteProductProtocolByProtoclIdList(String protoclIdList) throws IOException {
        getCwmData().getIProductStrategy().deleteProductProtocolByProtoclIdList(protoclIdList);
    }

    /**
     * 获取一条产品协议数据
     *
     * @param protoclId 产品协议模型
     **/
    public ProductProtocolInfo getProductProtocolByProtoclId(int protoclId) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolByProtoclId(protoclId);
    }


    /**
     * 获得产品协议数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductProtocolInfo
     **/
    public List<ProductProtocolInfo> getProductProtocolList(Specification<ProductProtocolInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolList(condition, sort);
    }


    /**
     * 获得产品协议数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductProtocolInfo
     **/
    public Page<ProductProtocolInfo> getProductProtocolList(Integer pageSize, Integer pageNumber, Specification<ProductProtocolInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolList(pageSize, pageNumber, condition, sort);
    }


    //endregion

    //region  协议参数方法

    /**
     * 获得协议参数数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getProductProtocolParamCount(Specification<ProductProtocolParamInfo> condition) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolParamCount(condition);
    }

    /**
     * 创建一条协议参数数据
     *
     * @param productprotoclparamInfo 协议参数模型
     * @return 返回创建信息
     **/
    public ProductProtocolParamInfo createProductProtocolParam(ProductProtocolParamInfo productprotoclparamInfo) throws IOException {
        return getCwmData().getIProductStrategy().createProductProtocolParam(productprotoclparamInfo);
    }

    /**
     * 更新一条协议参数数据
     *
     * @param productprotoclparamInfo 协议参数模型
     **/
    public ProductProtocolParamInfo updateProductProtocolParam(ProductProtocolParamInfo productprotoclparamInfo) throws IOException {
        return getCwmData().getIProductStrategy().updateProductProtocolParam(productprotoclparamInfo);
    }

    /**
     * 删除一条协议参数数据
     *
     * @param paramId 协议参数模型
     **/
    public void deleteProductProtocolParamByParamId(int paramId) throws IOException {
        getCwmData().getIProductStrategy().deleteProductProtocolParamByParamId(paramId);
    }

    /**
     * 批量删除一批协议参数数据
     **/
    public void deleteProductProtocolParamByParamIdList(String paramIdList) throws IOException {
        getCwmData().getIProductStrategy().deleteProductProtocolParamByParamIdList(paramIdList);
    }

    /**
     * 获取一条协议参数数据
     *
     * @param paramId 协议参数模型
     **/
    public ProductProtocolParamInfo getProductProtocolParamByParamId(int paramId) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolParamByParamId(paramId);
    }


    /**
     * 获得协议参数数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductProtocolParamInfo
     **/
    public List<ProductProtocolParamInfo> getProductProtocolParamList(Specification<ProductProtocolParamInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolParamList(condition, sort);
    }


    /**
     * 获得协议参数数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductProtocolParamInfo
     **/
    public Page<ProductProtocolParamInfo> getProductProtocolParamList(Integer pageSize, Integer pageNumber, Specification<ProductProtocolParamInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolParamList(pageSize, pageNumber, condition, sort);
    }

    /**
     * 通读产品协议参数列表
     *
     * @param protocolId 协议id
     * @return
     */
    public List<ProductProtocolParamInfo> getProductProtocolParamList(Integer protocolId) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolParamList(protocolId);
    }

    /**
     * 通读产品协议参数列表
     *
     * @param productId 产品Id
     * @return
     */
    public List<ProductProtocolParamInfo> getProductProtocolParamListByProductId(Integer productId) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolParamListByProductId(productId);
    }

    public List<ProductInfo> findSimpleModel() {
        return getCwmData().getIProductStrategy().findSimpleModel();
    }

    /**
     * 获取所有的产器列表
     *
     * @param sort
     * @return
     */
    public List<ProductInfo> getAllProductList(Sort sort) throws IOException {

        return getCwmData().getIProductStrategy().getAllProductList(sort);
    }

    /**
     * 通过token获取产品信息
     *
     * @param secretkey token
     * @return
     */
    @Cacheable(value = "product", key = "#secretkey")
    public ProductInfo getProductBySecretkey(String secretkey) throws IOException {
        return getCwmData().getIProductStrategy().getProductBySecretkey(secretkey);
    }

    /**
     * 获取产品id的协议
     *
     * @param productId 产品id
     * @param protocol  协议
     * @return
     */
    @Cacheable(value = "productprotocol", key = "#productId+#protocol")
    public ProductProtocolInfo getProductProtocolByProductIdAndProtocol(Integer productId, String protocol) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolByProductIdAndProtocol(productId, protocol);
    }

    /**
     * 获取产品的默认协议
     *
     * @param productId 产品id
     * @return
     */
    public ProductProtocolInfo getDefaultProductProtocolByProductId(Integer productId) throws IOException {
        return getCwmData().getIProductStrategy().getDefaultProductProtocolByProductId(productId);
    }

    /**
     * 获取参数的协议
     *
     * @param productId 产品Id
     * @param protocol  协议
     */
    @Deprecated
    public ProductProtocolInfo getProductProtocolByProtocl(Integer productId, String protocol) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolByProtocl(productId, protocol);
    }

    /**
     * 获取产品的名称的协议
     *
     * @param productId 产品Id
     */
    public String getProductNameByProductId(String productId) throws IOException {
        return getProductByProductId(TypeHelper.stringToInt(productId)).getName();
    }

    /**
     * 查找产品信息
     *
     * @param productSN
     * @return
     * @throws IOException
     */
    public ProductInfo getProductByProductSN(String productSN) throws IOException {
        return getCwmData().getIProductStrategy().getProductByProductSN(productSN);
    }

    /**
     * 查询协议参数
     *
     * @param protocolId
     * @throws IOException
     */
    public List<ProductProtocolParamInfo> getProductProtocolParamByProtocolId(Integer protocolId) throws IOException {
        return getCwmData().getIProductStrategy().getProductProtocolParamByProtocolId(protocolId);
    }

    /**
     * 获取产品所有协议
     *
     * @param productId
     * @return
     * @throws IOException
     */
    public List<ProductProtocolInfo> getAllProtocolByProtoclId(Integer productId) throws IOException {
        return getCwmData().getIProductStrategy().getAllProtocolByProtoclId(productId);
    }


    /**
     * @return
     */
    public ProductProtocolInfo getProductDefaultProtocolInfo() {


        ProductProtocolInfo productProtocolInfo = new ProductProtocolInfo();
        productProtocolInfo.setName("未定义");
        productProtocolInfo.setMsgLevel(MsgLevel.Other.getLevel());
        return productProtocolInfo;
    }

    /**
     * 删除产品协议和参数
     *
     * @param productId 产品Id
     * @return boolean
     */
    public boolean deleteProductProtocolByProductId(Integer productId) throws IOException {
        return getCwmData().getIProductStrategy().deleteProductProtocolByProductId(productId);
    }


    //endregion

}
