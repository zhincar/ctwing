package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.message.MessageRecordInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//消息解析记录
@Service(value = "MessageRecordsData")
public class MessageRecordds extends DataService {


    //region  消息解析记录方法

    /**
     * 获得消息解析记录数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getMessageRecordCount(Specification<MessageRecordInfo> condition) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageRecordCount(condition);
    }

    /**
     * 创建一条消息解析记录数据
     *
     * @param messagerecordInfo 消息解析记录模型
     * @return 返回创建信息
     **/
    public MessageRecordInfo createMessageRecord(MessageRecordInfo messagerecordInfo) throws IOException {
        return getCwmData().getIMessageStrategy().createMessageRecord(messagerecordInfo);
    }

    /**
     * 更新一条消息解析记录数据
     *
     * @param messagerecordInfo 消息解析记录模型
     **/
    public MessageRecordInfo updateMessageRecord(MessageRecordInfo messagerecordInfo) throws IOException {
        return getCwmData().getIMessageStrategy().updateMessageRecord(messagerecordInfo);
    }

    /**
     * 删除一条消息解析记录数据
     *
     * @param recordId 消息解析记录模型
     **/
    public void deleteMessageRecordByRecordId(int recordId) throws IOException {
        getCwmData().getIMessageStrategy().deleteMessageRecordByRecordId(recordId);
    }

    /**
     * 批量删除一批消息解析记录数据
     **/
    public void deleteMessageRecordByRecordIdList(String recordIdList) throws IOException {
        getCwmData().getIMessageStrategy().deleteMessageRecordByRecordIdList(recordIdList);
    }

    /**
     * 获取一条消息解析记录数据
     *
     * @param recordId 消息解析记录模型
     **/
    public MessageRecordInfo getMessageRecordByRecordId(int recordId) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageRecordByRecordId(recordId);
    }


    /**
     * 获得消息解析记录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageRecordInfo
     **/
    public List<MessageRecordInfo> getMessageRecordList(Specification<MessageRecordInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageRecordList(condition, sort);
    }


    /**
     * 获得消息解析记录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageRecordInfo
     **/
    public Page<MessageRecordInfo> getMessageRecordList(Integer pageSize, Integer pageNumber, Specification<MessageRecordInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageRecordList(pageSize, pageNumber, condition, sort);
    }

    /**
     * 获取消息的解析列表
     *
     * @param msgId 消息id
     * @return 返回
     */
    public List<MessageRecordInfo> getMessageRecordList(Integer msgId) throws IOException {
        return getCwmData().getIMessageStrategy().getMessageRecordList(msgId);
    }


    //endregion

}
