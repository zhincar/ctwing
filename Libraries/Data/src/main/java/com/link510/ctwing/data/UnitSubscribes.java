/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.unit.UnitSubscribeInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//单位订阅
@Service(value = "UnitSubscribesData")
public class UnitSubscribes extends DataService {


    //region  单位订阅方法

    /**
     * 获得单位订阅数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getUnitSubscribeCount(Specification<UnitSubscribeInfo> condition) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitSubscribeCount(condition);
    }

    /**
     * 创建一条单位订阅数据
     *
     * @param unitSubscribeInfo 单位订阅模型
     * @return 返回创建信息
     **/
    public UnitSubscribeInfo createUnitSubscribe(UnitSubscribeInfo unitSubscribeInfo) throws IOException {
        return getCwmData().getIUnitStrategy().createUnitSubscribe(unitSubscribeInfo);
    }

    /**
     * 更新一条单位订阅数据
     *
     * @param unitSubscribeInfo 单位订阅模型
     **/
    public UnitSubscribeInfo updateUnitSubscribe(UnitSubscribeInfo unitSubscribeInfo) throws IOException {
        return getCwmData().getIUnitStrategy().updateUnitSubscribe(unitSubscribeInfo);
    }

    /**
     * 删除一条单位订阅数据
     *
     * @param subId 单位订阅模型
     **/
    public void deleteUnitSubscribeBySubId(int subId) throws IOException {
        getCwmData().getIUnitStrategy().deleteUnitSubscribeBySubId(subId);
    }

    /**
     * 批量删除一批单位订阅数据
     **/
    public void deleteUnitSubscribeBySubIdList(String subIdList) throws IOException {
        getCwmData().getIUnitStrategy().deleteUnitSubscribeBySubIdList(subIdList);
    }

    /**
     * 获取一条单位订阅数据
     *
     * @param subId 单位订阅模型
     **/
    public UnitSubscribeInfo getUnitSubscribeBySubId(int subId) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitSubscribeBySubId(subId);
    }


    /**
     * 获得单位订阅数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UnitSubscriptionInfo
     **/
    public List<UnitSubscribeInfo> getUnitSubscribeList(Specification<UnitSubscribeInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitSubscribeList(condition, sort);
    }


    /**
     * 获得单位订阅数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UnitSubscriptionInfo
     **/
    public Page<UnitSubscribeInfo> getUnitSubscribeList(Integer pageSize, Integer pageNumber, Specification<UnitSubscribeInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitSubscribeList(pageSize, pageNumber, condition, sort);
    }

    /**
     * 通过产品Id和产品协议读取订阅消息
     *
     * @param unitCode  单位编码
     * @param productId 产品Id
     * @param protocol  协议
     * @return
     */
    public UnitSubscribeInfo getUnitSubscribeByProductIdAndProtocol(String unitCode, Integer productId, String protocol) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitSubscribeByProductIdAndProtocol(unitCode, productId, protocol);
    }


    //endregion

}
