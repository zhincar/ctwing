package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.message.HeartMessageInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//心跳消息
@Service(value = "HeartMessagesData")
public class HeartMessageds extends DataService {


    //region  心跳消息方法

    /**
     * 获得心跳消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getHeartMessageCount(Specification<HeartMessageInfo> condition) throws IOException {
        return getCwmData().getIMessageStrategy().getHeartMessageCount(condition);
    }

    /**
     * 创建一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     * @return 返回创建信息
     **/
    public HeartMessageInfo createHeartMessage(HeartMessageInfo heartmessageInfo) throws IOException {
        return getCwmData().getIMessageStrategy().createHeartMessage(heartmessageInfo);
    }

    /**
     * 更新一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     **/
    public HeartMessageInfo updateHeartMessage(HeartMessageInfo heartmessageInfo) throws IOException {
        return getCwmData().getIMessageStrategy().updateHeartMessage(heartmessageInfo);
    }

    /**
     * 删除一条心跳消息数据
     *
     * @param msgId 心跳消息模型
     **/
    public void deleteHeartMessageByMsgId(int msgId) throws IOException {
        getCwmData().getIMessageStrategy().deleteHeartMessageByMsgId(msgId);
    }

    /**
     * 批量删除一批心跳消息数据
     **/
    public void deleteHeartMessageByMsgIdList(String msgIdList) throws IOException {
        getCwmData().getIMessageStrategy().deleteHeartMessageByMsgIdList(msgIdList);
    }

    /**
     * 获取一条心跳消息数据
     *
     * @param msgId 心跳消息模型
     **/
    public HeartMessageInfo getHeartMessageByMsgId(int msgId) throws IOException {
        return getCwmData().getIMessageStrategy().getHeartMessageByMsgId(msgId);
    }


    /**
     * 获得心跳消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回HeartMessageInfo
     **/
    public List<HeartMessageInfo> getHeartMessageList(Specification<HeartMessageInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getHeartMessageList(condition, sort);
    }


    /**
     * 获得心跳消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回HeartMessageInfo
     **/
    public Page<HeartMessageInfo> getHeartMessageList(Integer pageSize, Integer pageNumber, Specification<HeartMessageInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getHeartMessageList(pageSize, pageNumber, condition, sort);
    }


    //endregion

}
