package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.message.OutputInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//输出日志
@Service(value = "OutputsData")
public class Outputs extends DataService {


    //region  输出日志方法

    /**
     * 获得输出日志数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getOutputCount(Specification<OutputInfo> condition) throws IOException {
        return getCwmData().getIMessageStrategy().getOutputCount(condition);
    }

    /**
     * 创建一条输出日志数据
     *
     * @param outputInfo 输出日志模型
     * @return 返回创建信息
     **/
    public OutputInfo createOutput(OutputInfo outputInfo) throws IOException {
        return getCwmData().getIMessageStrategy().createOutput(outputInfo);
    }

    /**
     * 更新一条输出日志数据
     *
     * @param outputInfo 输出日志模型
     **/
    public OutputInfo updateOutput(OutputInfo outputInfo) throws IOException {
        return getCwmData().getIMessageStrategy().updateOutput(outputInfo);
    }

    /**
     * 删除一条输出日志数据
     *
     * @param logId 输出日志模型
     **/
    public void deleteOutputByLogId(Integer logId) throws IOException {
        getCwmData().getIMessageStrategy().deleteOutputByLogId(logId);
    }

    /**
     * 批量删除一批输出日志数据
     **/
    public void deleteOutputByLogIdList(String logIdList) throws IOException {
        getCwmData().getIMessageStrategy().deleteOutputByLogIdList(logIdList);
    }

    /**
     * 获取一条输出日志数据
     *
     * @param logId 输出日志模型
     **/
    public OutputInfo getOutputByLogId(Integer logId) throws IOException {
        return getCwmData().getIMessageStrategy().getOutputByLogId(logId);
    }


    /**
     * 获得输出日志数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OutputInfo
     **/
    public List<OutputInfo> getOutputList(Specification<OutputInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getOutputList(condition, sort);
    }


    /**
     * 获得输出日志数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OutputInfo
     **/
    public Page<OutputInfo> getOutputList(Integer pageSize, Integer pageNumber, Specification<OutputInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIMessageStrategy().getOutputList(pageSize, pageNumber, condition, sort);
    }


    //endregion

}
