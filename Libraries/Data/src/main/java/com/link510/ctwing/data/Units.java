package com.link510.ctwing.data;

import com.link510.ctwing.core.domain.unit.UnitInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */


//单位
@Service(value = "UnitsData")
public class Units extends DataService {


    //region  单位方法

    /**
     * 获得单位数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getUnitCount(Specification<UnitInfo> condition) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitCount(condition);
    }

    /**
     * 创建一条单位数据
     *
     * @param unitInfo 单位模型
     * @return 返回创建信息
     **/
    public UnitInfo createUnit(UnitInfo unitInfo) throws IOException {
        return getCwmData().getIUnitStrategy().createUnit(unitInfo);
    }

    /**
     * 更新一条单位数据
     *
     * @param unitInfo 单位模型
     **/
    public UnitInfo updateUnit(UnitInfo unitInfo) throws IOException {
        return getCwmData().getIUnitStrategy().updateUnit(unitInfo);
    }

    /**
     * 删除一条单位数据
     *
     * @param unitId 单位模型
     **/
    public void deleteUnitByUnitId(int unitId) throws IOException {
        getCwmData().getIUnitStrategy().deleteUnitByUnitId(unitId);
    }

    /**
     * 批量删除一批单位数据
     **/
    public void deleteUnitByUnitIdList(String unitIdList) throws IOException {
        getCwmData().getIUnitStrategy().deleteUnitByUnitIdList(unitIdList);
    }

    /**
     * 获取一条单位数据
     *
     * @param unitId 单位模型
     **/
    public UnitInfo getUnitByUnitId(int unitId) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitByUnitId(unitId);
    }

    /**
     * 通过编号获取单位
     *
     * @param code 单位编号
     * @return
     */
    public UnitInfo getUnitByUnitCode(String code) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitByUnitCode(code);
    }


    /**
     * 通过密钥获取单位信息
     *
     * @param apiKey    key
     * @param apiSecret 密钥
     */
    public UnitInfo getUnitByApiKeyAndApiSecret(String apiKey, String apiSecret) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitByApiKeyAndApiSecret(apiKey, apiSecret);
    }


    /**
     * 获得单位数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UnitInfo
     **/
    public List<UnitInfo> getUnitList(Specification<UnitInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitList(condition, sort);
    }


    /**
     * 获得单位数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UnitInfo
     **/
    public Page<UnitInfo> getUnitList(Integer pageSize, Integer pageNumber, Specification<UnitInfo> condition, Sort sort) throws IOException {
        return getCwmData().getIUnitStrategy().getUnitList(pageSize, pageNumber, condition, sort);
    }

    //endregion

}
