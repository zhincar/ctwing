package com.link510.ctwing.services;

import com.link510.ctwing.core.domain.unit.UnitInfo;
import com.link510.ctwing.core.domain.unit.UnitSubscribeInfo;
import com.link510.ctwing.core.helper.RandomHelper;
import com.link510.ctwing.core.helper.SecureHelper;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.data.UnitSubscribes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author cqnews
 * @date 2017/4/11
 */

//单位
@Service(value = "Units")
public class Units {

    @Resource(name = "UnitsData")
    private com.link510.ctwing.data.Units units;


    @Autowired
    private Logs logs;

    //region  单位方法

    /**
     * 获得单位数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getUnitCount(Specification<UnitInfo> condition) {

        try {
            return units.getUnitCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得单位数量失败");
        }
        return 0;
    }

    /**
     * 创建一条单位数据
     *
     * @param unitInfo 单位模型
     * @return 返回创建信息
     **/
    public UnitInfo createUnit(UnitInfo unitInfo) {

        try {

            if (unitInfo == null || StringHelper.isNullOrWhiteSpace(unitInfo.getUnitCode())) {
                throw new IllegalArgumentException("参数异常");
            }

            return units.createUnit(unitInfo);

        } catch (Exception ex) {

            logs.write(ex, "创建一条单位数据失败");

        }
        return null;
    }

    /**
     * 更新一条单位数据
     *
     * @param unitInfo 单位模型
     **/
    public UnitInfo updateUnit(UnitInfo unitInfo) {
        try {

            if (unitInfo == null || unitInfo.getUnitId() <= 0) {

                throw new IllegalArgumentException("参数异常");

            }

            return units.updateUnit(unitInfo);

        } catch (Exception ex) {

            logs.write(ex, "更新一条单位数据异常");

        }

        return null;
    }

    /**
     * 删除一条单位数据
     *
     * @param unitId 单位模型
     **/
    public void deleteUnitByUnitId(int unitId) {
        try {

            if (unitId <= 0) {
                throw new IllegalArgumentException("参数异常");
            }

            units.deleteUnitByUnitId(unitId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条单位数据异常");
        }
    }

    /**
     * 批量删除一批单位数据
     **/
    public void deleteUnitByUnitIdList(String unitIdList) {
        try {

            if (StringHelper.isNullOrWhiteSpace(unitIdList)) {
                throw new IllegalArgumentException("参数异常");
            }

            units.deleteUnitByUnitIdList(unitIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批单位数据异常");
        }
    }

    /**
     * 获取一条单位数据
     *
     * @param unitId 单位模型
     **/
    public UnitInfo getUnitByUnitId(int unitId) {

        try {

            if (unitId <= 0) {
                throw new IllegalArgumentException("参数异常");
            }

            return units.getUnitByUnitId(unitId);

        } catch (Exception ex) {
            logs.write(ex, "获取一条单位数据");
        }

        return null;
    }


    /**
     * 通过编号获取单位
     *
     * @param code 单位编号
     * @return
     */
    public UnitInfo getUnitByUnitCode(String code) {

        try {
            if (StringHelper.isNullOrWhiteSpace(code)) {
                throw new IllegalArgumentException("参数异常");
            }

            return units.getUnitByUnitCode(code);

        } catch (Exception ex) {
            logs.write(ex, "通过编号获取单位");
        }

        return null;
    }


    /**
     * 通过密钥获取单位信息
     *
     * @param apiKey    key
     * @param apiSecret 密钥
     */
    public UnitInfo getUnitByApiKeyAndApiSecret(String apiKey, String apiSecret) {

        try {

            if (StringHelper.isNullOrWhiteSpace(apiKey) || StringHelper.isNullOrWhiteSpace(apiSecret)) {
                throw new IllegalArgumentException("参数异常");
            }

            return units.getUnitByApiKeyAndApiSecret(apiKey, apiSecret);

        } catch (Exception ex) {
            logs.write(ex, "参数不能为空");
        }
        return null;
    }


    /**
     * 获得单位数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UnitInfo
     **/
    public List<UnitInfo> getUnitList(Specification<UnitInfo> condition, Sort sort) {

        List<UnitInfo> unitList = new ArrayList<UnitInfo>();

        try {
            unitList = units.getUnitList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得单位数据列表异常");
        }

        return unitList;
    }


    /**
     * 获得单位数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UnitInfo
     **/
    public Page<UnitInfo> getUnitList(Integer pageSize, Integer pageNumber, Specification<UnitInfo> condition, Sort sort) {

        Page<UnitInfo> unitList = null;

        try {
            unitList = units.getUnitList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得单位数据列表异常");
        }

        return unitList;
    }

    public Specification<UnitInfo> getUnitListCondition(String address) {

        return (Specification<UnitInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();
            if (StringHelper.isNotNullOrWhiteSpace(address)) {
                list.add(cb.like(root.get("address").as(String.class), "%" + address + "%"));
            }


            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));
            return query.getGroupRestriction();
        };

    }

    /**
     * 获取全部的单位编号
     *
     * @return
     */
    public List<UnitInfo> getAllUnitList() {

        List<UnitInfo> infoList = new ArrayList<>();

        try {
            infoList = getUnitList(null, null);

        } catch (Exception ex) {
            logs.write(ex, "获取全部的单位编号");
        }
        return infoList;


    }

    public Specification<UnitInfo> getUnitListCondition(String code, String name, String areaCode, String masterName, String busLiceNum) {

        return (Specification<UnitInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (StringHelper.isNotNullOrWhiteSpace(code)) {
                list.add(cb.equal(root.get("code").as(String.class), code));
            }

            if (StringHelper.isNotNullOrWhiteSpace(name)) {
                list.add(cb.like(root.get("name").as(String.class), "%" + name + "%"));
            }


            if (StringHelper.isNotNullOrWhiteSpace(areaCode)) {
                list.add(cb.equal(root.get("areaCode").as(String.class), areaCode));
            }

            if (StringHelper.isNotNullOrWhiteSpace(masterName)) {
                list.add(cb.like(root.get("masterName").as(String.class), "%" + masterName + "%"));
            }

            if (StringHelper.isNotNullOrWhiteSpace(busLiceNum)) {
                list.add(cb.like(root.get("busLiceNum").as(String.class), "%" + busLiceNum + "%"));
            }

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };
    }

    /**
     * 生成唯一的apikey
     *
     * @return
     */
    public static String generateApiKey() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 生成唯一的密钥
     *
     * @return
     */
    public static String generateApiSecret(String apiKey, String salt) {
        // System.out.println("apiKey:" + apiKey);
        // System.out.println("salt:" + salt);
        // System.out.println("md5:" + SecureHelper.md5(apiKey + salt));
        return SecureHelper.md5(apiKey + salt);
    }

    /**
     * 生成盐值
     *
     * @return
     */
    public static String generateSalt() {
        return RandomHelper.generateMixString(6);
    }


    /**
     * 刷新token
     *
     * @param unitInfo 单位信息
     * @return
     */
    public UnitInfo refreshToken(UnitInfo unitInfo) {
        try {

            if (unitInfo == null || unitInfo.getUnitId() <= 0) {
                throw new IllegalArgumentException("参数错误");
            }

            String apiKey = generateApiKey();
            String salt = generateSalt();
            String apiSecret = generateApiSecret(apiKey, salt);

            unitInfo.setApiKey(apiKey);
            unitInfo.setApiSecret(apiSecret);
            unitInfo.setSalt(salt);

            unitInfo = updateUnit(unitInfo);

            return unitInfo;

        } catch (Exception ex) {
            logs.write(ex, "刷新代理商token");
        }

        return null;
    }

    //endregion

    @Resource(name = "UnitSubscribesData")
    private UnitSubscribes unitSubscribes;

    //region  单位订阅方法

    /**
     * 获得单位订阅数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getUnitSubscribeCount(Specification<UnitSubscribeInfo> condition) {

        try {
            return unitSubscribes.getUnitSubscribeCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得单位订阅数量失败");
        }
        return 0;
    }

    /**
     * 创建一条单位订阅数据
     *
     * @param unitsubscriptionInfo 单位订阅模型
     * @return 返回创建信息
     **/
    public UnitSubscribeInfo createUnitSubscribe(UnitSubscribeInfo unitsubscriptionInfo) {
        try {
            return unitSubscribes.createUnitSubscribe(unitsubscriptionInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条单位订阅数据失败");
        }
        return null;
    }

    /**
     * 更新一条单位订阅数据
     *
     * @param unitsubscriptionInfo 单位订阅模型
     **/
    public UnitSubscribeInfo updateUnitSubscribe(UnitSubscribeInfo unitsubscriptionInfo) {
        try {
            return unitSubscribes.updateUnitSubscribe(unitsubscriptionInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条单位订阅数据异常");
        }

        return null;
    }

    /**
     * 删除一条单位订阅数据
     *
     * @param subId 单位订阅模型
     **/
    public void deleteUnitSubscribeBySubId(int subId) {
        try {
            unitSubscribes.deleteUnitSubscribeBySubId(subId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条单位订阅数据异常");
        }
    }

    /**
     * 批量删除一批单位订阅数据
     **/
    public void deleteUnitSubscribeBySubIdList(String subIdList) {
        try {
            unitSubscribes.deleteUnitSubscribeBySubIdList(subIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批单位订阅数据异常");
        }
    }

    /**
     * 获取一条单位订阅数据
     *
     * @param subId 单位订阅模型
     **/
    public UnitSubscribeInfo getUnitSubscribeBySubId(int subId) {
        try {
            return unitSubscribes.getUnitSubscribeBySubId(subId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条单位订阅数据");
        }

        return null;
    }


    /**
     * 获得单位订阅数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UnitSubscriptionInfo
     **/
    public List<UnitSubscribeInfo> getUnitSubscribeList(Specification<UnitSubscribeInfo> condition, Sort sort) {

        List<UnitSubscribeInfo> unitSubscriptionList = new ArrayList<UnitSubscribeInfo>();

        try {
            unitSubscriptionList = unitSubscribes.getUnitSubscribeList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得单位订阅数据列表异常");
        }

        return unitSubscriptionList;
    }


    /**
     * 获得单位订阅数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UnitSubscriptionInfo
     **/
    public Page<UnitSubscribeInfo> getUnitSubscribeList(Integer pageSize, Integer pageNumber, Specification<UnitSubscribeInfo> condition, Sort sort) {

        Page<UnitSubscribeInfo> unitSubscriptionList = null;

        try {
            unitSubscriptionList = unitSubscribes.getUnitSubscribeList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得单位订阅数据列表异常");
        }

        return unitSubscriptionList;
    }

    /**
     * 通过产品Id和产品协议读取订阅消息
     *
     * @param unitCode  单位编码
     * @param productId 产品Id
     * @param protocol  协议
     * @return
     */
    public UnitSubscribeInfo getUnitSubscribeByProductIdAndProtocol(String unitCode, Integer productId, String protocol) {

        try {

            if (StringHelper.isNullOrWhiteSpace(unitCode)) {
                throw new IllegalArgumentException("单位编号参数异常");
            }

            if (productId <= 0 || StringHelper.isNullOrWhiteSpace(protocol)) {
                throw new IllegalArgumentException("产品或者协议参数异常");
            }

            return unitSubscribes.getUnitSubscribeByProductIdAndProtocol(unitCode, productId, protocol);

        } catch (Exception ex) {
            logs.write(ex, "");
        }

        return null;

    }

    /**
     * 获取订单列表条件
     *
     * @param unitCode 单位编码
     * @return
     */
    public Specification<UnitSubscribeInfo> getUnitSubscribeListCondition(String unitCode) {

        return (Specification<UnitSubscribeInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(cb.equal(root.get("unitCode").as(String.class), unitCode));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();
        };


    }


    //endregion


}
