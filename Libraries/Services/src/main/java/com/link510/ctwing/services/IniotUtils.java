package com.link510.ctwing.services;


import com.link510.ctwing.core.domain.producrs.EnvironmentMessageInfo;
import com.link510.ctwing.core.iniots.CWMIniot;
import com.link510.ctwing.core.log.Logs;
import com.link510.iniot.sdk.management.DeviceManagement;
import com.link510.iniot.sdk.management.MessageManagement;
import com.link510.iniot.sdk.management.SubscribeManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.NotSupportedException;

@Service(value = "IniotUtils")
public class IniotUtils {


    @Autowired
    private Logs logs;


    @Autowired
    private CWMIniot cwmIniot;

    /**
     * 设备管理器
     *
     * @return 设备管理器
     */
    public DeviceManagement getDeviceManagement() throws NotSupportedException {

        try {
            return cwmIniot.getIIniotStrategy().getDeviceManagement();

        } catch (Exception ex) {

            logs.write(ex, "设备管理器");
        }

        throw new NotSupportedException("协议不支持");
    }

    /**
     * 消息管理器
     *
     * @return 消息管理器
     */
    public MessageManagement getMessageManagement() throws NotSupportedException {
        try {
            return cwmIniot.getIIniotStrategy().getMessageManagement();

        } catch (Exception ex) {

            logs.write(ex, "消息管理器");
        }

        throw new NotSupportedException("协议不支持");
    }

    /**
     * 订阅管理器
     *
     * @return 订阅管理器
     */
    public SubscribeManagement getSubscribeManagement() throws NotSupportedException {
        try {
            return cwmIniot.getIIniotStrategy().getSubscribeManagement();

        } catch (Exception ex) {

            logs.write(ex, "订阅管理器");
        }

        throw new NotSupportedException("协议不支持");
    }


    /**
     * 获取最后一包信息
     *
     * @param deviceSN 设备编号
     * @return String
     */
    public EnvironmentMessageInfo lastEnvironmentMessage(String deviceSN) throws NotSupportedException {

        try {

            return cwmIniot.getIIniotStrategy().lastEnvironmentMessage(deviceSN);

        } catch (Exception ex) {
            logs.write(ex, "获取最后一包信息");
        }

        return null;
    }

    //endregion

}
