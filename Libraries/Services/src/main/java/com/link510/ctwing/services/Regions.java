package com.link510.ctwing.services;

import com.link510.ctwing.core.domain.base.RegionInfo;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.data.Regionds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cqnews on 2017/4/11.
 */

//区域信息
@Service(value = "Regions")
public class Regions {
    @Resource(name = "RegionsData")
    Regionds regionds;

    @Autowired
    private Logs logs;


    //region  区域信息方法

    /**
     * 获得区域信息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getRegionCount(Specification<RegionInfo> condition) {

        try {
            return regionds.getRegionCount(condition);
        } catch (Exception e) {
            logs.write(e, "获得区域信息数量失败");
        }
        return 0;
    }

    /**
     * 创建一条区域信息数据
     *
     * @param regionInfo 区域信息模型
     * @return 返回创建信息
     **/
    public RegionInfo createRegion(RegionInfo regionInfo) {
        try {
            return regionds.createRegion(regionInfo);
        } catch (Exception e) {
            logs.write(e, "创建一条区域信息数据失败");
        }
        return null;
    }

    /**
     * 更新一条区域信息数据
     *
     * @param regionInfo 区域信息模型
     **/
    public RegionInfo updateRegion(RegionInfo regionInfo) {
        try {
            return regionds.updateRegion(regionInfo);
        } catch (Exception e) {
            logs.write(e, "更新一条区域信息数据异常");
        }

        return null;
    }

    /**
     * 删除一条区域信息数据
     *
     * @param regionid 区域信息模型
     **/
    public void deleteRegionByRegionid(Integer regionid) {
        try {
            regionds.deleteRegionByRegionid(regionid);
        } catch (Exception e) {
            logs.write(e, "删除一条区域信息数据异常");
        }
    }

    /**
     * 批量删除一批区域信息数据
     **/
    public void deleteRegionByRegionidList(String regionidList) {
        try {
            regionds.deleteRegionByRegionidList(regionidList);
        } catch (Exception e) {
            logs.write(e, "批量删除一批区域信息数据异常");
        }
    }

    /**
     * 获取一条区域信息数据
     *
     * @param regionid 区域信息模型
     **/
    public RegionInfo getRegionByRegionid(Integer regionid) {
        try {
            return regionds.getRegionByRegionid(regionid);
        } catch (Exception e) {
            logs.write(e, "获取一条区域信息数据");
        }

        return null;
    }


    /**
     * 获得区域信息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回RegionInfo
     **/
    public List<RegionInfo> getRegionList(Specification<RegionInfo> condition, Sort sort) {

        List<RegionInfo> regionList = new ArrayList<RegionInfo>();

        try {
            regionList = regionds.getRegionList(condition, sort);
        } catch (Exception e) {
            logs.write(e, "获得区域信息数据列表异常");
        }

        return regionList;
    }


    /**
     * 获得区域信息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回RegionInfo
     **/
    public Page<RegionInfo> getRegionList(Integer pageSize, Integer pageNumber, Specification<RegionInfo> condition, Sort sort) {

        Page<RegionInfo> regionList = null;

        try {
            regionList = regionds.getRegionList(pageSize, pageNumber, condition, sort);
        } catch (Exception e) {
            logs.write(e, "获得区域信息数据列表异常");
        }

        return regionList;
    }


    //endregion

}
