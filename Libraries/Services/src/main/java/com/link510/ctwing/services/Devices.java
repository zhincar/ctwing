package com.link510.ctwing.services;

import com.alibaba.fastjson.JSON;
import com.link510.ctwing.core.domain.device.DeviceInfo;
import com.link510.ctwing.core.domain.device.DeviceToken;
import com.link510.ctwing.core.domain.users.UserToken;
import com.link510.ctwing.core.enums.device.WarnState;
import com.link510.ctwing.core.exption.CWMException;
import com.link510.ctwing.core.exption.ExistsException;
import com.link510.ctwing.core.helper.*;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.data.Deviceds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.link510.ctwing.core.helper.SecureHelper.md5;

/**
 * 设备
 * Created by cqnews on 2017/4/11.
 */
@Service(value = "Devices")
public class Devices {

    @Resource(name = "DevicesData")
    private Deviceds deviceds;


    @Autowired
    private Logs logs;


    //region  设备权限方法

    /**
     * 生成盐值
     *
     * @return
     */
    public String createSalt() {
        return RandomHelper.generateString(6);
    }

    /**
     * 创建用户密码
     *
     * @param password 真实密码
     * @param salt     散列盐值
     * @return
     */
    public String createDevicePassword(String password, String salt) {
        return md5(password + salt);
    }

    /**
     * 创建设备token
     *
     * @param deviceSN  设备编号
     * @param salt      盐值
     * @param limitTime 过期时间
     * @return
     */
    public String createDeviceToken(String deviceSN, String salt, Integer limitTime) {

        String token = null;

        try {


            DeviceToken deviceToken = new DeviceToken(deviceSN, salt, limitTime);
            String content = JSON.toJSONString(deviceToken);

            token = AESHelper.encode(content);

        } catch (Exception ex) {

            //logs.write("用户创建token失败");

        }

        return token;
    }

    /**
     * 创建刷新密角球
     *
     * @param token token
     * @return 刷新密钥
     */
    public String createRefreshToken(String token) {

        String refreshToken = null;

        try {
            refreshToken = md5(token + "iloverliuyufei");
        } catch (Exception ex) {
            logs.write("用户创建token失败");
        }

        return refreshToken;
    }

    /**
     * 用户解密
     *
     * @param token token
     * @return
     */
    public DeviceToken decryptDeviceToken(String token) {

        DeviceToken deviceToken = null;

        if (StringHelper.isNullOrWhiteSpace(token)) {
            return null;
        }

        try {
            String postStr = AESHelper.decode(token);
            deviceToken = JSON.parseObject(postStr, DeviceToken.class);

        } catch (Exception ex) {

            //logs.write(ex, "用户解密失败");
        }

        return deviceToken;

    }

    //endregion

    //region  设备方法

    /**
     * 获得设备数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getDeviceCount(Specification<DeviceInfo> condition) {

        try {
            return deviceds.getDeviceCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得设备数量失败");
        }
        return 0;
    }

    /**
     * 创建一条设备数据
     *
     * @param deviceInfo 设备模型
     * @return 返回创建信息
     **/
    public DeviceInfo createDevice(DeviceInfo deviceInfo) throws ExistsException {
        try {

            if (isDeviceExists(deviceInfo.getProductId(), deviceInfo.getDeviceSN())) {
                throw new ExistsException("数据重复");
            }
            return deviceds.createDevice(deviceInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条设备数据失败");
        }
        return null;
    }


    /**
     * 校验主要的数据是否存在
     *
     * @param productId 产品id
     * @param deviceSN  设备编号
     * @return
     */
    public boolean isDeviceExists(Integer productId, String deviceSN) throws IOException {

        try {
            return deviceds.isDeviceExists(productId, deviceSN);
        } catch (Exception ex) {

            logs.write(ex, "校验主要的数据是否存在");
        }

        return false;
    }

    /**
     * 更新一条设备数据
     *
     * @param deviceInfo 设备模型
     **/
    public DeviceInfo updateDevice(DeviceInfo deviceInfo) {
        try {
            return deviceds.updateDevice(deviceInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条设备数据异常");
        }

        return null;
    }

    /**
     * 删除一条设备数据
     *
     * @param deviceId 设备模型
     **/
    public boolean deleteDeviceByDeviceId(int deviceId) {
        try {
            return deviceds.deleteDeviceByDeviceId(deviceId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条设备数据异常");
        }
        return false;
    }

    /**
     * 设备恢复
     *
     * @param deviceId 设备Id
     * @return
     */
    public boolean recoverDeviceByDeviceId(Integer deviceId) {
        try {
            return deviceds.recoverDeviceByDeviceId(deviceId);
        } catch (Exception ex) {
            logs.write(ex, "恢复一条设备数据异常");
        }
        return false;
    }

    /**
     * 批量删除一批设备数据
     **/
    public void deleteDeviceByIdList(String idList) {
        try {
            deviceds.deleteDeviceByIdList(idList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批设备数据异常");
        }
    }

    /**
     * 获取一条设备数据
     *
     * @param deviceId 设备Id
     **/
    public DeviceInfo getDeviceById(int deviceId) {
        try {
            return deviceds.getDeviceById(deviceId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条设备数据");
        }

        return null;
    }

    /**
     * 获取一条无缓存设备信息
     *
     * @param deviceId 设备Id
     * @return
     */
    public DeviceInfo getNoCacheDeviceById(Integer deviceId) {
        try {
            return deviceds.getNoCacheDeviceById(deviceId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条设备数据");
        }

        return null;
    }


    /**
     * 通过设备号获取设备信息
     *
     * @param deviceSN
     * @return
     */
    public DeviceInfo getDeviceByDeviceSN(Integer productId, String deviceSN) {

        DeviceInfo deviceInfo = null;

        try {

            deviceInfo = deviceds.getDeviceByDeviceSN(deviceSN);

        } catch (Exception ex) {
            ex.printStackTrace();
            logs.write(ex, "通过设备号获取设备信息");
        }
        return deviceInfo;
    }


    /**
     * 通过产品id和设备编号获取设备信息
     *
     * @param productId 产品id
     * @param deviceSN  产品编号
     * @return 返回设备信息
     */
    public DeviceInfo getDeviceByProductIdAndDeviceSN(Integer productId, String deviceSN) {

        try {

            return deviceds.getDeviceByProductIdAndDeviceSN(productId, deviceSN);

        } catch (Exception ex) {

            logs.write(ex, "通过产品id和设备编号获取设备信息");

        }

        return null;
    }


    /**
     * 获得设备数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回DeviceInfo
     **/
    public List<DeviceInfo> getDeviceList(Specification<DeviceInfo> condition, Sort sort) {

        List<DeviceInfo> deviceList = new ArrayList<DeviceInfo>();

        try {
            deviceList = deviceds.getDeviceList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得设备数据列表异常");
        }

        return deviceList;
    }

    /**
     * 获得设备数据列表
     *
     * @return
     */
    public List<DeviceInfo> getAllList() {

        List<DeviceInfo> deviceList = new ArrayList<>();
        try {

            deviceList = deviceds.getDeviceList(null, null);

        } catch (Exception ex) {
            {
                logs.write(ex, "获得设备数据列表异常");
            }
        }

        return deviceList;

    }


    /**
     * 获得设备数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回DeviceInfo
     **/
    public Page<DeviceInfo> getDeviceList(Integer pageSize, Integer pageNumber, Specification<DeviceInfo> condition, Sort sort) {

        Page<DeviceInfo> deviceList = null;

        try {
            deviceList = deviceds.getDeviceList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得设备数据列表异常");
        }

        return deviceList;
    }


    /**
     * 获取设备列表
     *
     * @param deviceId 设备id
     * @return
     */
    public Specification<DeviceInfo> getDeviceListCondition(String deviceId, String deviceSN, String token) {

        Specification<DeviceInfo> condition = null;

        try {

            condition = (Specification<DeviceInfo>) (root, query, cb) -> {

                List<Predicate> list = new ArrayList<>();

                if (StringHelper.isNotNullOrWhiteSpace(deviceId)) {
                    list.add(cb.equal(root.get("deviceId").as(String.class), deviceId));
                }

                list.add(cb.equal(root.get("isDelete").as(Integer.class), 0));

                if (StringHelper.isNotNullOrWhiteSpace(deviceSN)) {
                    list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
                }

                if (StringHelper.isNotNullOrWhiteSpace(token)) {
                    list.add(cb.equal(root.get("token").as(String.class), token));
                }

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));


                return query.getGroupRestriction();
            };

        } catch (Exception ignored) {

        }
        return condition;
    }


    /**
     * 获取设备列表条件
     *
     * @param unitCode    单位编码
     * @param productName 设备名称
     * @return
     * @throws IOException
     */
    public Specification<DeviceInfo> getListCondition(String unitCode, String productName) {
        Specification<DeviceInfo> condition = null;

        try {
            condition = (Specification<DeviceInfo>) (root, query, cb) -> {

                Integer nowtime = UnixTimeHelper.getUnixTimeStamp();

                List<Predicate> list = new ArrayList<Predicate>();

                if (StringHelper.isNotNullOrWhiteSpace(unitCode)) {
                    list.add(cb.like(root.get("unitCode").as(String.class), "%" + unitCode + "%"));
                }
                if (StringHelper.isNotNullOrWhiteSpace(productName)) {
                    list.add(cb.like(root.get("productName").as(String.class), "%" + productName + "%"));
                }

                list.add(cb.equal(root.get("isDelete").as(Integer.class), 0));

                Predicate[] p = new Predicate[list.size()];

                query.where(cb.and(list.toArray(p)));

                return query.getGroupRestriction();
            };
        } catch (Exception e) {
            logs.write("获取设备列表条件失败");
            e.printStackTrace();
        }
        return condition;
    }

    /**
     * 获取设计
     *
     * @param productId
     * @param unitCode
     * @param deviceSN
     * @param token
     * @param name
     * @param deviceModel
     * @return
     */
    public Specification<DeviceInfo> getDeviceListCondition(Integer productId, String unitCode, String deviceSN, String token, String name, Integer deviceModel) {

        return (Specification<DeviceInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (productId > 0) {
                list.add(cb.equal(root.get("productId").as(Integer.class), productId));
            }

            if (StringHelper.isNotNullOrWhiteSpace(unitCode)) {
                list.add(cb.equal(root.get("unitCode").as(String.class), unitCode));
            }


            list.add(cb.equal(root.get("isDelete").as(Integer.class), 0));

            if (StringHelper.isNotNullOrWhiteSpace(deviceSN)) {
                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
            }

            if (StringHelper.isNotNullOrWhiteSpace(token)) {
                list.add(cb.equal(root.get("token").as(String.class), token));
            }

            if (StringHelper.isNotNullOrWhiteSpace(name)) {
                list.add(cb.like(root.get("name").as(String.class), "%" + name + "%"));
            }

            if (deviceModel == 0 || deviceModel == 1) {
                list.add(cb.equal(root.get("deviceModel").as(Integer.class), deviceModel));
            }


            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };
    }


    /**
     * 获取切换设备列表条件
     *
     * @param productId 产品Id
     * @param unitCode  单位编码
     * @param name      设备名称
     * @return
     */
    public Specification<DeviceInfo> getSwitchDeviceListCondition(int productId, String unitCode, String name) {

        return (Specification<DeviceInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (productId >= 1) {
                list.add(cb.equal(root.get("productId").as(Integer.class), productId));
            }

            if (StringHelper.isNotNullOrWhiteSpace(unitCode)) {
                list.add(cb.equal(root.get("unitCode").as(String.class), unitCode));
            }

            if (StringHelper.isNotNullOrWhiteSpace(name)) {
                list.add(cb.like(root.get("name").as(String.class), name));
            }

            list.add(cb.equal(root.get("isDelete").as(Integer.class), 0));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();
        };
    }

    /**
     * 发起设备警告
     *
     * @param deviceSN 设备编号
     * @param warnSate 警告类型
     * @param warnCode 警告的代码
     * @param warnTime 发起时间
     */
    @Async
    public void sendWarning(String deviceSN, WarnState warnSate, String warnCode, Integer warnTime) {
        sendWarning(1, deviceSN, warnSate, warnCode, warnTime);
    }

    /**
     * 发起设备警告
     *
     * @param productId 产品Id
     * @param deviceSN  设备编号
     * @param warnSate  警告类型
     * @param warnCode  警告的代码
     * @param warnTime  发起时间
     */
    @Async
    public void sendWarning(Integer productId, String deviceSN, WarnState warnSate, String warnCode, Integer warnTime) {

        try {

            if (productId <= 0 || StringHelper.isNullOrWhiteSpace(deviceSN)) {
                throw new CWMException("设备编号或产品Id缺失");
            }

            DeviceInfo deviceInfo = getDeviceByDeviceSN(productId, deviceSN);

            if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
                throw new CWMException("设备信息不存在,无法预警");
            }

            deviceInfo.setWarningLevel(warnSate.getLevel());
            deviceInfo.setWarningCode(warnCode);
            deviceInfo.setWarningTime(warnTime);

            updateDevice(deviceInfo);


        } catch (Exception ex) {
            logs.write(ex, "发起设备警告");
        }

    }


    /**
     * 查询设备
     *
     * @param deviceSN
     * @return
     */
    public DeviceInfo getDeviceByDeviceSN(String deviceSN) {
        DeviceInfo result = null;
        try {

            deviceSN = deviceSN.trim();

            result = deviceds.getDeviceByDeviceSN(deviceSN);
        } catch (Exception e) {
            logs.write(e, "查询设备异常，请检查,deviceSN:" + deviceSN);
        }
        return result;
    }


    /**
     * 根据token获取设备详情
     * NB设备token等于deviceId
     *
     * @param token
     * @return
     */
    public DeviceInfo getNbDeviceByToken(String token) {
        DeviceInfo result = null;
        try {
            result = deviceds.getNbDeviceByToken(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 查询设备信息
     *
     * @param imsi
     * @return
     */
    public DeviceInfo getDeviceByImsi(String imsi) {
        DeviceInfo result = null;
        try {
            result = deviceds.getDeviceByImsi(imsi);
        } catch (Exception e) {
            logs.write(e, "查询设备信息异常");
        }
        return result;
    }

    /**
     * 物理删除设备
     *
     * @param deviceId
     */
    public void deletePhysicsDeviceByDeviceId(Integer deviceId) {
        try {
            deviceds.deletePhysicsDeviceByDeviceId(deviceId);
        } catch (Exception e) {
            e.printStackTrace();
            logs.write(e, "物理删除设备异常");
        }
    }

    /**
     * 设备列表的条件
     *
     * @param deviceSN
     * @param token
     * @param name
     * @param deviceModel
     * @param agentId     代理Id
     * @return
     */
    public Specification<DeviceInfo> getAgentDeviceListCondition(String deviceSN, String token, String name, Integer deviceModel, Integer agentId) {

        return (Specification<DeviceInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();


            list.add(cb.equal(root.get("isDelete").as(Integer.class), 0));

            if (StringHelper.isNotNullOrWhiteSpace(deviceSN)) {
                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
            }

            if (StringHelper.isNotNullOrWhiteSpace(token)) {
                list.add(cb.equal(root.get("token").as(String.class), token));
            }

            if (StringHelper.isNotNullOrWhiteSpace(name)) {
                list.add(cb.like(root.get("name").as(String.class), "%" + name + "%"));
            }

            if (deviceModel == 0 || deviceModel == 1) {
                list.add(cb.equal(root.get("deviceModel").as(Integer.class), deviceModel));
            }


            list.add(cb.equal(root.get("agentId").as(Integer.class), agentId));


            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };

    }


    /**
     * 设备列表的条件
     *
     * @param agentId 代理Id
     * @return
     */
    public Specification<DeviceInfo> getAgentDeviceListCondition(Integer agentId) {

        return (Specification<DeviceInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(cb.equal(root.get("isDelete").as(Integer.class), 0));

            list.add(cb.equal(root.get("agentId").as(Integer.class), agentId));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };
    }


    /**
     * 单位列表条件
     *
     * @param unitCode
     * @return
     */
    public Specification<DeviceInfo> getUnitDeviceListCondition(String unitCode) {

        return (Specification<DeviceInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            list.add(cb.equal(root.get("isDelete").as(Integer.class), 0));

            list.add(cb.equal(root.get("unitCode").as(String.class), unitCode));

            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();

        };
    }


    /**
     * 更新设备的产品Id和产品名称
     *
     * @param productId   产品Id
     * @param productName 产品名称
     * @return
     */
    public DeviceInfo updateProductId(String deviceSN, Integer productId, String productName) {

        try {

            if (StringHelper.isNullOrWhiteSpace(deviceSN)) {
                throw new IllegalArgumentException("产品编号参数异常");
            }

            if (productId <= 0 || StringHelper.isNullOrWhiteSpace(productName)) {
                throw new IllegalArgumentException("产品ID或产品名称参数异常");
            }


            DeviceInfo deviceInfo = getDeviceByDeviceSN(deviceSN);

            if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
                throw new IllegalArgumentException("设备信息异常");
            }

            deviceInfo.setProductId(productId);
            deviceInfo.setProductName(productName);

            return updateDevice(deviceInfo);

        } catch (Exception ex) {
            logs.write(ex, "更新设备的产品Id和产品名称");
        }
        return null;
    }

    /**
     * 通过设备获取当前设备的产品id
     *
     * @param deviceSN 产品编号
     * @return
     */
    public Integer getDeviceProductIdByDeviceSN(String deviceSN) {

        try {

            if (StringHelper.isNullOrWhiteSpace(deviceSN)) {
                throw new IllegalArgumentException("设备编号参数异常");
            }

            DeviceInfo deviceInfo = getDeviceByDeviceSN(deviceSN);

            if (deviceInfo == null || deviceInfo.getDeviceId() <= 0) {
                throw new IllegalArgumentException("设备信息异常");
            }
            return deviceInfo.getProductId();

        } catch (Exception ex) {
            logs.write(ex, "通过设备获取当前设备的产品id");
        }

        return 0;
    }

    /**
     * 回收站
     *
     * @param productId   产品Id
     * @param unitCode    单位编码
     * @param deviceSN    设备编号
     * @param token       token
     * @param name        名称
     * @param deviceModel 设备模式
     * @return
     */
    public Specification<DeviceInfo> getDeviceRecycleListCondition(Integer productId, String unitCode, String deviceSN, String token, String name, Integer deviceModel) {

        return (Specification<DeviceInfo>) (root, query, cb) -> {

            List<Predicate> list = new ArrayList<>();

            if (productId > 0) {
                list.add(cb.equal(root.get("productId").as(Integer.class), productId));
            }

            if (StringHelper.isNotNullOrWhiteSpace(unitCode)) {
                list.add(cb.equal(root.get("unitCode").as(String.class), unitCode));
            }


            list.add(cb.equal(root.get("isDelete").as(Integer.class), 1));

            if (StringHelper.isNotNullOrWhiteSpace(deviceSN)) {
                list.add(cb.equal(root.get("deviceSN").as(String.class), deviceSN));
            }

            if (StringHelper.isNotNullOrWhiteSpace(token)) {
                list.add(cb.equal(root.get("token").as(String.class), token));
            }

            if (StringHelper.isNotNullOrWhiteSpace(name)) {
                list.add(cb.like(root.get("name").as(String.class), "%" + name + "%"));
            }

            if (deviceModel == 0 || deviceModel == 1) {
                list.add(cb.equal(root.get("deviceModel").as(Integer.class), deviceModel));
            }


            Predicate[] p = new Predicate[list.size()];

            query.where(cb.and(list.toArray(p)));

            return query.getGroupRestriction();


        };

    }


    //endregion

}
