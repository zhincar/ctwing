package com.link510.ctwing.services;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.link510.ctwing.core.domain.product.ProductInfo;
import com.link510.ctwing.core.domain.product.ProductProtocolInfo;
import com.link510.ctwing.core.domain.product.ProductProtocolParamInfo;
import com.link510.ctwing.core.helper.StringHelper;
import com.link510.ctwing.core.helper.TypeHelper;
import com.link510.ctwing.core.helper.ValidateHelper;
import com.link510.ctwing.core.log.Logs;
import com.link510.ctwing.core.model.SelectListItem;
import com.link510.ctwing.core.products.CWMProduct;
import com.link510.ctwing.core.products.IProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by cqnews on 2017/4/11.
 */

//产品库
@Service(value = "Products")
public class Products {

    @Resource(name = "ProductsData")
    private com.link510.ctwing.data.Products products;

    @Autowired
    private CWMProduct cwmProduct;


    @Autowired
    private Logs logs;

    //region  产品库方法

    /**
     * 获得产品库数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getProductCount(Specification<ProductInfo> condition) {

        try {
            return products.getProductCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得产品库数量失败");
        }
        return 0;
    }

    /**
     * 创建一条产品库数据
     *
     * @param productInfo 产品库模型
     * @return 返回创建信息
     **/
    public ProductInfo createProduct(ProductInfo productInfo) {
        try {
            return products.createProduct(productInfo);
        } catch (Exception ex) {
            ex.printStackTrace();
            logs.write(ex, "创建一条产品库数据失败");
        }
        return null;
    }

    /**
     * 更新一条产品库数据
     *
     * @param productInfo 产品库模型
     **/
    public ProductInfo updateProduct(ProductInfo productInfo) {
        try {
            return products.updateProduct(productInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条产品库数据异常");
        }

        return null;
    }

    /**
     * 删除一条产品库数据
     *
     * @param productId 产品库模型
     **/
    public void deleteProductByProductId(int productId) {
        try {
            products.deleteProductByProductId(productId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条产品库数据异常");
        }
    }

    /**
     * 批量删除一批产品库数据
     **/
    public void deleteProductByProductIdList(String productIdList) {
        try {
            products.deleteProductByProductIdList(productIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批产品库数据异常");
        }
    }

    /**
     * 获取一条产品库数据
     *
     * @param productId 产品库模型
     **/
    public ProductInfo getProductByProductId(int productId) {
        try {
            return products.getProductByProductId(productId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条产品库数据");
        }

        return null;
    }


    /**
     * 获得产品库数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductInfo
     **/
    public List<ProductInfo> getProductInfoList(Specification<ProductInfo> condition, Sort sort) {

        List<ProductInfo> productList = new ArrayList<ProductInfo>();

        try {
            productList = products.getProductInfoList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得产品库数据列表异常");
        }

        return productList;
    }


    /**
     * 获得产品库数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductInfo
     **/
    public Page<ProductInfo> getProductInfoList(Integer pageSize, Integer pageNumber, Specification<ProductInfo> condition, Sort sort) {

        Page<ProductInfo> productList = null;

        try {
            productList = products.getProductInfoList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得产品库数据列表异常");
        }

        return productList;
    }


    /**
     * 获取产品列表
     *
     * @param name 产品名称
     * @return
     */
    public Specification<ProductInfo> getProductListCondition(String name) {

        Specification<ProductInfo> condition = new Specification<ProductInfo>() {
            @Override
            public Predicate toPredicate(Root<ProductInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> list = new ArrayList<>();

                if (StringHelper.isNotNullOrWhiteSpace(name)) {
                    list.add(cb.like(root.get("name").as(String.class), "%" + name + "%"));
                }

                Predicate[] p = new Predicate[list.size()];

                query.where(list.toArray(p));
                return query.getGroupRestriction();
            }
        };


        return condition;
    }

    /**
     * 获取产品的协议列表
     *
     * @param productId 产品id
     * @return
     */
    public List<ProductProtocolInfo> getProductProtocolList(Integer productId) {

        List<ProductProtocolInfo> protocolInfoList = new ArrayList<>();

        try {

            protocolInfoList = products.getProductProtocolList(productId);

        } catch (Exception ex) {
            logs.write(ex, "获取产品的协议列表");
        }

        return protocolInfoList;

    }


    //endregion

    //region  产品协议方法

    /**
     * 获得产品协议数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getProductProtocolCount(Specification<ProductProtocolInfo> condition) {

        try {
            return products.getProductProtocolCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得产品协议数量失败");
        }
        return 0;
    }

    /**
     * 创建一条产品协议数据
     *
     * @param productprotoclInfo 产品协议模型
     * @return 返回创建信息
     **/
    public ProductProtocolInfo createProductProtocol(ProductProtocolInfo productprotoclInfo) {
        try {

            return products.createProductProtocol(productprotoclInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条产品协议数据失败");
        }
        return null;
    }

    /**
     * 更新一条产品协议数据
     *
     * @param productprotoclInfo 产品协议模型
     **/
    public ProductProtocolInfo updateProductProtocol(ProductProtocolInfo productprotoclInfo) {
        try {
            return products.updateProductProtocol(productprotoclInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条产品协议数据异常");
        }

        return null;
    }

    /**
     * 删除一条产品协议数据
     *
     * @param protoclId 产品协议模型
     **/
    public void deleteProductProtocolByProtoclId(int protoclId) {
        try {
            products.deleteProductProtocolByProtoclId(protoclId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条产品协议数据异常");
        }
    }

    /**
     * 批量删除一批产品协议数据
     **/
    public void deleteProductProtocolByProtoclIdList(String protoclIdList) {
        try {
            products.deleteProductProtocolByProtoclIdList(protoclIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批产品协议数据异常");
        }
    }

    /**
     * 获取一条产品协议数据
     *
     * @param protoclId 产品协议模型
     **/
    public ProductProtocolInfo getProductProtocolByProtoclId(int protoclId) {
        try {
            return products.getProductProtocolByProtoclId(protoclId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条产品协议数据");
        }

        return null;
    }


    /**
     * 获得产品协议数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductProtocolInfo
     **/
    public List<ProductProtocolInfo> getProductProtocolList(Specification<ProductProtocolInfo> condition, Sort sort) {

        List<ProductProtocolInfo> productProtoclList = new ArrayList<ProductProtocolInfo>();

        try {
            productProtoclList = products.getProductProtocolList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得产品协议数据列表异常");
        }

        return productProtoclList;
    }


    /**
     * 获得产品协议数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductProtocolInfo
     **/
    public Page<ProductProtocolInfo> getProductProtocolList(Integer pageSize, Integer pageNumber, Specification<ProductProtocolInfo> condition, Sort sort) {

        Page<ProductProtocolInfo> productProtoclList = null;

        try {
            productProtoclList = products.getProductProtocolList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得产品协议数据列表异常");
        }

        return productProtoclList;
    }


    //endregion

    //region  协议参数方法

    /**
     * 获得协议参数数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    public long getProductProtocolParamCount(Specification<ProductProtocolParamInfo> condition) {

        try {
            return products.getProductProtocolParamCount(condition);
        } catch (Exception ex) {
            logs.write(ex, "获得协议参数数量失败");
        }
        return 0;
    }

    /**
     * 创建一条协议参数数据
     *
     * @param productprotoclparamInfo 协议参数模型
     * @return 返回创建信息
     **/
    public ProductProtocolParamInfo createProductProtocolParam(ProductProtocolParamInfo productprotoclparamInfo) {
        try {
            return products.createProductProtocolParam(productprotoclparamInfo);
        } catch (Exception ex) {
            logs.write(ex, "创建一条协议参数数据失败");
        }
        return null;
    }

    /**
     * 更新一条协议参数数据
     *
     * @param productprotoclparamInfo 协议参数模型
     **/
    public ProductProtocolParamInfo updateProductProtocolParam(ProductProtocolParamInfo productprotoclparamInfo) {
        try {
            return products.updateProductProtocolParam(productprotoclparamInfo);
        } catch (Exception ex) {
            logs.write(ex, "更新一条协议参数数据异常");
        }

        return null;
    }

    /**
     * 删除一条协议参数数据
     *
     * @param paramId 协议参数模型
     **/
    public void deleteProductProtocolParamByParamId(int paramId) {
        try {
            products.deleteProductProtocolParamByParamId(paramId);
        } catch (Exception ex) {
            logs.write(ex, "删除一条协议参数数据异常");
        }
    }

    /**
     * 批量删除一批协议参数数据
     **/
    public void deleteProductProtocolParamByParamIdList(String paramIdList) {
        try {
            products.deleteProductProtocolParamByParamIdList(paramIdList);
        } catch (Exception ex) {
            logs.write(ex, "批量删除一批协议参数数据异常");
        }
    }

    /**
     * 获取一条协议参数数据
     *
     * @param paramId 协议参数模型
     **/
    public ProductProtocolParamInfo getProductProtocolParamByParamId(int paramId) {
        try {
            return products.getProductProtocolParamByParamId(paramId);
        } catch (Exception ex) {
            logs.write(ex, "获取一条协议参数数据");
        }

        return null;
    }


    /**
     * 获得协议参数数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductProtocolParamInfo
     **/
    public List<ProductProtocolParamInfo> getProductProtocolParamList(Specification<ProductProtocolParamInfo> condition, Sort sort) {

        List<ProductProtocolParamInfo> productProtoclParamList = new ArrayList<ProductProtocolParamInfo>();

        try {
            productProtoclParamList = products.getProductProtocolParamList(condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得协议参数数据列表异常");
        }

        return productProtoclParamList;
    }


    /**
     * 获得协议参数数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductProtocolParamInfo
     **/
    public Page<ProductProtocolParamInfo> getProductProtocolParamList(Integer pageSize, Integer pageNumber, Specification<ProductProtocolParamInfo> condition, Sort sort) {

        Page<ProductProtocolParamInfo> productProtoclParamList = null;

        try {
            productProtoclParamList = products.getProductProtocolParamList(pageSize, pageNumber, condition, sort);
        } catch (Exception ex) {
            logs.write(ex, "获得协议参数数据列表异常");
        }

        return productProtoclParamList;
    }

    /**
     * 通读产品协议参数列表
     *
     * @param protocolId 协议id
     * @return
     */
    public List<ProductProtocolParamInfo> getProductProtocolParamList(Integer protocolId) {

        List<ProductProtocolParamInfo> productProtoclParamList = new ArrayList<>();

        try {
            productProtoclParamList = products.getProductProtocolParamList(protocolId);
        } catch (Exception ex) {
            logs.write(ex, "获得协议参数数据列表异常");
        }

        return productProtoclParamList;
    }

    /**
     * 通读产品协议参数列表
     *
     * @param productId 产品Id
     * @return
     */
    public List<ProductProtocolParamInfo> getProductProtocolParamListByProductId(Integer productId) {

        List<ProductProtocolParamInfo> productProtoclParamList = new ArrayList<>();

        try {
            productProtoclParamList = products.getProductProtocolParamListByProductId(productId);
        } catch (Exception ex) {
            logs.write(ex, "获得协议参数数据列表异常");
        }

        return productProtoclParamList;
    }

    public List<ProductInfo> findSimpleModel() {
        return products.findSimpleModel();
    }

    /**
     * 获取所有的产器列表
     *
     * @return
     */
    public List<ProductInfo>
    getAllProductList() {
        return getAllProductList(null);

    }

    /**
     * 获取所有的产器列表
     *
     * @return
     */
    public List<ProductInfo> getAllProductList(Sort sort) {

        List<ProductInfo> productInfoList = new ArrayList<>();

        try {

            productInfoList = products.getAllProductList(sort);

        } catch (Exception ex) {

            logs.write(ex, "获取所有的产器列表");
        }

        return productInfoList;
    }

    /**
     * 批量添加参数
     *
     * @param protocolInfo         协议信息
     * @param paramNameList        参数名称
     * @param paramKeyList         参数key值
     * @param paramDescriptionList 参数描述
     */
    public void createProductProtocolParam(ProductProtocolInfo protocolInfo,
                                           List<String> paramNameList,
                                           List<String> paramKeyList,
                                           List<Integer> dataTypes,
                                           List<String> defaultValues,
                                           List<String> units,
                                           List<Integer> formats,
                                           List<Integer> starts,
                                           List<Integer> lengths,
                                           List<String> expressions,
                                           List<String> paramDescriptionList) {


        /**
         * 排空
         */
        if (paramNameList == null || paramKeyList == null || paramDescriptionList == null) {
            return;
        }

        /**
         * 排空列表
         */
        if (paramNameList.size() <= 0) {
            return;
        }

        Integer size = paramNameList.size();

        for (int i = 0; i < size; i++) {

            try {

                String name = paramNameList.get(i).trim();

                String key = "";

                try {
                    key = paramKeyList.get(i).trim();
                } catch (Exception ex) {
                    key = name;
                }


                Integer dataType = 0;

                try {
                    dataType = dataTypes.get(i);
                } catch (Exception ex) {
                    dataType = 0;
                }

                String defaultValue = "";
                try {
                    defaultValue = defaultValues.get(i).trim();
                } catch (Exception ex) {
                    defaultValue = "";
                }
                String unit = "";
                try {
                    unit = units.get(i).trim();
                } catch (Exception ex) {
                    unit = "";
                }
                Integer format = 0;
                try {
                    format = formats.get(i);
                } catch (Exception ex) {
                    format = 0;
                }
                Integer start = 0;
                try {
                    start = starts.get(i);
                } catch (Exception ex) {
                    start = 0;
                }
                Integer length = 0;
                try {
                    length = lengths.get(i);
                } catch (Exception ex) {
                    length = 0;
                }
                String expression = "";
                try {
                    expression = expressions.get(i).trim();
                } catch (Exception ex) {
                    expression = "";
                }
                String description = "";
                try {
                    description = paramDescriptionList.get(i).trim();
                } catch (Exception ex) {
                    description = "";
                }


                if ("".equals(name) || "".equals(key)) {
                    return;
                }

                ProductProtocolParamInfo paramInfo = new ProductProtocolParamInfo();

                paramInfo.setProtocolId(protocolInfo.getProtocolId());
                paramInfo.setProductId(protocolInfo.getProductId());
                paramInfo.setProductName(protocolInfo.getProductName());
                paramInfo.setName(name);
                paramInfo.setKey(key);
                paramInfo.setDescription(description);

                paramInfo.setDataType(dataType);
                paramInfo.setDefaultValue(defaultValue);
                paramInfo.setUnit(unit);
                paramInfo.setFormat(format);
                paramInfo.setStart(start);
                paramInfo.setLength(length);
                paramInfo.setExpression(expression);

                createProductProtocolParam(paramInfo);

            } catch (Exception ex) {
                logs.write(ex, "批量添加参数");
            }

        }

    }

    /**
     * 通过token获取产品信息
     *
     * @param secretkey token
     * @return
     */
    public ProductInfo getProductBySecretkey(String secretkey) {

        try {

            return products.getProductBySecretkey(secretkey);

        } catch (Exception ex) {
            logs.write(ex, "通过token获取产品信息");
        }

        return null;

    }

    /**
     * 获取产品协议的参数列表
     *
     * @param productId 产品id
     * @param protocol  协议key
     * @return
     */
    public List<ProductProtocolParamInfo> getProductProtocolParamList(Integer productId, String protocol) {

        List<ProductProtocolParamInfo> protocolParamInfoList = new ArrayList<>();

        process:
        {

            try {

                /**
                 * 产品Id不能为空
                 */
                if (productId <= 0) {
                    break process;
                }

                ProductInfo productInfo = getProductByProductId(productId);

                /**
                 * 产品不能为空
                 */
                if (productInfo == null || productInfo.getProductId() <= 0) {
                    break process;
                }

                ProductProtocolInfo protocolInfo = getProductProtocolByProductIdAndProtocol(productId, protocol);

                /**
                 * 协议为空
                 */
                if (protocolInfo == null || protocolInfo.getProtocolId() <= 0) {
                    break process;
                }

                protocolParamInfoList = getProductProtocolParamList(protocolInfo.getProtocolId());

            } catch (Exception ex) {

                logs.write(ex, "获取产品协议的参数列表");

            }
        }

        return protocolParamInfoList;
    }

    /**
     * 通过产品id获取产品协议
     *
     * @param productId 产品id
     * @param protocol  协议
     * @return
     */
    public ProductProtocolInfo getProductProtocolByProductIdAndProtocol(Integer productId, String protocol) {

        ProductProtocolInfo protocolInfo = null;

        try {

            if (productId <= 0) {
                throw new IllegalArgumentException("参数Id参数异常");
            }

            if (StringHelper.isNullOrWhiteSpace(protocol)) {
                throw new IllegalArgumentException("协议参数异常");
            }

            protocolInfo = products.getProductProtocolByProductIdAndProtocol(productId, protocol);

            if (protocolInfo == null) {
                protocolInfo = getDefaultProductProtocolByProductId(productId);
            }


        } catch (Exception ex) {
            logs.write(ex, "通过产品id获取产品协议");
        }

        if (protocolInfo == null) {
            protocolInfo = products.getProductDefaultProtocolInfo();
        }

        return protocolInfo;
    }

    /**
     * 获取产品的默认协议
     *
     * @param productId 产品id
     * @return
     */
    public ProductProtocolInfo getDefaultProductProtocolByProductId(Integer productId) {

        try {

            return products.getDefaultProductProtocolByProductId(productId);

        } catch (Exception ex) {

            logs.write(ex, "获取产品的默认协议");
        }
        return null;
    }

    /**
     * 获取参数的协议
     *
     * @param productId 产品Id
     * @param protocol  协议
     */
    @Deprecated
    public ProductProtocolInfo getProductProtocolByProtocl(Integer productId, String protocol) {

        try {

            return products.getProductProtocolByProtocl(productId, protocol);

        } catch (Exception ex) {
            logs.write(ex, "获取参数的协议");
        }

        return null;

    }

    /**
     * 获取产品协议的模板输出消息
     *
     * @param productId 产品Id
     * @param protocol  产品协议
     * @return
     */
    public String getOutTempletMessage(Integer productId, String protocol) {

        try {

            if (productId <= 0) {
                productId = 1;
            }


            ProductProtocolInfo protocolInfo = getProductProtocolByProductIdAndProtocol(productId, protocol);

            return protocolInfo.getOutTemplet();
        } catch (Exception ex) {

            logs.write(ex, "获取产品协议的模板输出消息");
        }

        return "";

    }


    public static void main(String[] args) {

        String s2 = "您好,您的探测器{deviceSN}发出一次{warn}.";

        String regex = "{[\\w]+}";

        List<String> stringList = ValidateHelper.find(regex, s2);

        // System.out.println(stringList.toString());
    }

    /**
     * @param productId
     */
    public String getProductNameByProductId(String productId) {

        try {

            Integer productId2 = TypeHelper.stringToInt(productId);

            return getProductNameByProductId(productId2);

        } catch (Exception ex) {

            logs.write(ex, "通过设备id查询产品名称");
        }

        return "";
    }

    /**
     * 通过设备id查询产品名称
     *
     * @param productId 产吕Id
     */
    public String getProductNameByProductId(Integer productId) {

        try {

            return products.getProductByProductId(productId).getName();


        } catch (Exception ex) {
            logs.write(ex, "通过设备id查询产品名称");
        }

        return "";
    }

    /**
     * 查找产品
     *
     * @param productSN 产品编码
     * @return
     */
    public ProductInfo getProductByProductSN(String productSN) {
        ProductInfo result = null;
        try {
            result = products.getProductByProductSN(productSN);
        } catch (Exception e) {
            logs.write(e, "查找产品异常，请检查");
        }
        return result;
    }

    /**
     * 查询协议参数
     *
     * @param protocolId
     * @return
     */
    public List<ProductProtocolParamInfo> getProductProtocolParamByProtocolId(Integer protocolId) {
        List<ProductProtocolParamInfo> result = Lists.newArrayList();
        try {
            result = products.getProductProtocolParamByProtocolId(protocolId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取产品所有协议
     *
     * @param productId
     * @return
     */
    public List<ProductProtocolInfo> getAllProtocolByProtoclId(Integer productId) {
        List<ProductProtocolInfo> result = Lists.newArrayList();
        try {
            result = products.getAllProtocolByProtoclId(productId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    //endregion


    //region 产品


    /**
     * 根据产品
     *
     * @param code 产品编码
     * @return IProduct
     */
    public IProduct getProduct(String code) {

        try {
            return cwmProduct.getProduct(code);
        } catch (Exception ex) {
            logs.write(ex, "");
        }
        return null;

    }


    /**
     * 获取所有插件
     *
     * @return Map<String, IProduct>
     */
    public Map<String, IProduct> getProductList() {


        try {
            return cwmProduct.getProductList();
        } catch (Exception ex) {
            logs.write(ex, "获取所有插件");
        }

        return Maps.newHashMap();
    }


    /**
     * 获取产品的下拉列表
     *
     * @return List<SelectListItem>
     */
    public List<SelectListItem> getProductItemList() {
        return getProductItemList(null);
    }

    /**
     * 获取产品的下拉列表
     *
     * @param code code
     * @return List<SelectListItem>
     */
    public List<SelectListItem> getProductItemList(String code) {

        List<SelectListItem> itemList = Lists.newArrayList();

        try {

            Map<String, IProduct> maps = getProductList();

            maps.values().stream()
                    .filter(Objects::nonNull)
                    .forEach(product -> {

                        SelectListItem item = SelectListItem.builder()
                                .text(product.name())
                                .value(product.code()).build();

                        if (!Strings.isNullOrEmpty(code) && code.equals(product.code())) {
                            item.setSelected(true);
                        }

                        itemList.add(item);
                    });

        } catch (Exception ex) {
            logs.write(ex, "获取产品的下拉列表");
        }

        return itemList;
    }

    /**
     * 删除产品协议和参数
     *
     * @param productId 产品Id
     * @return boolean
     */
    public boolean deleteProductProtocolByProductId(Integer productId) {

        try {
            return products.deleteProductProtocolByProductId(productId);
        } catch (Exception ex) {
            logs.write(ex, "删除产品协议和参数");
        }

        return false;
    }

    //endregion
}
