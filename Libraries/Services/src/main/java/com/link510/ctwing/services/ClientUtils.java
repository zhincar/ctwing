package com.link510.ctwing.services;


import com.google.common.collect.Maps;
import com.link510.ctwing.core.client.CWMClient;
import com.link510.ctwing.core.helper.MapHelper;
import com.link510.ctwing.core.log.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.util.Map;


/**
 * 客户端请求工具
 */
@Service(value = "ClientUtils")
public class ClientUtils {

    @Autowired
    private CWMClient cwmClient;

    @Autowired
    private Logs logs;


    /**
     * 远程json请求数据
     *
     * @param httpUrl     请求的地址
     * @param jsonStrName 请求参数名称
     * @param jsonStr     请求的串
     */
    public String postJson(String httpUrl, String jsonStrName, String jsonStr) throws Exception {


        try {
            Map<String, Object> params = Maps.newHashMap();

            params.put(jsonStrName, jsonStr);

            return post(httpUrl, params);

        } catch (Exception ex) {
            logs.write(ex, "远程请求");
            throw ex;
        }
    }

//    /**
//     * 远程post请求数据
//     *
//     * @param httpUrl 请求的地址
//     * @param params  请求的参数
//     * @return
//     */
//    public String post(String httpUrl, Map<String, String> params) throws Exception {
//        try {
//
//
//            return post(httpUrl, params);
//
//        } catch (Exception ex) {
//            logs.write(ex, "远程post请求数据");
//            throw ex;
//        }
//    }

    /**
     * 远程post请求数据
     *
     * @param httpUrl 请求的地址
     * @param params  请求的参数
     * @return
     */
    public String post(String httpUrl, Map<String, Object> params) throws Exception {

        try {
            return cwmClient.post(httpUrl, params);
        } catch (Exception ex) {
            logs.write(ex, "远程post请求数据");
            throw ex;
        }
    }

    /**
     * postJSON数据提交
     *
     * @param httpUrl 请求的地址
     * @param params  请求的参数
     * @return
     * @throws Exception
     */
    public String postJson(String httpUrl, MultiValueMap<String, Object> params) throws Exception {
        try {

            Map<String, Object> maps = MapHelper.toHashMap(params);

            return postJson(httpUrl, maps);

        } catch (Exception ex) {
            logs.write(ex, "远程post请求数据");
            throw ex;
        }
    }

    /**
     * postJSON数据提交
     *
     * @param httpUrl 请求的地址
     * @param params  请求的参数
     * @return
     * @throws Exception
     */
    public String postJson(String httpUrl, Map<String, Object> params) throws Exception {
        try {

            return cwmClient.postJson(httpUrl, params);

        } catch (Exception ex) {
            logs.write(ex, "远程post请求数据");
            throw ex;
        }
    }

    /**
     * 远程post请求数据
     *
     * @param httpUrl 请求的地址
     * @return
     */
    public String get(String httpUrl) throws Exception {

        try {

            return cwmClient.get(httpUrl);

        } catch (Exception ex) {
            logs.write(ex, "远程get请求数据");
            throw ex;
        }


    }
}
