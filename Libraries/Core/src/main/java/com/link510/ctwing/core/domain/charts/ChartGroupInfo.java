package com.link510.ctwing.core.domain.charts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author cqnews
 */ //chartgroup
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_chart_group")
public class ChartGroupInfo implements Serializable {


    private static final long serialVersionUID = -3276472750956203681L;
    /**
     * auto_increment
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "groupid")
    private Integer groupid = 0;
    /**
     * 组名
     */
    @Column(name = "name", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String name = "";
    /**
     * 说明
     */
    @Column(name = "description", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String description = "";

    /**
     * 产品ID
     */
    @Column(name = "productid", nullable = false, length = 500)
    @ColumnDefault(value = "0")
    private Integer productId = 0;
}