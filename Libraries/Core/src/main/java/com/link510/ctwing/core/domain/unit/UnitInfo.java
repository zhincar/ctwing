/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.core.domain.unit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 单位模型
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_units")
public class UnitInfo implements Serializable {



    private static final long serialVersionUID = 349404037675567269L;

    /**
     * 单位编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unitid")
    private Integer unitId = 0;

    /**
     * 编码（保证唯一，由其所属区域编码+id三位）
     */
    @Column(name = "unitcode", unique = true, nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String unitCode = "";
    /**
     * 名称
     */
    @Column(name = "name", unique = true, nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String name = "";


    /**
     * 拼音简写
     */
    @Column(name = "shorthand", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    private String shorthand = "";


    /**
     * 状态(0：禁用，1：启用)
     */
    @Column(name = "state", nullable = false)
    @ColumnDefault(value = "0")
    private Integer state = 0;

    /**
     * 单位类型(1=企业、2=林场、3=行政)
     */
    @Column(name = "type", nullable = false)
    @ColumnDefault(value = "1")
    private Integer type = 1;


    /**
     * 单位性质(1=国有、2=私营、3=集体)
     */
    @Column(name = "property", nullable = false)
    @ColumnDefault(value = "1")
    private Integer property = 1;


    /**
     * 所属区域编码
     */
    @Column(name = "areacode", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    private String areaCode = "";
    /**
     * 排序序号
     */
    @Column(name = "sort", nullable = false, length = 20)
    @ColumnDefault(value = "50")
    private Integer sort = 50;


    /**
     * 单位地址
     */
    @Column(name = "address", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String address = "";
    /**
     * 单位法人
     */
    @Column(name = "corporation", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    private String corporation = "";
    /**
     * 营业执照号
     */
    @Column(name = "buslicenum", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String busLiceNum = "";


    /**
     * 单位组织机构代码
     */
    @Column(name = "orgcode", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String orgCode = "";


    /**
     * 单位税务登记号
     */
    @Column(name = "texregnum", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String texRegNum = "";


    /**
     * 单位负责人名称
     */
    @Column(name = "mastername", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    private String masterName = "";


    /**
     * 单位负责人电话
     */
    @Column(name = "masterphone", nullable = false, length = 15)
    @ColumnDefault(value = "''")
    private String masterPhone = "";


    /**
     * 单位负责人手机
     */
    @Column(name = "mastermobile", nullable = false, length = 15)
    @ColumnDefault(value = "''")
    private String masterMobile = "";


    /**
     * 单位联系人名称
     */
    @Column(name = "contactsname", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    private String contactsName = "";


    /**
     * 单位联系人电话
     */
    @Column(name = "contactsphone", nullable = false, length = 15)
    @ColumnDefault(value = "''")
    private String contactsPhone = "";


    /**
     * 单位联系人手机
     */
    @Column(name = "contactsmobile", nullable = false, length = 15)
    @ColumnDefault(value = "''")
    private String contactsMobile = "";


    /**
     * 单位联系人邮箱
     */
    @Column(name = "contactsemail", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String contactsEmail = "";


    /**
     * 单位公共IP
     */
    @Column(name = "publicip", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String publicIp = "";


    /**
     * 纬度
     */
    @Column(name = "latitude", nullable = false)
    @ColumnDefault(value = "0.00")
    private double latitude = 0.00d;

    /**
     * 经度
     */
    @Column(name = "longitude", nullable = false)
    @ColumnDefault(value = "0.00")
    private double longitude = 0.00d;

    /**
     * wg经度
     */
    @Column(name = "wglng", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String wgLng = "";

    /**
     * wg纬度
     */
    @Column(name = "wglat", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String wgLat = "";


    /**
     * 是否开通微信报警(1：开通，0：未开通)
     */
    @Column(name = "openwx", nullable = false)
    @ColumnDefault(value = "0")
    private Integer openWx = 0;

    /**
     * 平台名称
     */
    @Column(name = "platformname", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String platformName = "";


    /**
     * ApiKey
     */
    @Column(name = "apikey", unique = true, nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String apiKey = "";

    /**
     * ApiKey
     */
    @Column(name = "apisecret", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String apiSecret = "";

    /**
     * 盐值
     */
    @Column(name = "salt", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String salt = "";

    /**
     * 首页配置地址
     */
    @Column(name = "indexUrl", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String indexUrl = "";

}
