package com.link510.ctwing.core.domain.device;


import com.link510.ctwing.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 在线网关
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_gateway_online")
public class OnlineGateWayInfo extends SupperGateWayInfo {

    private static final long serialVersionUID = 8470877759659976261L;

    /**
     * application
     */
    @Column(name = "application", nullable = false,length = 255)
    @ColumnDefault(value = "''")
    private String application = "";

    /**
     * 连接通道id
     */
    @Column(name = "channelid", nullable = false, length = 255)
    @ColumnDefault(value = "0")
    private String channelId = "";

    //    /**
    //     * 连接方式
    //     */
    //    @Column(name = "tcpmethod", nullable = false, length = 20)
    //    private String tcpMethod = "";

    /**
     * 在线时间
     */
    @Column(name = "onlinetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer onlineTime = UnixTimeHelper.getUnixTimeStamp();

    /**
     * 最后一次心跳包
     */
    @Column(name = "lasthearttime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer lastHeartTime = UnixTimeHelper.getUnixTimeStamp();

    /**
     * 校验时间戳
     */
    @Column(name = "timestamp", nullable = false)
    @ColumnDefault(value = "''")
    private String timestamp = "";

    /**
     * 校验随机数据
     */
    @Column(name = "mark", nullable = false)
    @ColumnDefault(value = "''")
    private String mark = "";


    /**
     * 校验随机码
     */
    @Column(name = "crc", nullable = false)
    @ColumnDefault(value = "''")
    private String crc = "";

    /**
     * 在线状态
     */
    @Column(name = "onlinestate", nullable = false)
    @ColumnDefault(value = "0")
    private Integer onlineState = 0;


    public OnlineGateWayInfo(GateWayInfo gateWayInfo, String channelId, String tcpMethod, String timestamp, String mark, String crc) {

        this.version = gateWayInfo.getVersion();
        this.description = gateWayInfo.getDescription();
        this.longitude = gateWayInfo.getLongitude();
        this.latitude = gateWayInfo.getLatitude();
        this.token = gateWayInfo.getToken();
        this.encryptKey = gateWayInfo.getEncryptKey();
        this.channelId = channelId;
        this.tcpMethod = tcpMethod;
        this.onlineTime = UnixTimeHelper.getUnixTimeStamp();
        this.lastHeartTime = UnixTimeHelper.getUnixTimeStamp();
        this.timestamp = timestamp;
        this.mark = mark;
        this.crc = crc;
        this.litpic = gateWayInfo.getLitpic();
        this.regionId = gateWayInfo.getRegionId();
        this.address = gateWayInfo.getAddress();
        this.addTime = gateWayInfo.getAddTime();
        this.updateTime = gateWayInfo.getUpdateTime();
    }
}
