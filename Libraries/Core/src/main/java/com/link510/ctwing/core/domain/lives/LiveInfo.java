package com.link510.ctwing.core.domain.lives;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 直播
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_lives")
public class LiveInfo implements Serializable {

    private static final long serialVersionUID = 3139749285148337273L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "liveid")
    private Integer liveId = -1;

    /**
     * uid
     */
    @Column(name = "uid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String uid = "";


    /**
     * uid
     */
    @Column(name = "nickname", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String nickName = "";

    /**
     * 直播主题
     */
    @Column(name = "title", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String title = "";


    /**
     * 直播描述
     */
    @Column(name = "description", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String description = "";


    /**
     * 显示地址
     */
    @Column(name = "address", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String address = "";

    /**
     * 二维码地址
     */
    @Column(name = "code2", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String code2 = "";

    /**
     * 经度
     */
    @Column(name = "longitude", nullable = false)
    @ColumnDefault(value = "0.00")
    private Double longitude = 0.00;

    /**
     * 纬度
     */
    @Column(name = "latitude", nullable = false)
    @ColumnDefault(value = "0.00")
    private Double latitude = 0.00;


    /**
     * 直播状态
     */
    @Column(name = "livestate", nullable = false)
    @ColumnDefault(value = "0")
    private Integer liveState = 0;


    /**
     * 开始时间
     */
    @Column(name = "starttime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer startTime = 0;


    /**
     * 过期时间
     */
    @Column(name = "expiretime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer expireTime = 0;

    /**
     * 参与人数
     */
    @Column(name = "count", nullable = false)
    @ColumnDefault(value = "0")
    private Integer count = 0;

    /**
     * 全部消息
     */
    @Column(name = "isall", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isAll = 0;


    /**
     * 是否推送心跳类消息
     */
    @Column(name = "isheart", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isHeart = 0;

    /**
     * 是否推送数据类消息
     */
    @Column(name = "isdata", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isData = 0;

    /**
     * 是否推送报警类消息
     */
    @Column(name = "iswarning", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isWarning = 0;


    /**
     * 过期时间
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer addTime = 0;


    /**
     * 过期时间
     */
    @Column(name = "updatetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer updateTime = 0;

}
