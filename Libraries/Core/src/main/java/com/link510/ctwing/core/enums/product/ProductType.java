package com.link510.ctwing.core.enums.product;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

/**
 * 产品类型
 *
 * @author cqnews
 */

@AllArgsConstructor
@Getter
public enum ProductType {



    Camera(7, "camera", "摄像机", false, "", "", ""),

    Environment(8, "environment", "环境探测器", false, "", "", ""),

    Default(0, "default", "普通设备", false, "", "", "");

    /**
     * 产品Id
     */
    private Integer typeId;

    /**
     * 设备编码
     */
    private String code;

    /**
     * 名称
     */
    private String title;


    /**
     * 是否显示
     */
    private boolean isShow;


    /**
     * 正常图标状态
     */
    private String icon;

    /**
     * 关闭图标状态
     */
    private String iconOff;

    /**
     * 图标热点状态
     */
    private String iconHover;

    /**
     * 获取产品
     *
     * @param typeId type
     * @return ProductType
     */
    public static ProductType getProductType(Integer typeId) {

        try {

            return Arrays.stream(values())
                    .filter(Objects::nonNull)
                    .filter(x -> x.getTypeId().equals(typeId))
                    .findFirst().orElse(null);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    /**
     * 获取产品
     *
     * @param code code
     * @return
     */
    public static ProductType getProductType(String code) {

        try {

            return Arrays.stream(values()).filter(x -> x.code.equals(code)).findFirst().orElse(Default);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return Default;
    }


}
