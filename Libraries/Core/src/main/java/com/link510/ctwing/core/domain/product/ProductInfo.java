package com.link510.ctwing.core.domain.product;

import com.link510.ctwing.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * 产品
 *
 * @author cqnews
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_products")
public class ProductInfo implements Serializable {

    private static final long serialVersionUID = 7393405761103295260L;
    /**
     * 产品的id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "productid", nullable = false)
    private Integer productId = -1;


    /**
     * 产品编号
     */
    @Column(name = "prodcutsn", nullable = false, unique = true, length = 80)
    @ColumnDefault(value = "''")
    private String productSN = "";


    /**
     * 产品内置编号
     */
    @Column(name = "productcode", nullable = false, unique = true, length = 80)
    @ColumnDefault(value = "''")
    private String productCode = "";


    /**
     * 协议
     */
    @Column(name = "tcp", nullable = false, length = 50)
    @ColumnDefault(value = "'tcp'")
    private String protocol = "tcp";


    /**
     * uid
     */
    @Column(name = "uid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String uid = "";

    /**
     * 产品名称
     */
    @Column(name = "name", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String name = "";


    /**
     * 设备图片
     */
    @Column(name = "litpic", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String litpic = "";


    /**
     * 生产公司
     */
    @Column(name = "company", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String company = "";


    /**
     * 产品的版本
     */
    @Column(name = "version", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    private String version = "";

    /**
     * 上线时间
     */
    @Column(name = "onlinetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer onlineTime = 0;

    /**
     * 分组描述
     */
    @Column(name = "description", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String description = "";

    /**
     * 设备详情描述
     */
    @Column(name = "body", nullable = false)
    @ColumnDefault(value = "''")
    private String body = "";

    /**
     * 新增时间
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer addTime = UnixTimeHelper.getUnixTimeStamp();

    /**
     * 更新时间
     */
    @Column(name = "updatetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer updateTime = UnixTimeHelper.getUnixTimeStamp();


    /**
     * token
     */
    @Column(name = "token", unique = true, nullable = false)
    @ColumnDefault(value = "''")
    private String token = "";


    /**
     * 密钥
     */
    @Column(name = "secretkey", unique = true, nullable = false)
    @ColumnDefault(value = "''")
    private String secretkey = "";

    /**
     * 盐值
     */
    @Column(name = "salt", nullable = false, length = 16)
    @ColumnDefault(value = "''")
    private String salt = "";


    /**
     * 是否启用产品
     */
    @Column(name = "isenable", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isEnable = 0;

//    /**
//     * 是否支持解析
//     */
//    @Column(name = "isparse", nullable = false)
//    @ColumnDefault(value = "0")
//    private Integer isParse = 0;


    /**
     * 插件编号
     */
    @Column(name = "pluginid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer pluginId = 0;

//    /**
//     * 解析格式 FF 10111111 255
//     */
//    @Column(name = "format", nullable = false, length = 10)
//    @ColumnDefault(value = "0")
//    private Integer format = 0;

//    /**
//     * 数据提取key,供json使用
//     */
//    @Column(name = "datakey", nullable = false, length = 20)
//    @ColumnDefault(value = "''")
//    private String dataKey = "";

//    /**
//     * 数据提取开始位置
//     */
//    @Column(name = "datastart", nullable = false)
//    @ColumnDefault(value = "0")
//    private Integer dataStart = 0;

//    /**
//     * 数据提取长度
//     */
//    @Column(name = "datalength", nullable = false)
//    @ColumnDefault(value = "0")
//    private Integer dataLength = 0;

//    /**
//     * 数据机校验位
//     */
//    @Column(name = "dataregex", nullable = false, length = 250)
//    @ColumnDefault(value = "''")
//    private String dataRegex = "";

//    /**
//     * 协议提取key
//     */
//    @Column(name = "protocolkey", nullable = false, length = 20)
//    @ColumnDefault(value = "''")
//    private String protocolKey = "";

//    /**
//     * 协议提取开始位置
//     */
//    @Column(name = "protocolstart", nullable = false)
//    @ColumnDefault(value = "0")
//    private Integer protocolStart = 0;

//    /**
//     * 协议提取长度
//     */
//    @Column(name = "protocollength", nullable = false)
//    @ColumnDefault(value = "0")
//    private Integer protocolLength = 0;


    public ProductInfo(Integer productId, String name) {
        this.productId = productId;
        this.name = name;
    }


    public Map<String, String> toSecurityMap() {

        Map<String, String> maps = new HashMap<>();

        try {
            maps.put("productId", productId.toString());
            maps.put("productSN", productSN);
            maps.put("name", name);
            maps.put("litpic", litpic);
            maps.put("company", company);
            maps.put("description", description);
            maps.put("body", body);

        } catch (Exception ignored) {

        }

        return maps;
    }
}
