package com.link510.ctwing.core.enums.chart;

import com.link510.ctwing.core.model.SelectListItem;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public enum ChartTypeEnum {
    /**
     * 统计类型
     */
    TIME_VARYING(1, "数据流"),
    STATISTIC(2, "统计数据");;
    @Getter
    private Integer id;
    @Getter
    private String name;

    ChartTypeEnum(Integer id, String type) {
        this.id = id;
        this.name = type;
    }

    public static List<SelectListItem> getSelectList(Integer id) {
        return Arrays.stream(ChartTypeEnum.values()).map(i -> new SelectListItem(i.getId().intValue() == id, i.getName(), i.getId() + "")).collect(toList());
    }
}
