package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.device.GateWayInfo;
import com.link510.ctwing.core.domain.device.OnlineGateWayInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

/**
 * 设备操作规则
 */
@SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
public interface IGateWayStrategy {


    /**
     * 获取所有设备列表
     *
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<GateWayInfo> getGateWayList() throws IOException;


    /**
     * 查询设备
     *
     * @param gateWayInfo 设备token
     * @param channelId   channelId
     * @param tcpMethod   连接方式
     * @param timestamp   时间戳
     * @param mark        随机密码
     * @return
     * @throws IOException
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo loginGateWay(GateWayInfo gateWayInfo, String channelId, String tcpMethod, String timestamp, String mark, String crc);

    /**
     * 更新上线状态
     *
     * @param channelId
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo updateOnlineState(String channelId);

    /**
     * 更新心跳包
     *
     * @param channelId
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo updateOnlineHeart(String channelId);

    /**
     * 删除在线设备
     *
     * @param token token
     * @throws IOException
     */
    void deleteByToken(String token) throws IOException;

    /**
     * 删除在线设备
     *
     * @param channelId channelId
     * @throws IOException
     */
    void deleteByChannelId(String channelId) throws IOException;


    /**
     * 通过通道id获取设备上线信息
     *
     * @param token 通道id
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo getOnlineGateWayByToken(String token) throws IOException;

    /**
     * 通过连接通道id 获取设备信息
     *
     * @param channelId 通道id
     * @return
     * @throws IOException
     */
    OnlineGateWayInfo getOnlineGateWayByChannelId(String channelId) throws IOException;

    /**
     * 判断通道是否存在
     *
     * @param channelId
     * @param token
     * @return
     * @throws IOException
     */
    boolean existsByChannelIdOrToken(String channelId, String token) throws IOException;

    /**
     * 判断设备是否存在于数据库中
     *
     * @param token
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    boolean existsByToken(String token) throws IOException;

    /**
     * 更新在线设备信息
     *
     * @param gateWayInfo 设备信息
     * @param channelId   设备id
     * @param tcpMethod   连接方式
     * @param timestamp   设备时间戳
     * @param mark        随机数
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo updateLoginGateWay(GateWayInfo gateWayInfo, String channelId, String tcpMethod, String timestamp, String mark, String crc) throws IOException;

    /**
     * 删除心跳过期设备
     *
     * @param outtime 过期时间
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteTimeOutGateWay(Integer outtime) throws IOException;



    //region 设备

    /**
     * 获得设备数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getGateWayCount(Specification<GateWayInfo> condition) throws IOException;


    /**
     * 创建一条设备数据
     *
     * @param gateWayInfo 设备模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    GateWayInfo createGateWay(GateWayInfo gateWayInfo) throws IOException;

    /**
     * 检测设备是否存在
     *
     * @param token token
     * @return
     */
    boolean isGateWayExists(String token) throws IOException;

    /**
     * 更新一条设备数据
     *
     * @param gateWayInfo 设备模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    GateWayInfo updateGateWay(GateWayInfo gateWayInfo) throws IOException;

    /**
     * 删除一条设备数据
     *
     * @param id 设备模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteGateWayById(int id) throws IOException;

    /**
     * 批量删除一批设备数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteGateWayByIdList(String idList) throws IOException;


    /**
     * 获得设备一条记录
     *
     * @param id deviceId
     * @return 返回一条GateWayInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    GateWayInfo getGateWayById(int id) throws IOException;

    /**
     * 通过token 获取设备信息
     * @param token
     * @return
     */
    GateWayInfo getGateWayByToken(String token);

    /**
     * 获得设备数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回GateWayInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<GateWayInfo> getGateWayList(Specification<GateWayInfo> condition, Sort sort) throws IOException;


    /**
     * 获得设备数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回GateWayInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<GateWayInfo> getGateWayList(Integer pageSize, Integer pageNumber, Specification<GateWayInfo> condition, Sort sort) throws IOException;


    //endregion 设备结束


    //region 在线设备

    /**
     * 获得在线设备数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getOnlineGateWayCount(Specification<OnlineGateWayInfo> condition) throws IOException;


    /**
     * 创建一条在线设备数据
     *
     * @param onlineGateWayInfo 在线设备模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo creategateway(OnlineGateWayInfo onlineGateWayInfo) throws IOException;


    /**
     * 更新一条在线设备数据
     *
     * @param onlineGateWayInfo 在线设备模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo updateOnlineGateWay(OnlineGateWayInfo onlineGateWayInfo) throws IOException;

    /**
     * 删除一条在线设备数据
     *
     * @param id 在线设备模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteOnlineGateWayById(int id) throws IOException;

    /**
     * 批量删除一批在线设备数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteOnlineGateWayByIdList(String idList) throws IOException;


    /**
     * 获得在线设备一条记录
     *
     * @param id deviceId
     * @return 返回一条OnlineGateWayInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    OnlineGateWayInfo getOnlineGateWayById(int id) throws IOException;

    /**
     * 获得在线设备数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OnlineGateWayInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<OnlineGateWayInfo> getOnlineGateWayList(Specification<OnlineGateWayInfo> condition, Sort sort) throws IOException;


    /**
     * 获得在线设备数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OnlineGateWayInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<OnlineGateWayInfo> getOnlineGateWayList(Integer pageSize, Integer pageNumber, Specification<OnlineGateWayInfo> condition, Sort sort) throws IOException;

    /**
     * 获取网关列表
     *
     * @param pageSize
     * @param pageNumber
     * @param token
     * @param sort
     * @return
     * @throws IOException
     */
    Page<GateWayInfo> getAuthorGateWayList(Integer pageSize, Integer pageNumber, String uid, String token, Sort sort) throws IOException;




    //endregion 在线设备结束
}


