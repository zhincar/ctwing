package com.link510.ctwing.core.domain.lives;

import com.link510.ctwing.core.domain.device.DeviceInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 直播设备列表
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_live_device")
public class LiveDeviceInfo implements Serializable {

    private static final long serialVersionUID = -1023611877324623151L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id = 0;


    /**
     * 直播Id
     */
    @Column(name = "liveid", nullable = false)
    private Integer liveId = 0;


    /**
     * 设备Id
     */
    @Column(name = "deviceid", nullable = false)
    private Integer deviceId = 0;


    /**
     * 设备编号
     */
    @Column(name = "devicesn", nullable = false)
    private String deviceSN = "";

    /**
     * 设备地址
     */
    @Column(name = "address", nullable = false)
    private String address = "";


    public static LiveDeviceInfo apply(LiveInfo liveInfo, DeviceInfo deviceInfo) {
        LiveDeviceInfo info = new LiveDeviceInfo();
        info.setLiveId(liveInfo.getLiveId());
        info.setDeviceId(deviceInfo.getDeviceId());
        info.setDeviceSN(deviceInfo.getDeviceSN());
        info.setAddress(deviceInfo.getAddress());
        return info;
    }
}
