package com.link510.ctwing.core.enums.device;


import com.link510.ctwing.core.helper.TypeHelper;
import com.link510.ctwing.core.model.SelectListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * 设备工作模式
 */
public enum DeviceModel {

    /**
     * 设备工作模式
     */
    Normal(0, "正常模式"),
    Debug(1, "调试模式");

    /**
     * 键值
     */
    private Integer index;

    /**
     * 名称
     */
    private String name;

    DeviceModel(Integer index, String name) {
        this.index = index;
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DeviceModel{" +
                "index=" + index +
                ", name='" + name + '\'' +
                '}';
    }

    public static String getName(Integer index) {

        for (DeviceModel model : values()) {

            if (model.getIndex().equals(index)) {
                return model.getName();
            }

        }

        return Normal.getName();
    }


    public static DeviceModel getDeviceModel(Integer index) {

        for (DeviceModel model : values()) {

            if (model.getIndex().equals(index)) {
                return model;
            }

        }

        return Normal;
    }

    /**
     * 获取枚举的列表
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem() {
        return getSelectListItem(1);
    }


    /**
     * 获取枚举的列表
     * index 默认选择项目
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem(Integer index) {
        List<SelectListItem> selectListItemList = new ArrayList<SelectListItem>();

        for (DeviceModel model : values()) {

            SelectListItem item = new SelectListItem();

            item.setText(model.getName());
            item.setValue(TypeHelper.intToString(model.getIndex()));

            if (model.getIndex().equals(index)) {
                item.setSelected(true);
            }

            selectListItemList.add(item);
        }

        return selectListItemList;
    }

}
