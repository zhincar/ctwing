/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.core.domain.unit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_unit_subscribes")
public class UnitSubscribeInfo implements Serializable {

    private static final long serialVersionUID = 3448667108349741188L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "unitid")
    private Integer subId = 0;

    /**
     * 单位编号
     */
    @Column(name = "unitcode", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String unitCode = "";

    /**
     * 订阅名称
     */
    @Column(name = "title", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String title = "";

    /**
     * 产品Id
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer productId = 0;

    /**
     * 协议
     */
    @Column(name = "protocol", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String protocol = "";

    /**
     * 协议
     */
    @Column(name = "httpurl", nullable = false, length = 350)
    @ColumnDefault(value = "''")
    private String httpUrl = "";

    /**
     * 订阅格式
     */
    @Column(name = "format", nullable = false)
    @ColumnDefault(value = "0")
    private Integer format = 0;

    /**
     * 订阅状态
     */
    @Column(name = "substate", nullable = false)
    @ColumnDefault(value = "0")
    private Integer subState = 0;


    /**
     * 设备id
     */
    @Column(name = "deviceid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String deviceId = "";


    @Column(name = "apikey", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    public String apiKey = "";

    /**
     * 添加时间
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer addTime = 0;

    /**
     * 更新时间
     */
    @Column(name = "updatetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer updateTime = 0;

}
