/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.link510.ctwing.core.envent;

/**
 * Created by cqnews on 2017/4/14.
 */

public interface IEvent {

    /**
     * 执行方法
     */
    void execute();

}
