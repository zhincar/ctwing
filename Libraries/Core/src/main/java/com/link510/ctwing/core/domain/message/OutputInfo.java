package com.link510.ctwing.core.domain.message;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 数据吐出消息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "w_output")
public class OutputInfo implements Serializable {

    private static final long serialVersionUID = 49455497417344007L;

    /**
     * 输出日志管理
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "logid")
    private Integer logId = 0;


    /**
     * 日志分类
     */
    @Column(name = "type", nullable = false)
    @ColumnDefault(value = "0")
    private Integer type = 0;


    /**
     * 来自的分组uid
     */
    @Column(name = "uid", nullable = false)
    @ColumnDefault(value = "''")
    private String uid = "";


    /**
     * 来自的分组
     */
    @Column(name = "groupid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer groupId = 0;

    /**
     * 来自的分组的动作
     */
    @Column(name = "actionid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer actionId = 0;

    /**
     * 消息Id
     */
    @Column(name = "msgid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer msgId = 0;


    /**
     * token
     */
    @Column(name = "token", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String token = "";

    /**
     * 产品id
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer productId = 0;

    /**
     * 设备编号
     */
    @Column(name = "devicesn", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String deviceSN = "";

    /**
     * 吐出地址
     */
    @Column(name = "httpurl", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String httpUrl = "";

    /**
     * 吐出的内容
     */
    @Column(name = "content", nullable = false)
    @ColumnDefault(value = "''")
    private String content = "";

    /**
     * 返回结果
     */
    @Column(name = "result", nullable = false)
    @ColumnDefault(value = "''")
    private String result = "";

    /**
     * 添加时间
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer addTime = 0;


}
