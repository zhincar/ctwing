package com.link510.ctwing.core.domain.lives;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_live_messages")
public class LiveMessageInfo implements Serializable {

    private static final long serialVersionUID = -230269093689356953L;


    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id = 0;


    /**
     * 直播原始消息Id
     */
    @Column(name = "msgid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer msgId = 0;


    /**
     * 直播 Id
     */
    @Column(name = "liveid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer liveId = 0;

    /**
     * 消息内容
     */
    @Column(name = "content", nullable = false, length = 5000)
    @ColumnDefault(value = "''")
    private String content = "";


    /**
     * 发送时间
     */
    @Column(name = "addtime")
    @ColumnDefault(value = "0")
    private Integer addTime = 0;

    public LiveMessageInfo(Integer msgId, Integer liveId, String content, Integer addTime) {
        this.msgId = msgId;
        this.liveId = liveId;
        this.content = content;
        this.addTime = addTime;
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }
}
