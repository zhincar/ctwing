package com.link510.ctwing.core.client;

import java.util.Map;

public interface IClientStrategy {

    /**
     * 远程post请求数据
     *
     * @param httpUrl 请求的地址
     * @param params  请求的参数
     * @return
     */
    String post(String httpUrl, Map<String, Object> params) throws Exception;


    /**
     * 远程post请求数据
     *
     * @param httpUrl 请求的地址
     * @param params  请求的参数
     * @return
     */
    String post(String httpUrl, Map<String, Object> params, Map<String, String> headers) throws Exception;

    /**
     * postJSON数据提交
     *
     * @param httpUrl 请求的地址
     * @param params  请求的参数
     * @return
     * @throws Exception
     */
    String postJson(String httpUrl, Map<String, Object> params) throws Exception;

    /**
     * postJSON数据提交
     *
     * @param httpUrl 请求的地址
     * @param params  请求的参数
     * @param headers 请求的头
     * @return
     * @throws Exception
     */
    String postJson(String httpUrl, Map<String, Object> params, Map<String, String> headers) throws Exception;

    /**
     * get数据提交
     *
     * @param httpUrl 请求的地址
     * @return
     * @throws Exception
     */
    String get(String httpUrl) throws Exception;

    /**
     * get数据提交
     *
     * @param httpUrl 请求的地址
     * @param headers 请求的包头
     */
    String get(String httpUrl, Map<String, String> headers) throws Exception;
}
