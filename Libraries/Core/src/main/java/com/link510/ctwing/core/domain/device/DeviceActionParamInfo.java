package com.link510.ctwing.core.domain.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 设备动作参数表
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_device_action_params")
public class DeviceActionParamInfo implements Serializable {


    private static final long serialVersionUID = -156916418061971771L;
    /**
     * 参数id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paramId", nullable = false)
    private Integer paramId = -1;


    /**
     * 动作id
     */
    @Column(name = "actionid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer actionId = -1;

    /**
     * 参数编码
     */
    @Column(name = "key2", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String key = "";

    /**
     * 对外key值
     */
    @Column(name = "tokey", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String tokey = "";

    /**
     * 默认值
     */
    @Column(name = "defaultvalue", nullable = false, length = 255)
    @ColumnDefault(value = "''")
    private String defaultValue = "";
}
