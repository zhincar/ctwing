package com.link510.ctwing.core.domain.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 产品协议
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_product_protocol_param")
public class ProductProtocolParamInfo implements Serializable {

    private static final long serialVersionUID = -6240061426951268768L;
    /**
     * 参数id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paramid", nullable = false)
    private Integer paramId = -1;

    /**
     * 所属产品类型
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer productId = -1;

    /**
     * 所属产品类型
     */
    @Column(name = "productname", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String productName = "";


    /**
     * 所属协议id
     */
    @Column(name = "protocolid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer protocolId = -1;


    /**
     * 所属协议名称
     */
    @Column(name = "protocolname", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String protocolName = "";

    /**
     * 参数名称
     */
    @Column(name = "name", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String name = "";

    /**
     * 参数唯一标识
     */
    @Column(name = "key2", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String key = "";
    /**
     * 协议描述
     */
    @Column(name = "description", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String description = "";


    /**
     * 数据类型
     */
    @Column(name = "datatype", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer dataType = 0;

    /**
     * 默认参数
     */
    @Column(name = "defaultvalue", nullable = false, length = 250)
    @ColumnDefault(value = "''")
    private String defaultValue = "";

    /**
     * 展示单位
     */
    @Column(name = "unit", nullable = false, length = 10)
    @ColumnDefault(value = "''")
    private String unit = "";


    /**
     * 消息类型
     */
    @Column(name = "format", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer format = 0;

    /**
     * 位始位置,供十六进度使用
     */
    @Column(name = "start", nullable = false)
    @ColumnDefault(value = "0")
    private Integer start = 0;

    /**
     * 长度
     */
    @Column(name = "length", nullable = false)
    @ColumnDefault(value = "0")
    private Integer length = 1;

    /**
     * 解析公式 @me*1.56/500
     */
    @Column(name = "expression", nullable = false, length = 250)
    @ColumnDefault(value = "''")
    private String expression = "";


    /**
     * 排序
     */
    @Column(name = "sort", nullable = false)
    @ColumnDefault(value = "50")
    private Integer sort = 50;


    public ProductProtocolParamInfo(String protocolName, String key, String name, String description, String defaultValue, String unit, Integer dataType) {
        this.protocolName = protocolName;
        this.name = name;
        this.key = key;
        this.description = description;
        this.dataType = dataType;
        this.defaultValue = defaultValue;
        this.unit = unit;
    }


    public ProductProtocolParamInfo(String protocolName, String key, String name, String description, String defaultValue, Integer dataType) {
        this.protocolName = protocolName;
        this.name = name;
        this.key = key;
        this.description = description;
        this.dataType = dataType;
        this.defaultValue = defaultValue;
    }

    public ProductProtocolParamInfo(String protocolName, String key, String name, String description,Integer dataType) {
        this.protocolName = protocolName;
        this.name = name;
        this.key = key;
        this.description = description;
        this.dataType = dataType;
    }
}
