package com.link510.ctwing.core.iniots;


import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "CWMIniot")
@Getter
public class CWMIniot {


    @Autowired(required = false)
    private IIniotStrategy iIniotStrategy;
}
