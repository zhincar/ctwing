package com.link510.ctwing.core.exption.product;


import com.link510.ctwing.core.exption.CWMException;

/**
 * 错误的产品异常
 * @author cqnews
 */
public class ErrorProductException extends CWMException {


    private static final long serialVersionUID = -1728952379668426005L;

    public ErrorProductException() {
        super();
    }

    public ErrorProductException(String message) {
        super(message);
    }

    public ErrorProductException(String message, Throwable cause) {
        super(message, cause);
    }

    public ErrorProductException(Throwable cause) {
        super(cause);
    }

    public ErrorProductException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
