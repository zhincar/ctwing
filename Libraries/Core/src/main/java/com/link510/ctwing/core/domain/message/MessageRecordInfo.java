package com.link510.ctwing.core.domain.message;

import com.link510.ctwing.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 消息解析记录表
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_message_record")
public class MessageRecordInfo implements Serializable {


    private static final long serialVersionUID = 5064102402121013812L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recordid")
    private Integer recordId = -1;

    /**
     * 消息id
     */
    @Column(name = "msgid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer msgId = -1;

    /**
     * token
     */
    @Column(name = "token", nullable = false)
    @ColumnDefault(value = "''")
    private String token = "";

    /**
     * 设备id
     */
    @Column(name = "devicesn", nullable = false)
    @ColumnDefault(value = "''")
    private String deviceSN = "";

    /**
     * 协议关键词
     */
    @Column(name = "protocol", nullable = false)
    @ColumnDefault(value = "''")
    private String protocol = "";

    /**
     * 消息等级
     */
    @Column(name = "msglevel", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer msgLevel = -1;


    /**
     * 协议描述
     */
    @Column(name = "protodesc", nullable = false)
    @ColumnDefault(value = "''")
    private String protodesc = "";

    /**
     * 关键词
     */
    @Column(name = "key2", nullable = false)
    @ColumnDefault(value = "''")
    private String key = "";

    /**
     * 参数值
     */
    @Column(name = "value", nullable = false)
    @ColumnDefault(value = "''")
    private String value = "";


    @Column(name = "desc2", nullable = false)
    @ColumnDefault(value = "''")
    private String desc = "";

    /**
     * 时间戳
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer addTime = UnixTimeHelper.getUnixTimeStamp();

    public MessageRecordInfo(String key) {
        this.key = key;
    }
}
