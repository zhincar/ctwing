
package com.link510.ctwing.core.data.rdbs.repository.messages;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.message.HeartMessageInfo;
import org.springframework.data.jpa.repository.Query;

public interface HeartMessageRepository extends BaseRepository<HeartMessageInfo, Integer> {


    /**
     * 统计时间段内的数量
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return //    @Query(value = "select count(info.msgId) from HeartMessageInfo info where info.timestamp>=?1 and info.timestamp<=?2")
     * //    long count(Integer startTime, Integer endTime);
     */
    long countHeartMessageInfoByTimestampBetween(Integer startTime, Integer endTime);

    /**
     * 获取最后一包心跳包时间
     *
     * @param token 设备编号
     * @return
     */
    @Query(value = "select max(info.timestamp) from HeartMessageInfo info where info.token = ?1 order by info.msgId desc")
    Integer getLastHeartMessageTimeByToken(String token);

}