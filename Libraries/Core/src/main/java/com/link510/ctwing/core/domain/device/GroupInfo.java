package com.link510.ctwing.core.domain.device;


import com.link510.ctwing.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Map;

/**
 * 设备分组
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_groups")
public class GroupInfo implements Serializable {


    private static final long serialVersionUID = 8642308360885090443L;
    /**
     * 分组id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "groupid", nullable = false)
    private Integer groupId = -1;

    /**
     * 分组名称
     */
    @Column(name = "name", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String name = "";


    /**
     * uid
     */
    @Column(name = "uid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    protected String uid = "";


    /**
     * 代理Id
     */
    @Column(name = "agentid", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer agentId = 0;


    /**
     * 设备类型Id
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer productId = 0;

    /**
     * 设备类型
     */
    @Column(name = "productname", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String productName = "";

    /**
     * 设备接入方式
     */
    @Column(name = "network", nullable = false, length = 20)
    @ColumnDefault(value = "4g")
    private String network = "4g";

    /**
     * 使用协议
     */
    @Column(name = "netprotocol", nullable = false, length = 20)
    @ColumnDefault(value = "tcp")
    private String netprotocol = "tcp";


    /**
     * 分组描述
     */
    @Column(name = "description", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String description = "";


    /**
     * 新增时间
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer addTime = UnixTimeHelper.getUnixTimeStamp();

    /**
     * 更新时间
     */
    @Column(name = "updatetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer updateTime = UnixTimeHelper.getUnixTimeStamp();

    /**
     * 是否启用分组
     */
    @Column(name = "isenable", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isEnable = 0;

    public GroupInfo(Integer groupId, String name) {
        this.groupId = groupId;
        this.name = name;
    }

    public GroupInfo(Map<String, Object> res) {
        this.groupId = Integer.parseInt(res.get("groupid") + "");
        this.name = res.get("name") + "";
        this.productId = (int) res.get("productId");
        this.network = res.get("network") + "";
        this.netprotocol = res.get("netprotocol") + "";
        this.description = res.get("description") + "";
        this.addTime = (int) res.get("addTime");
        this.updateTime = (int) res.get("updateTime");
        this.isEnable = (int) res.get("isEnable");
    }
}
