/*
 *  *  Copyright  2018.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2018.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.core.domain.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;


/**
 * 命令参数
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DynamicUpdate
@Table(name = "w_command_params")
public class CommandParamInfo {
    /**
     * 命令Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paramid", nullable = false)
    private Integer paramId = 0;

    /**
     * 命令Id
     */
    @Column(name = "commandid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer commandId = 0;

    /**
     * 命令名称
     */
    @Column(name = "paramname", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String paramName = "";

    /**
     * 命令名称
     */
    @Column(name = "defaultvalue", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String defaultValue = "";

    /**
     * 命令名称
     */
    @Column(name = "description", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String description = "";


}
