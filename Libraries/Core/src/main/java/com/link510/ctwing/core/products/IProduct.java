package com.link510.ctwing.core.products;

import java.io.Serializable;

/**
 * 产品中心
 */
public interface IProduct extends Serializable {

    /**
     * 设备编号
     */
    Integer typeId();

    /**
     * 产品代码
     */
    String code();

    /**
     * 产品名称
     */
    String name();


    /**
     * 版本
     */
    String version();


    /**
     * 是否电路控制
     *
     * @return 是否电路控制
     */
    default Integer isSwitch() {
        return 1;
    }


}
