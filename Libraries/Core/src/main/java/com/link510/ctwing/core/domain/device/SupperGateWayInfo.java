package com.link510.ctwing.core.domain.device;


import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 设备信息
 *
 * @author cqnews
 */
@Data
@MappedSuperclass
public class SupperGateWayInfo implements Serializable {

    private static final long serialVersionUID = 4685627065511962478L;
    /**
     * 开发者ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    protected Integer id = -1;


    /**
     * 单位编码
     */
    @Column(name = "unitcode", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    protected String unitCode = "";

    /**
     * uid
     */
    @Column(name = "uid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    protected String uid = "";


    @Column(name = "nickname", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    protected String nickName = "";
    
    /**
     * 代码商Id
     */
    @Column(name = "agentid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer agentId = 0;

    /**
     * 代码商名称
     */
    @Column(name = "agentname", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String agentName = "";

    /**
     * 网关名称
     */
    @Column(name = "name", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    protected String name = "";

    /**
     * token
     */
    @Column(name = "token", unique = true, nullable = false, length = 120)
    @ColumnDefault(value = "''")
    protected String token = "";


    /**
     * SIM
     */
    @Column(name = "sim", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    protected String sim = "";


    /**
     * 设备版本
     */
    @Column(name = "version", nullable = false)
    @ColumnDefault(value = "''")
    protected String version = "";

    /**
     * 设备描述
     */
    @Column(name = "description", nullable = false)
    @ColumnDefault(value = "''")
    protected String description = "";

    /**
     * 经度
     */
    @Column(name = "longitude", nullable = false)
    @ColumnDefault(value = "0.00")
    protected double longitude = 0.00;

    /**
     * 纬度
     */
    @Column(name = "latitude", nullable = false)
    @ColumnDefault(value = "0.00")
    protected double latitude = 0.00;


    /**
     * 设备缩略图
     */
    @Column(name = "litpic", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    protected String litpic = "";

    /**
     * 设备所在地域
     */
    @Column(name = "regionid", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer regionId = 0;


    /**
     * 设备安装地址
     */
    @Column(name = "address", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    protected String address = "";


    /**
     * 探测器添加时间
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer addTime = 0;


    /**
     * 探测器修改时间
     */
    @Column(name = "updatetime", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer updateTime = 0;

    /**
     * 密钥
     */
    @Column(name = "encryptkey", nullable = false, length = 120)
    @ColumnDefault(value = "1234")
    protected String encryptKey = "1234";

    /**
     * 连接方式
     */
    @Column(name = "tcpmethod", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    protected String tcpMethod = "";


}
