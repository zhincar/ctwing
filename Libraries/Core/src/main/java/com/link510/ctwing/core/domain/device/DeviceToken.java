package com.link510.ctwing.core.domain.device;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: cqnews
 * @Date: 2018-09-29 17:51
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceToken implements Serializable {

    private static final long serialVersionUID = 7075261125215507326L;

    /**
     * 用户uid
     */
    private String deviceSN = "";

    /**
     * 盐值
     */
    private String salt = "";

    /**
     * 过期时间
     */
    private Integer limitTime = 0;

}
