/*
 *
 *  *
 *  *  * Copyright (C) 2018.
 *  *  * 用于JAVA项目开发
 *  *  * 重庆青沃科技有限公司 版权所有
 *  *  * Copyright (C)  2018.  CqingWo Systems Incorporated. All rights reserved.
 *  *
 *
 */

package com.link510.ctwing.core.push;

import com.link510.ctwing.core.message.MessageInfo;

import java.io.IOException;
import java.util.Map;

/**
 * Created by cqnews on 2017/4/16.
 */


public interface IPushStrategy {


    /**
     * 发送消息
     *
     * @param deviceToken 机器
     * @param title       通知标题
     * @param text        通知内容
     * @param messageInfo 自定义消息的内容
     * @return
     * @throws IOException
     */
    boolean send(String deviceToken, String title, String text, MessageInfo messageInfo) throws IOException;


    /**
     * 发送广播
     *
     * @param title 通知标题
     * @param text  通知内容
     * @return
     * @throws IOException
     */
    boolean send(String title, String text, MessageInfo messageInfo) throws IOException;

    /**
     * 远程post请求数据
     *
     * @param params   请求的参数
     * @param deviceId 设备Id
     * @param apiKey   apikey
     * @return 返回的情况
     * @throws Exception 异常
     */
    String push(Map<String, Object> params, String deviceId, String apiKey) throws Exception;


}
