package com.link510.ctwing.core.domain.device;


import com.link510.ctwing.core.helper.TypeHelper;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Map;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "W_gateway")
public class GateWayInfo extends SupperGateWayInfo {


    private static final long serialVersionUID = 8470877759659976261L;


    private static Logger logger = LoggerFactory.getLogger("GateWayInfo");

    /**
     * @param params 解析参数
     */
    public static GateWayInfo of(Map params) {

        GateWayInfo gateWayInfo = null;

        try {
            gateWayInfo = new GateWayInfo();

            gateWayInfo.unitCode = TypeHelper.objectToString(params.get("unit_code"));
            gateWayInfo.token = TypeHelper.objectToString(params.get("code"));
            gateWayInfo.address = TypeHelper.objectToString(params.get("address"));
            gateWayInfo.longitude = TypeHelper.objectToDouble(params.get("lng"));
            gateWayInfo.latitude = TypeHelper.objectToDouble(params.get("lat"));
            gateWayInfo.sim = TypeHelper.objectToString(params.get("num"));
            gateWayInfo.addTime = UnixTimeHelper.getUnixTimeStamp();
            gateWayInfo.updateTime = gateWayInfo.addTime;

            gateWayInfo.name = TypeHelper.objectToString(params.get("name"));
            gateWayInfo.description = TypeHelper.objectToString(params.get("description"));
            gateWayInfo.regionId = TypeHelper.objectToInt(params.get("regionId"));


        } catch (Exception ex) {
            logger.error("数据解析失败," + ex.getMessage());
        }
        return gateWayInfo;
    }


}
