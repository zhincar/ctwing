package com.link510.ctwing.core.domain.charts;

import com.link510.ctwing.core.enums.chart.ChartModuleEnum;
import com.link510.ctwing.core.enums.chart.ChartTypeEnum;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;


//charttemplate
@Entity
@Table(name = "w_chart_template")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@DynamicUpdate
public class ChartTemplateInfo implements Serializable {


    private static final long serialVersionUID = -1198804442823997490L;
    /**
     * ID
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "chartid", nullable = false)
    private int chartId = 0;

    /**
     * 图表配置
     **/
    @Column(name = "option2", nullable = false, length = 5210)
    @ColumnDefault(value = "''")
    private String option2 = "";

    /**
     * 产品ID
     **/
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "0")
    private int productId = 0;

//    /**
//     * protocolId
//     **/
//    @Column(name = "protocolid", nullable = false)
//    @ColumnDefault(value = "0")
//    private int protocolId = 0;
    /**
     * 图表名
     **/
    @Column(name = "name", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String name = "";
    /**
     * 描述
     **/
    @Column(name = "description", nullable = false, length = 512)
    @ColumnDefault(value = "''")
    private String description = "";

    /**
     * isDefault 是否默认模板，0是；1否
     **/
    @Column(name = "isdefault", nullable = false)
    @ColumnDefault(value = "1")
    private int isDefault = 1;
    /**
     * 实时数据个数
     */
    @Column(name = "rtnum", nullable = false)
    @ColumnDefault(value = "100")
    private int rtNum = 100;

    /**
     * 历史数据个数
     */
    @Column(name = "hisnum", nullable = false)
    @ColumnDefault(value = "100")
    private int hisNum = 100;

    /**
     * 历史数据保留策略码
     */
    @Column(name = "savestrategycode", nullable = false)
    @ColumnDefault(value = "1")
    private int saveStrategyCode = 1;
    /**
     * 数据类型，数据流或统计数据 {@link ChartTypeEnum }
     */
    @Column(name = "datatype", nullable = false)
    @ColumnDefault(value = "1")
    private Integer dataType=1;
    /**
     * 支持模块id   {@link ChartModuleEnum }
     */
    @Column(name = "supportmoduleid", nullable = false)
    @ColumnDefault(value = "1")
    private Integer supportModuleId=1;
}