package com.link510.ctwing.core.iniots;


import com.link510.ctwing.core.domain.producrs.EnvironmentMessageInfo;
import com.link510.iniot.sdk.management.DeviceManagement;
import com.link510.iniot.sdk.management.MessageManagement;
import com.link510.iniot.sdk.management.SubscribeManagement;

import javax.transaction.NotSupportedException;
import java.io.Serializable;

/**
 * 环境探测器
 */
public interface IIniotStrategy extends Serializable {

    /**
     * 设备管理器
     *
     * @return 设备管理器
     */
    DeviceManagement getDeviceManagement();

    /**
     * 消息管理器
     *
     * @return 消息管理器
     */
    MessageManagement getMessageManagement();

    /**
     * 订阅管理器
     *
     * @return 订阅管理器
     */
    SubscribeManagement getSubscribeManagement();

    /**
     * 获取最近一条数据
     *
     * @param deviceSN 设备编号
     * @return EnvironmentMessageInfo
     * @throws NotSupportedException NotSupportedException
     */
    EnvironmentMessageInfo lastEnvironmentMessage(String deviceSN) throws NotSupportedException;


}
