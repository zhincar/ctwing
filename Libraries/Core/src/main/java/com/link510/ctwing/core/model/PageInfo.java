package com.link510.ctwing.core.model;

import lombok.*;

import java.util.List;

/**
 * 分页模型
 *
 * @param <T>
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PageInfo<T> {

    /**
     * 分页模型
     */
    private PageModel pageModel;

    /**
     * 列表
     */
    private List<T> list;
}
