package com.link510.ctwing.core.data.rdbs.repository.authors;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.authors.AuthorLogInfo;

/**
 * @author cqnews
 */
public interface AuthorLogRepository extends BaseRepository<AuthorLogInfo, Integer> {




}
