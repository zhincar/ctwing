package com.link510.ctwing.core.plugin.interface2;

import com.link510.ctwing.core.enums.device.NetworkEnums;

public interface IParserPlugin extends IPlugin {

    /**
     * 插件id
     *
     * @return
     */
    default Integer pluginId() {
        return 0;
    }

    /**
     * 网络模块
     *
     * @return
     */
    default Integer network() {
        return NetworkEnums.TCP.getId();
    }

    /**
     * 解析格式
     *
     * @return
     */
    default Integer format() {
        return -1;
    }

    /**
     * topic
     *
     * @return
     */
    String topic();

    /**
     * 广播
     *
     * @param msg 消息内容
     */
    void broadcast(String msg);


    /**
     * 单播消息
     *
     * @param deviceSN 通道id
     * @param msg       消息内容
     */
    void message(String deviceSN, String msg);

}
