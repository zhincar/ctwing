package com.link510.ctwing.core.domain.message;

import com.link510.ctwing.core.helper.UnixTimeHelper;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 消息根模型
 */
@MappedSuperclass
public class SupperMessageInfo implements Serializable {

    private static final long serialVersionUID = -9166219948367925756L;
    /**
     * 消息id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "msgid", nullable = false)
    protected Integer msgId = -1;


    /**
     * uid
     */
    @Column(name = "uid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    protected String uid = "";

    /**
     * 代理商Id
     */
    @Column(name = "agentid", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer agentId = 0;


    /**
     * 单位编号
     */
    @Column(name = "unitcode", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    protected String unitCode = "";

    /**
     * 设备id
     */
    @Column(name = "devicesn", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    protected String deviceSN = "";

    /**
     * 产品id
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer productId = 0;

    /**
     * 产品类型
     */
    @Column(name = "producttype", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer productType = 0;

    /**
     * 消息协议
     */
    @Column(name = "protocol", nullable = false)
    @ColumnDefault(value = "''")
    protected String protocol = "";


    /**
     * 消息等级
     */
    @Column(name = "msglevel", nullable = false)
    @ColumnDefault(value = "-1")
    protected Integer msgLevel = -1;


    /**
     * 协议描述
     */
    @Column(name = "protodesc", nullable = false)
    @ColumnDefault(value = "''")
    protected String protodesc = "";

    /**
     * token
     */
    @Column(name = "token", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    protected String token = "";

    /**
     * 应用
     */
    @Column(name = "application", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    protected String application = "";


    /**
     * channel
     */
    @Column(name = "channelid", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    protected String channelId = "";

    /**
     * 报事ip
     */
    @Column(name = "ip", nullable = false)
    @ColumnDefault(value = "''")
    protected String ip = "";

    /**
     * 端口
     */
    @Column(name = "port", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer port = 0;


    /**
     * 插件Id
     */
    @Column(name = "pluginid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer pluginId = 0;

    /**
     * 消息分类
     */
    @Column(name = "type", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer type = 0;


    /**
     * 消息状态
     */
    @Column(name = "state", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer state = 0;

    /**
     * 消息说明
     */
    @Column(name = "message", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    protected String message = "";

    /**
     * 有效数据
     */
    @Column(name = "data", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    protected String data = "";

    /**
     * 消息内容
     */
    @Column(name = "content", nullable = false, length = 2000)
    @ColumnDefault(value = "''")
    protected String content = "";


    /**
     * 缩略图
     */
    @Column(name = "litpic", nullable = false, length = 280)
    @ColumnDefault(value = "''")
    protected String litpic = "";


    /**
     * 设备名称
     */
    @Column(name = "devicename", nullable = false)
    @ColumnDefault(value = "''")
    protected String deviceName = "";


    /**
     * 设备编码
     */
    @Column(name = "devicecode", nullable = false)
    @ColumnDefault(value = "''")
    protected String deviceCode = "";


    /**
     * 来自于设备的topic
     */
    @Column(name = "topic", nullable = false)
    @ColumnDefault(value = "''")
    protected String topic = "";

    /**
     * 时间戳
     */
    @Column(name = "timestamp", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer timestamp = UnixTimeHelper.getUnixTimeStamp();

    /**
     * 失效消息
     */
    @Column(name = "lose", nullable = false)
    @ColumnDefault(value = "0")
    protected Integer lose = 0;


    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }

    public String getDeviceSN() {
        return deviceSN;
    }

    public void setDeviceSN(String deviceSN) {
        this.deviceSN = deviceSN;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getMsgLevel() {
        return msgLevel;
    }

    public void setMsgLevel(Integer msgLevel) {
        this.msgLevel = msgLevel;
    }

    public String getProtodesc() {
        return protodesc;
    }

    public void setProtodesc(String protodesc) {
        this.protodesc = protodesc;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getPluginId() {
        return pluginId;
    }

    public void setPluginId(Integer pluginId) {
        this.pluginId = pluginId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLitpic() {
        return litpic;
    }

    public void setLitpic(String litpic) {
        this.litpic = litpic;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getLose() {
        return lose;
    }

    public void setLose(Integer lose) {
        this.lose = lose;
    }

    public SupperMessageInfo() {
    }

    public SupperMessageInfo(String uid, Integer agentId, String unitCode, String deviceSN, Integer productId, Integer productType, String protocol, Integer msgLevel, String protodesc, String token, String channelId, String ip, Integer port, Integer pluginId, Integer type, Integer state, String message, String content, String deviceName, String deviceCode, String topic, Integer timestamp, Integer lose) {
        this.uid = uid;
        this.agentId = agentId;
        this.unitCode = unitCode;
        this.deviceSN = deviceSN;
        this.productId = productId;
        this.productType = productType;
        this.protocol = protocol;
        this.msgLevel = msgLevel;
        this.protodesc = protodesc;
        this.token = token;
        this.channelId = channelId;
        this.ip = ip;
        this.port = port;
        this.pluginId = pluginId;
        this.type = type;
        this.state = state;
        this.message = message;
        this.content = content;
        this.deviceName = deviceName;
        this.deviceCode = deviceCode;
        this.topic = topic;
        this.timestamp = timestamp;
        this.lose = lose;
    }

    @Override
    public String toString() {
        return "SupperMessageInfo{" +
                "msgId=" + msgId +
                ", uid='" + uid + '\'' +
                ", agentId=" + agentId +
                ", unitCode='" + unitCode + '\'' +
                ", deviceSN='" + deviceSN + '\'' +
                ", productId=" + productId +
                ", productType=" + productType +
                ", protocol='" + protocol + '\'' +
                ", msgLevel=" + msgLevel +
                ", protodesc='" + protodesc + '\'' +
                ", token='" + token + '\'' +
                ", channelId='" + channelId + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", pluginId=" + pluginId +
                ", type=" + type +
                ", state=" + state +
                ", message='" + message + '\'' +
                ", content='" + content + '\'' +
                ", litpic='" + litpic + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", deviceCode='" + deviceCode + '\'' +
                ", topic='" + topic + '\'' +
                ", timestamp=" + timestamp +
                ", lose=" + lose +
                '}';
    }


}
