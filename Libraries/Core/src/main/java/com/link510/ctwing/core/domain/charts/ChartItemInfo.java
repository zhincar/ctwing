package com.link510.ctwing.core.domain.charts;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

//chartitem
@Entity
@Table(name = "w_chart_item")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@DynamicUpdate
public class ChartItemInfo implements Serializable {


    private static final long serialVersionUID = 3109858909192814499L;
    /**
     * ID
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "itemid", nullable = false)
    private int itemId = 0;

    /**
     * 图表ID
     **/
    @Column(name = "chartid", nullable = false)
    @ColumnDefault(value = "0")
    private int chartId = 0;

    /**
     * 协议ID
     **/
    @Column(name = "protocolid", nullable = false)
    @ColumnDefault(value = "0")
    private int protocolId = 0;

    /**
     * 协议key2值
     **/
    @Column(name = "key2", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    private String key2 = "";

    /**
     * 参数ID
     **/
    @Column(name = "paramid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer paramId = 0;


}