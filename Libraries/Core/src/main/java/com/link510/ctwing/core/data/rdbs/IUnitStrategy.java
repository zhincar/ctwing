/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.unit.UnitInfo;
import com.link510.ctwing.core.domain.unit.UnitSubscribeInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

/**
 * Created by cqnews on 2017/12/6.
 */
@SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
public interface IUnitStrategy {


    //region 单位

    /**
     * 获得单位数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getUnitCount(Specification<UnitInfo> condition) throws IOException;


    /**
     * 创建一条单位数据
     *
     * @param unitInfo 单位模型
     * @return 返回创建信息
     **/
    UnitInfo createUnit(UnitInfo unitInfo) throws IOException;


    /**
     * 更新一条单位数据
     *
     * @param unitInfo 单位模型
     **/
    UnitInfo updateUnit(UnitInfo unitInfo) throws IOException;

    /**
     * 删除一条单位数据
     *
     * @param unitId 单位模型
     **/
    void deleteUnitByUnitId(int unitId) throws IOException;

    /**
     * 批量删除一批单位数据
     **/
    void deleteUnitByUnitIdList(String unitIdList) throws IOException;


    /**
     * 获得单位一条记录
     *
     * @param unitId unitid
     * @return 返回一条UnitInfo
     **/
    UnitInfo getUnitByUnitId(int unitId) throws IOException;

    /**
     * 通过编号获取单位
     *
     * @param unitCode 单位编号
     * @return
     */
    UnitInfo getUnitByUnitCode(String unitCode) throws IOException;


    /**
     * 通过密钥获取单位信息
     *
     * @param apiKey    key
     * @param apiSecret 密钥
     */
    UnitInfo getUnitByApiKeyAndApiSecret(String apiKey, String apiSecret) throws IOException;

    /**
     * 获得单位数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UnitInfo
     **/
    List<UnitInfo> getUnitList(Specification<UnitInfo> condition, Sort sort) throws IOException;


    /**
     * 获得单位数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UnitInfo
     **/
    Page<UnitInfo> getUnitList(Integer pageSize, Integer pageNumber, Specification<UnitInfo> condition, Sort sort) throws IOException;


    //endregion 单位结束

    //region 单位订阅

    /**
     * 获得单位订阅数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getUnitSubscribeCount(Specification<UnitSubscribeInfo> condition) throws IOException;


    /**
     * 创建一条单位订阅数据
     *
     * @param unitsubscriptionInfo 单位订阅模型
     * @return 返回创建信息
     **/
    UnitSubscribeInfo createUnitSubscribe(UnitSubscribeInfo unitsubscriptionInfo) throws IOException;


    /**
     * 更新一条单位订阅数据
     *
     * @param unitsubscriptionInfo 单位订阅模型
     **/
    UnitSubscribeInfo updateUnitSubscribe(UnitSubscribeInfo unitsubscriptionInfo) throws IOException;

    /**
     * 删除一条单位订阅数据
     *
     * @param subId 单位订阅模型
     **/
    void deleteUnitSubscribeBySubId(int subId) throws IOException;

    /**
     * 批量删除一批单位订阅数据
     **/
    void deleteUnitSubscribeBySubIdList(String subIdList) throws IOException;


    /**
     * 获得单位订阅一条记录
     *
     * @param subId subid
     * @return 返回一条UnitSubscriptionInfo
     **/
    UnitSubscribeInfo getUnitSubscribeBySubId(int subId) throws IOException;

    /**
     * 获得单位订阅数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回UnitSubscriptionInfo
     **/
    List<UnitSubscribeInfo> getUnitSubscribeList(Specification<UnitSubscribeInfo> condition, Sort sort) throws IOException;


    /**
     * 获得单位订阅数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回UnitSubscriptionInfo
     **/
    Page<UnitSubscribeInfo> getUnitSubscribeList(Integer pageSize, Integer pageNumber, Specification<UnitSubscribeInfo> condition, Sort sort) throws IOException;

    /**
     * 通过产品Id和产品协议读取订阅消息
     *
     * @param unitCode  单位编码
     * @param productId 产品Id
     * @param protocol  协议
     * @return
     */
    UnitSubscribeInfo getUnitSubscribeByProductIdAndProtocol(String unitCode, Integer productId, String protocol) throws IOException;
    //endregion 单位订阅结束

}
