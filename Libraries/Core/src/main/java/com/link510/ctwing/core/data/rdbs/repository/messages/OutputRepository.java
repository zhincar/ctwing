
package com.link510.ctwing.core.data.rdbs.repository.messages;

import com.link510.ctwing.core.domain.message.OutputInfo;
import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.message.OutputInfo;

public interface OutputRepository extends BaseRepository<OutputInfo, Integer> {
}