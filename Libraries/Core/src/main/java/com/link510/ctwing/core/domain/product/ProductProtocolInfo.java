package com.link510.ctwing.core.domain.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * 产品协议
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_product_protocol")
public class ProductProtocolInfo implements Serializable {

    private static final long serialVersionUID = -1374738264789416865L;
    /**
     * 产品的id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "protocolid", nullable = false)
    private Integer protocolId = -1;

    /**
     * 是否默认协议
     */
    @Column(name = "isdefault", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isDefault = 0;

    /**
     * 协议名称
     */
    @Column(name = "name", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String name = "";

    /**
     * 协议唯一标识
     */
    @Column(name = "protocol", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String protocol = "";

    /**
     * 协议标识
     */
    @Column(name = "features", nullable = false, length = 512)
    @ColumnDefault(value = "''")
    private String features = "";


    /**
     * 协议所属产品
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer productId = -1;

    /**
     * 消息等级
     */
    @Column(name = "msglevel", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer msgLevel = -1;


    /**
     * 协议所属产品
     */
    @Column(name = "productname", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String productName = "";


    /**
     * 默认输出地址
     */
    @Column(name = "defaulthttpurl", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String defaultHttpUrl = "";


    /**
     * 默认输出地址
     */
    @Column(name = "testhttpurl", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String testHttpUrl = "";

    /**
     * 协议描述
     */
    @Column(name = "description", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String description = "";

    /**
     * 协议输出模板
     */
    @Column(name = "outtemplet", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String outTemplet = "";

    /**
     * 是否补充协议
     */
    @Column(name = "isreplenish", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isReplenish = 0;

    /**
     * 补充协议列表
     */
    @Column(name = "replenishlist", nullable = false)
    @ColumnDefault(value = "''")
    private String replenishList = "";


    public ProductProtocolInfo(String protocol, String name, String features, String description, Integer isDefault) {
        this.protocol = protocol;
        this.name = name;
        this.features = features;
        this.description = description;
        this.isDefault = isDefault;
    }


    public Map<String, String> toSecurityMap() {

        Map<String, String> map2 = new HashMap<>();

        try {

            map2.put("protocol", protocol);
            map2.put("name", name);
            map2.put("description", description);
            map2.put("msgLevel", msgLevel.toString());

        } catch (Exception ignored) {

        }

        return map2;
    }
}
