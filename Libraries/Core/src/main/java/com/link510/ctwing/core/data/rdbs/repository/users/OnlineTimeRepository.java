package com.link510.ctwing.core.data.rdbs.repository.users;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.users.OnlineTimeInfo;

public interface OnlineTimeRepository extends BaseRepository<OnlineTimeInfo, Integer> {
}
