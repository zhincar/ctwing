package com.link510.ctwing.core.domain.device;


import com.link510.ctwing.core.helper.TypeHelper;
import com.link510.ctwing.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * 设备探测器
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_devices")
public class DeviceInfo implements Serializable {

    private static final long serialVersionUID = -3762452645813988665L;

    private static Logger logger = LoggerFactory.getLogger(DeviceInfo.class);

    /**
     * 探测器id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "deviceid")
    private Integer deviceId = -1;

    /**
     * 设备的串号
     */
    @Column(name = "imei", nullable = false, length = 64)
    @ColumnDefault(value = "''")
    private String imei = "";


    /**
     * 第三方设备的注册串号
     */
    @Column(name = "otherdeviceid", nullable = false, length = 128)
    @ColumnDefault(value = "''")
    private String otherDeviceId = "";

    /**
     * 探测器编号
     */
    @Column(name = "devicesn", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String deviceSN = "";

    /**
     * 模块编号
     */
    @Column(name = "moduleid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer moduleId = 0;

    /**
     * 产品d
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer productId = -1;

    /**
     * 产品d
     */
    @Column(name = "productsn", nullable = false)
    @ColumnDefault(value = "''")
    private String productSN = "";

    /**
     * 产品名称
     */
    @Column(name = "productname", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String productName = "";

    /**
     * 产品类型
     */
    @Column(name = "producttype", nullable = false)
    @ColumnDefault(value = "0")
    private Integer productType = 0;

    /**
     * 单位编码
     */
    @Column(name = "unitcode", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String unitCode = "";

    /**
     * uid
     */
    @Column(name = "uid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String uid = "";

    /**
     * uid昵称
     */
    @Column(name = "nickname", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String nickName = "";

    /**
     * 代码商Id
     */
    @Column(name = "agentid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer agentId = 0;

    /**
     * 代码商名称
     */
    @Column(name = "agentname", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String agentName = "";

    /**
     * 探测器名称
     */
    @Column(name = "name", nullable = false, length = 30)
    @ColumnDefault(value = "''")
    private String name = "";

    /**
     * SIM
     */
    @Column(name = "sim", nullable = false, length = 20)
    @ColumnDefault(value = "''")
    protected String sim = "";

    /**
     * IMSI
     */
    @Column(name = "imsi", nullable = false, length = 32)
    @ColumnDefault(value = "''")
    protected String imsi = "";

    /**
     * 设备缩略图
     */
    @Column(name = "litpic", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String litpic = "";

    /**
     * 所属网关设备
     */
    @Column(name = "token", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String token = "";


    /**
     * 设备版本
     */
    @Column(name = "version", nullable = false)
    @ColumnDefault(value = "''")
    private String version = "";

    /**
     * 设备描述
     */
    @Column(name = "description", nullable = false)
    @ColumnDefault(value = "''")
    private String description = "";

    /**
     * 经度
     */
    @Column(name = "longitude", nullable = false)
    @ColumnDefault(value = "0.00")
    private Double longitude = 0.00;

    /**
     * 纬度
     */
    @Column(name = "latitude", nullable = false)
    @ColumnDefault(value = "0.00")
    private Double latitude = 0.00;

    /**
     * 设备所在地域
     */
    @Column(name = "regionid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer regionId = 0;


    /**
     * 设备安装地址
     */
    @Column(name = "address", nullable = false, length = 150)
    @ColumnDefault(value = "''")
    private String address = "";


    /**
     * 探测器添加时间
     */
    @Column(name = "addtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer addTime = 0;


    /**
     * 探测器修改时间
     */
    @Column(name = "updatetime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer updateTime = 0;

    /**
     * 设备当前模式
     */
    @Column(name = "devicemodel", nullable = false)
    @ColumnDefault(value = "0")
    private Integer deviceModel = 0;

    /**
     * 危险等级
     */
    @Column(name = "warninglevel", nullable = false)
    @ColumnDefault(value = "0")
    private Integer warningLevel = 0;

    /**
     * 危险等级代码
     */
    @Column(name = "warningcode", nullable = false)
    @ColumnDefault(value = "''")
    private String warningCode = "";

    /**
     * 危险等级预警时间
     */
    @Column(name = "warningtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer warningTime = 0;

    /**
     * 设备密码
     */
    @Column(name = "password", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String password = "";

    /**
     * 设备密码
     */
    @Column(name = "salt", nullable = false, length = 12)
    @ColumnDefault(value = "'NT3BxI'")
    private String salt = "";

    /**
     * 是否删除
     */
    @Column(name = "isdelete", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isDelete = 0;

    public DeviceInfo(Map<String, Object> params) {
        this.deviceId = TypeHelper.objectToInt(params.get("deviceid"));
        this.imei = TypeHelper.objectToString(params.get("imei"));
        this.productId = (int) params.get("productid");
        this.productName = TypeHelper.objectToString(params.get("productname"));
        this.name = TypeHelper.objectToString(params.get("name"));
        this.litpic = TypeHelper.objectToString(params.get("litpic"));
        this.token = TypeHelper.objectToString(params.get("token"));
        this.version = TypeHelper.objectToString(params.get("version"));
        this.description = TypeHelper.objectToString(params.get("description"));
        this.addTime = TypeHelper.objectToInt(params.get("addtime"));
        this.updateTime = TypeHelper.objectToInt(params.get("updatetime"));
        this.deviceModel = TypeHelper.objectToInt(params.get("devicemodel"));

    }

    /**
     * @param params 参数
     */
    public static DeviceInfo of(Map params) {

        DeviceInfo deviceInfo = null;

        try {
            deviceInfo = new DeviceInfo();

            deviceInfo.deviceSN = TypeHelper.objectToString(params.get("det_num"));
            deviceInfo.address = TypeHelper.objectToString(params.get("address"));
            deviceInfo.unitCode = TypeHelper.objectToString(params.get("unit_code"));
            deviceInfo.longitude = TypeHelper.objectToDouble(params.get("lng"));
            deviceInfo.latitude = TypeHelper.objectToDouble(params.get("lat"));
            deviceInfo.description = TypeHelper.objectToString(params.get("fire_area"));
            deviceInfo.deviceModel = TypeHelper.objectToInt(params.get("mode"), 0);
            deviceInfo.productId = TypeHelper.objectToInt(params.get("productId"), 1);
            deviceInfo.productName = TypeHelper.objectToString(params.get("productName"));

            deviceInfo.name = TypeHelper.objectToString(params.get("name"));
            deviceInfo.description = TypeHelper.objectToString(params.get("description"));
            deviceInfo.regionId = TypeHelper.objectToInt(params.get("regionId"));

            deviceInfo.addTime = UnixTimeHelper.getUnixTimeStamp();
            deviceInfo.updateTime = deviceInfo.addTime;


        } catch (Exception ex) {
            logger.error("数据解析失败," + ex.getMessage());
        }
        return deviceInfo;
    }


    /**
     * 设备转产品,去掉敏感信息
     */
    public Map<String, Object> toSecurityMap() {

        Map<String, Object> map2 = new HashMap<>();

        try {

            map2.put("name", name);
            map2.put("deviceSN", deviceSN);
            map2.put("productId", productId);
            map2.put("productType", productType);
            map2.put("unitCode", unitCode);
            map2.put("address", address);
            map2.put("latitude", latitude);
            map2.put("longitude", longitude);
            map2.put("description", description);

        } catch (Exception ignored) {

        }

        return map2;
    }

}
