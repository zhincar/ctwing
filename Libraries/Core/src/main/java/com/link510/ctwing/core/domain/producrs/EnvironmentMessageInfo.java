package com.link510.ctwing.core.domain.producrs;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 环境探测器数据
 *
 * @author cqnews
 */
@Data
@Builder
public class EnvironmentMessageInfo implements Serializable {

    private static final long serialVersionUID = 3192882104209004585L;

    /**
     * 温度
     */
    @Builder.Default
    private String temperature = "";

    /**
     * 湿度
     */
    @Builder.Default
    private String humidity = "";

    /**
     * 光照强度
     */
    @Builder.Default
    private String light = "";


    /**
     * UV强度
     */
    @Builder.Default
    private String uvPower = "";


    /**
     * UV系数
     */
    @Builder.Default
    private String uv = "";

    /**
     * 风速
     */
    @Builder.Default
    private String windSpeed = "";

    /**
     * 风向
     */
    @Builder.Default
    private String windDirection = "";

    /**
     * 阵风
     */
    @Builder.Default
    private String windPower = "";

    /**
     * 雨量
     */
    @Builder.Default
    private String rain = "";


    /**
     * 气压
     */
    @Builder.Default
    private String pressure = "";

    /**
     * 气压湿度
     */
    @Builder.Default
    private String pressureTemperature = "";

    /**
     * 土壤温度
     */
    @Builder.Default
    private String soilTemperature = "";

    /**
     * 土壤湿度
     */
    @Builder.Default
    private String soilHumidity = "";

    /**
     * 电池电压
     */
    @Builder.Default
    private String batteryVoltage = "";

    /**
     * 露点
     */
    @Builder.Default
    private String dewPoint = "";

    /**
     * 电容电压
     */
    @Builder.Default
    private String capacitanceVoltage = "";


}
