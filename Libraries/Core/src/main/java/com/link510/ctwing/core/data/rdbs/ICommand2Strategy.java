/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.command.CommandInfo;
import com.link510.ctwing.core.domain.command.CommandParamInfo;
import com.link510.ctwing.core.domain.command.CommandRecordInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;


public interface ICommand2Strategy {

    //region 命令

    /**
     * 获得命令数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getCommandCount(Specification<CommandInfo> condition) throws IOException;


    /**
     * 创建一条命令数据
     *
     * @param commandInfo 命令模型
     * @return 返回创建信息
     **/
    CommandInfo createCommand(CommandInfo commandInfo) throws IOException;


    /**
     * 更新一条命令数据
     *
     * @param commandInfo 命令模型
     **/
    CommandInfo updateCommand(CommandInfo commandInfo) throws IOException;

    /**
     * 删除一条命令数据
     *
     * @param commandId 命令模型
     **/
    void deleteCommandByCommandId(int commandId) throws IOException;

    /**
     * 批量删除一批命令数据
     **/
    void deleteCommandByCommandIdList(String commandIdList) throws IOException;


    /**
     * 获得命令一条记录
     *
     * @param commandId commandid
     * @return 返回一条CommandInfo
     **/
    CommandInfo getCommandByCommandId(int commandId) throws IOException;

    /**
     * 获得命令数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回CommandInfo
     **/
    List<CommandInfo> getCommandList(Specification<CommandInfo> condition, Sort sort) throws IOException;


    /**
     * 获得命令数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回CommandInfo
     **/
    Page<CommandInfo> getCommandList(Integer pageSize, Integer pageNumber, Specification<CommandInfo> condition, Sort sort) throws IOException;


    //endregion 命令结束

    //region 命令参数

    /**
     * 获得命令参数数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getCommandParamCount(Specification<CommandParamInfo> condition) throws IOException;


    /**
     * 创建一条命令参数数据
     *
     * @param commandparamInfo 命令参数模型
     * @return 返回创建信息
     **/
    CommandParamInfo createCommandParam(CommandParamInfo commandparamInfo) throws IOException;


    /**
     * 更新一条命令参数数据
     *
     * @param commandparamInfo 命令参数模型
     **/
    CommandParamInfo updateCommandParam(CommandParamInfo commandparamInfo) throws IOException;

    /**
     * 删除一条命令参数数据
     *
     * @param paramId 命令参数模型
     **/
    void deleteCommandParamByParamId(int paramId) throws IOException;

    /**
     * 批量删除一批命令参数数据
     **/
    void deleteCommandParamByParamIdList(String paramIdList) throws IOException;


    /**
     * 获得命令参数一条记录
     *
     * @param paramId paramid
     * @return 返回一条CommandParamInfo
     **/
    CommandParamInfo getCommandParamByParamId(int paramId) throws IOException;

    /**
     * 获得命令参数数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回CommandParamInfo
     **/
    List<CommandParamInfo> getCommandParamList(Specification<CommandParamInfo> condition, Sort sort) throws IOException;


    /**
     * 获得命令参数数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回CommandParamInfo
     **/
    Page<CommandParamInfo> getCommandParamList(Integer pageSize, Integer pageNumber, Specification<CommandParamInfo> condition, Sort sort) throws IOException;


    //endregion 命令参数结束

    //region 命令记录

    /**
     * 获得命令记录数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getCommandRecordCount(Specification<CommandRecordInfo> condition) throws IOException;


    /**
     * 创建一条命令记录数据
     *
     * @param commandrecordInfo 命令记录模型
     * @return 返回创建信息
     **/
    CommandRecordInfo createCommandRecord(CommandRecordInfo commandrecordInfo) throws IOException;


    /**
     * 更新一条命令记录数据
     *
     * @param commandrecordInfo 命令记录模型
     **/
    CommandRecordInfo updateCommandRecord(CommandRecordInfo commandrecordInfo) throws IOException;

    /**
     * 删除一条命令记录数据
     *
     * @param reocrdId 命令记录模型
     **/
    void deleteCommandRecordByReocrdId(int reocrdId) throws IOException;

    /**
     * 批量删除一批命令记录数据
     **/
    void deleteCommandRecordByReocrdIdList(String reocrdIdList) throws IOException;


    /**
     * 获得命令记录一条记录
     *
     * @param reocrdId reocrdid
     * @return 返回一条CommandRecordInfo
     **/
    CommandRecordInfo getCommandRecordByReocrdId(int reocrdId) throws IOException;

    /**
     * 获得命令记录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回CommandRecordInfo
     **/
    List<CommandRecordInfo> getCommandRecordList(Specification<CommandRecordInfo> condition, Sort sort) throws IOException;


    /**
     * 获得命令记录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回CommandRecordInfo
     **/
    Page<CommandRecordInfo> getCommandRecordList(Integer pageSize, Integer pageNumber, Specification<CommandRecordInfo> condition, Sort sort) throws IOException;

    /**
     * 删除命令所有参数
     * @param commandId
     * @throws IOException
     */
    void deleteCommandParamByCommandId(Integer commandId)throws IOException;

    /**
     * 查询命令所有参数
     * @param commandId
     * @throws IOException
     */
    List<CommandParamInfo> getCommandParamByCommandId(Integer commandId)throws IOException;


    //endregion 命令记录结束
    
}


