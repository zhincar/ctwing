package com.link510.ctwing.core.domain.lives;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 直播管理人员
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DynamicUpdate
@Table(name = "w_live_users")
public class LiveUserInfo implements Serializable {

    private static final long serialVersionUID = 3139749285148337273L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id = 0;

    /**
     * 直播Id
     */
    @Column(name = "liveid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer liveId = 0;

    @Column(name = "channelid", nullable = false, length = 80)
    @ColumnDefault(value = "''")
    private String channelId = "";

    /**
     * uid
     */
    @Column(name = "uid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String uid = "";


    /**
     * uid
     */
    @Column(name = "token", nullable = false, length = 350)
    @ColumnDefault(value = "''")
    private String token = "";


    /**
     * openId
     */
    @Column(name = "openid", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String openId = "";


    /**
     * uid
     */
    @Column(name = "nickname", nullable = false, length = 50)
    @ColumnDefault(value = "''")
    private String nickName = "";

    /**
     * 是否具备管理级别
     */
    @Column(name = "isadmin", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isAdmin = 0;


    /**
     * 是否在线
     */
    @Column(name = "isonline", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isOnline = 0;

    /**
     * 更新时间
     */
    @Column(name = "updateTime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer updateTime = 0;

    public void setToken(String token) {

        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
