package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.product.ProductInfo;
import com.link510.ctwing.core.domain.product.ProductProtocolInfo;
import com.link510.ctwing.core.domain.product.ProductProtocolParamInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;


@SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
public interface IProductStrategy {

    //region 产品库

    /**
     * 获得产品库数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getProductCount(Specification<ProductInfo> condition) throws IOException;


    /**
     * 创建一条产品库数据
     *
     * @param productInfo 产品库模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductInfo createProduct(ProductInfo productInfo) throws IOException;


    /**
     * 更新一条产品库数据
     *
     * @param productInfo 产品库模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductInfo updateProduct(ProductInfo productInfo) throws IOException;

    /**
     * 删除一条产品库数据
     *
     * @param productId 产品库模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteProductByProductId(int productId) throws IOException;

    /**
     * 批量删除一批产品库数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteProductByProductIdList(String productIdList) throws IOException;



    /**
     * 获得产品库一条记录
     *
     * @param productId productid
     * @return 返回一条ProductInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductInfo getProductByProductId(int productId) throws IOException;

    /**
     * 获得产品库数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<ProductInfo> getProductInfoList(Specification<ProductInfo> condition, Sort sort) throws IOException;


    /**
     * 获得产品库数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<ProductInfo> getProductInfoList(Integer pageSize, Integer pageNumber, Specification<ProductInfo> condition, Sort sort) throws IOException;

    /**
     * 获取产品的协议列表
     *
     * @param productId 产品id
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<ProductProtocolInfo> getProductProtocolList(Integer productId) throws IOException;

    /**
     * 获取所有的产器列表
     *
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<ProductInfo> getAllProductList(Sort sort) throws IOException;

    /**
     * 通过token获取产品信息
     *
     * @param secretkey token
     * @return
     */
    ProductInfo getProductBySecretkey(String secretkey) throws IOException;

    //endregion 产品库结束

    //region 产品协议

    /**
     * 获得产品协议数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getProductProtocolCount(Specification<ProductProtocolInfo> condition) throws IOException;


    /**
     * 创建一条产品协议数据
     *
     * @param productprotoclInfo 产品协议模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductProtocolInfo createProductProtocol(ProductProtocolInfo productprotoclInfo) throws IOException;


    /**
     * 更新一条产品协议数据
     *
     * @param productprotoclInfo 产品协议模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductProtocolInfo updateProductProtocol(ProductProtocolInfo productprotoclInfo) throws IOException;

    /**
     * 删除一条产品协议数据
     *
     * @param protoclId 产品协议模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteProductProtocolByProtoclId(int protoclId) throws IOException;

    /**
     * 批量删除一批产品协议数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteProductProtocolByProtoclIdList(String protoclIdList) throws IOException;


    /**
     * 删除产品协议和参数
     *
     * @param productId 产品Id
     * @return boolean
     */
    boolean deleteProductProtocolByProductId(Integer productId) throws IOException;

    /**
     * 获得产品协议一条记录
     *
     * @param protoclId protoclid
     * @return 返回一条ProductProtocolInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductProtocolInfo getProductProtocolByProtoclId(int protoclId) throws IOException;

    /**
     * 获得产品协议数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductProtocolInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<ProductProtocolInfo> getProductProtocolList(Specification<ProductProtocolInfo> condition, Sort sort) throws IOException;


    /**
     * 获得产品协议数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductProtocolInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<ProductProtocolInfo> getProductProtocolList(Integer pageSize, Integer pageNumber, Specification<ProductProtocolInfo> condition, Sort sort) throws IOException;


    //endregion 产品协议结束

    //region 协议参数

    /**
     * 获得协议参数数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getProductProtocolParamCount(Specification<ProductProtocolParamInfo> condition) throws IOException;


    /**
     * 创建一条协议参数数据
     *
     * @param productprotoclparamInfo 协议参数模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductProtocolParamInfo createProductProtocolParam(ProductProtocolParamInfo productprotoclparamInfo) throws IOException;


    /**
     * 更新一条协议参数数据
     *
     * @param productprotoclparamInfo 协议参数模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductProtocolParamInfo updateProductProtocolParam(ProductProtocolParamInfo productprotoclparamInfo) throws IOException;

    /**
     * 删除一条协议参数数据
     *
     * @param paramId 协议参数模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteProductProtocolParamByParamId(int paramId) throws IOException;

    /**
     * 批量删除一批协议参数数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteProductProtocolParamByParamIdList(String paramIdList) throws IOException;


    /**
     * 获得协议参数一条记录
     *
     * @param paramId paramid
     * @return 返回一条ProductProtocolParamInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    ProductProtocolParamInfo getProductProtocolParamByParamId(int paramId) throws IOException;

    /**
     * 获得协议参数数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ProductProtocolParamInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<ProductProtocolParamInfo> getProductProtocolParamList(Specification<ProductProtocolParamInfo> condition, Sort sort) throws IOException;


    /**
     * 获得协议参数数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ProductProtocolParamInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<ProductProtocolParamInfo> getProductProtocolParamList(Integer pageSize, Integer pageNumber, Specification<ProductProtocolParamInfo> condition, Sort sort) throws IOException;


    /**
     * 通读产品协议参数列表
     *
     * @param protocolId 协议id
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<ProductProtocolParamInfo> getProductProtocolParamList(Integer protocolId) throws IOException;

    /**
     * 通读产品协议参数列表
     *
     * @param productId 产品Id
     * @return
     */
    List<ProductProtocolParamInfo> getProductProtocolParamListByProductId(Integer productId) throws IOException;

    List<ProductInfo> findSimpleModel();


    /**
     * 获取产品id的协议
     *
     * @param productId 产品id
     * @param protocol  协议
     * @return
     */
    ProductProtocolInfo getProductProtocolByProductIdAndProtocol(Integer productId, String protocol) throws IOException;

    /**
     * 获取产品的默认协议
     *
     * @param productId 产品id
     * @return
     */
    ProductProtocolInfo getDefaultProductProtocolByProductId(Integer productId) throws IOException;

    /**
     * 获取参数的协议
     *
     * @param productId 产品Id
     * @param protocol  协议
     */
    @Deprecated
    ProductProtocolInfo getProductProtocolByProtocl(Integer productId, String protocol) throws IOException;

    /**
     * 查找产品信息
     *
     * @param productSN
     * @return
     * @throws IOException
     */
    ProductInfo getProductByProductSN(String productSN) throws IOException;

    /**
     * 查询协议参数
     *
     * @param protocolId
     * @return
     * @throws IOException
     */
    List<ProductProtocolParamInfo> getProductProtocolParamByProtocolId(Integer protocolId) throws IOException;

    /**
     * 获取产品所有协议
     *
     * @param productId
     * @return
     * @throws IOException
     */
    List<ProductProtocolInfo> getAllProtocolByProtoclId(Integer productId) throws IOException;

    //endregion 协议参数结束

}


