/*
 *  *  Copyright  2019.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2019.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.core.enums.message;

import com.link510.ctwing.core.helper.TypeHelper;
import com.link510.ctwing.core.model.SelectListItem;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
public enum DataType {

    /**
     * 解析文本类型
     */
    Text(0, "Text", "文本型"),
    Integer(1, "Integer", "整形"),
    Double(2, "Double", "浮点型"),
    Image(3, "Image", "图片"),
    EChart(4, "EChart", "数据图表");


    /**
     * 数据类型
     */
    private Integer type;

    /**
     * 英文键值
     */
    private String code;


    /**
     * 名称
     */
    private String name;

    public static String getName(Integer type) {

        for (DataType d : values()) {
            if (type.equals(d.getType())) {
                return d.getName();
            }
        }
        return Text.getName();
    }


    public static DataType getDateType(Integer type) {

        for (DataType d : values()) {
            if (type.equals(d.getType())) {
                return d;
            }
        }
        return Text;
    }


    /**
     * 获取枚举的列表
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem() {
        return getSelectListItem(Text.getType());
    }


    /**
     * 获取枚举的列表
     * index 默认选择项目
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem(Integer type) {

        List<SelectListItem> selectListItemList = new ArrayList<>();

        for (DataType data : values()) {

            SelectListItem item = new SelectListItem();

            item.setText(data.getName());
            item.setValue(TypeHelper.intToString(data.getType()));


            if (data.getType().equals(type)) {
                item.setSelected(true);
            }

            selectListItemList.add(item);
        }

        return selectListItemList;
    }


}
