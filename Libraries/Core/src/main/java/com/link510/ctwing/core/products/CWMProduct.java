package com.link510.ctwing.core.products;

import com.link510.ctwing.core.context.SpringContext;

import com.link510.ctwing.core.enums.product.ProductType;
import com.link510.ctwing.core.iniots.IIniotStrategy;
import com.link510.ctwing.core.log.Logs;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Objects;

@Component(value = "CWMProduct")
@Getter
public class CWMProduct {


    @Autowired
    private SpringContext springConext;


    @Autowired
    private Logs logs;


    /**
     * 定时器列表
     */
    private Map<String, IProduct> productList;

    /**
     * 初始化插件
     */
    @PostConstruct
    public void init() {
        this.productList = SpringContext.getApplicationContext().getBeansOfType(IProduct.class);
    }


    /**
     * 所有插件列表
     *
     * @return Map<String, IProduct>
     */
    public Map<String, IProduct> getProductList() {

        if (productList == null) {
            this.productList = SpringContext.getApplicationContext().getBeansOfType(IProduct.class);
        }

        return this.productList;
    }

    /**
     * 获取产品
     *
     * @param code 产品编号
     * @return IProduct
     */
    public IProduct getProduct(String code) {

        try {

            return getProductList().values().stream()
                    .filter(Objects::nonNull)
                    .filter(x -> x.code().equals(code))
                    .findFirst().orElse(null);

        } catch (Exception ex) {
            logs.write(ex, "获取产品");
        }

        return null;
    }

    /**
     * 获取环境探测器设备
     *
     * @param code code
     * @return
     */
    public IIniotStrategy getIEnvironmentProduct(String code) {

        try {

            return (IIniotStrategy) getProductList().values().stream()
                    .filter(Objects::nonNull)
                    .filter(x -> x.typeId().equals(ProductType.Environment.getTypeId()))
                    .filter(x -> x.code().equals(code))
                    .filter(x -> x instanceof IIniotStrategy)
                    .findFirst().orElse(null);

        } catch (Exception ex) {
            logs.write(ex, "获取环境探测器设备");
        }

        return null;
    }

}
