package com.link510.ctwing.core.domain.device;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;


/**
 * 设备探测器
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_device_action")
public class DeviceActionInfo implements Serializable {

    private static final long serialVersionUID = 6140477668970441400L;
    /**
     * 动作
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "actionid", nullable = false)
    private Integer actionId = -1;


    /**
     * 访问的ip或域名
     */
    @Column(name = "httpurl", nullable = false)
    @ColumnDefault(value = "''")
    private String httpUrl = "";

    /**
     * 所属分组id
     */
    @Column(name = "groupid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer groupId = -1;

    /**
     * 设备id
     */
    @Column(name = "deviceid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer deviceId = -1;


    /**
     * 协议id
     */
    @Column(name = "protocolid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer protocolId = -1;

    /**
     * 所属协议
     */
    @Column(name = "protocol", nullable = false, length = 10)
    @ColumnDefault(value = "''")
    private String protocol = "";

    /**
     * 间隔时间
     */
    @Column(name = "intervaltime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer intervalTime = 0;

    /**
     * 超过次数
     */
    @Column(name = "thancount", nullable = false)
    @ColumnDefault(value = "0")
    private Integer thanCount = 0;


    /**
     * 是否启用方案
     */
    @Column(name = "isenable", nullable = false)
    @ColumnDefault(value = "0")
    private Integer isEnable = 0;


}
