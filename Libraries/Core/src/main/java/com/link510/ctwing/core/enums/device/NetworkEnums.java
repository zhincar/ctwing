/*
 *  *  Copyright  2018.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2018.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.core.enums.device;

import com.google.common.collect.Maps;
import com.link510.ctwing.core.helper.TypeHelper;
import com.link510.ctwing.core.model.SelectListItem;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public enum NetworkEnums {

    /**
     * 分类
     */
    TCP(1, "TCP"),
    TCP4G(2, "4G"),
    NBIOT(3, "NBIOT"),
    OTHER(0, "其它");

    @Getter
    private Integer id;

    @Getter
    private String name;


    public static String getName(Integer id) {

        for (NetworkEnums cate : values()) {

            if (id.equals(cate.getId())) {
                return cate.getName();
            }
        }

        return OTHER.getName();
    }

    public static NetworkEnums getNetwork(Integer id) {
        for (NetworkEnums cate : values()) {

            if (id.equals(cate.getId())) {
                return cate;
            }
        }

        return OTHER;
    }

    /**
     * 获取枚举的列表
     *
     * @return List<SelectListItem>
     */
    public static List<SelectListItem> getSelectListItem() {
        return getSelectListItem(0);
    }


    /**
     * 获取枚举的列表
     * index 默认选择项目
     *
     * @return List<SelectListItem>
     */
    public static List<SelectListItem> getSelectListItem(Integer id) {
        List<SelectListItem> selectListItemList = new ArrayList<>();

        for (NetworkEnums cate : values()) {

            SelectListItem item = new SelectListItem();

            item.setText(cate.getName());
            item.setValue(TypeHelper.intToString(cate.getId()));

            if (cate.getId().equals(id)) {
                item.setSelected(true);
            }

            selectListItemList.add(item);
        }

        return selectListItemList;
    }

    public static Map<Integer, String> findNameMap() {
        Map<Integer, String> result = Maps.newHashMap();
        Arrays.stream(NetworkEnums.values()).forEach(i -> {
            result.put(i.getId(), i.getName());
        });
        return result;
    }

}
