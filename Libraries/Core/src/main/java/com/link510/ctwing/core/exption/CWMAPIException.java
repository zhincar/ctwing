/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆英卡电子有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.link510.ctwing.core.exption;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * @author cqnews
 * @date 2017/3/20
 */
public class CWMAPIException extends CWMException implements Serializable {

    private static final long serialVersionUID = -8042053209328899711L;


    @Getter
    private HttpStatus status = HttpStatus.OK;

    public CWMAPIException() {
    }

    public CWMAPIException(String message) {
        super(message);
    }

    public CWMAPIException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public CWMAPIException(String message, Throwable cause) {
        super(message, cause);
    }

    public CWMAPIException(Throwable cause) {
        super(cause);
    }

    public CWMAPIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
