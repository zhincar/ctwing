package com.link510.ctwing.core.domain.message;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProtocolRecord {

    /**
     * 解析的名称
     */
    private String key = "";

    /**
     * 解析的值
     */
    private String value = "";


    /**
     * 解析的值
     */
    private String unit = "";

    /**
     * 协议的名称描述
     */
    private String desc = "";

    public ProtocolRecord(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public ProtocolRecord(String dataTime, String dataTime1, String desc) {

        this.key = key;
        this.value = value;
        this.desc = desc;
    }
}
