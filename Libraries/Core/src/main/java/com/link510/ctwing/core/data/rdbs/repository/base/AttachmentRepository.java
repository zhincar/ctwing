
package com.link510.ctwing.core.data.rdbs.repository.base;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.base.AttachmentInfo;

/**
 * @author cqnews
 */
public interface AttachmentRepository extends BaseRepository<AttachmentInfo, Integer> {


}