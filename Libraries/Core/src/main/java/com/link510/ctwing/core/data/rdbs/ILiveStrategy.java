package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.lives.LiveDeviceInfo;
import com.link510.ctwing.core.domain.lives.LiveInfo;
import com.link510.ctwing.core.domain.lives.LiveMessageInfo;
import com.link510.ctwing.core.domain.lives.LiveUserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;


public interface ILiveStrategy {

    //region 直播

    /**
     * 获得直播数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getLiveCount(Specification<LiveInfo> condition) throws IOException;


    /**
     * 创建一条直播数据
     *
     * @param liveInfo 直播模型
     * @return 返回创建信息
     **/
    LiveInfo createLive(LiveInfo liveInfo) throws IOException;


    /**
     * 更新一条直播数据
     *
     * @param liveInfo 直播模型
     **/
    LiveInfo updateLive(LiveInfo liveInfo) throws IOException;

    /**
     * 删除一条直播数据
     *
     * @param liveId 直播模型
     **/
    void deleteLiveByLiveId(int liveId) throws IOException;

    /**
     * 批量删除一批直播数据
     **/
    void deleteLiveByLiveIdList(String liveIdList) throws IOException;


    /**
     * 获得直播一条记录
     *
     * @param liveId liveid
     * @return 返回一条LiveInfo
     **/
    LiveInfo getLiveByLiveId(int liveId) throws IOException;

    /**
     * 获得直播数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回LiveInfo
     **/
    List<LiveInfo> getLiveList(Specification<LiveInfo> condition, Sort sort) throws IOException;


    /**
     * 获得直播数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回LiveInfo
     **/
    Page<LiveInfo> getLiveList(Integer pageSize, Integer pageNumber, Specification<LiveInfo> condition, Sort sort) throws IOException;


    //endregion 直播结束

    //region 直播设备

    /**
     * 获得直播设备数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getLiveDeviceCount(Specification<LiveDeviceInfo> condition) throws IOException;


    /**
     * 创建一条直播设备数据
     *
     * @param livedeviceInfo 直播设备模型
     * @return 返回创建信息
     **/
    LiveDeviceInfo createLiveDevice(LiveDeviceInfo livedeviceInfo) throws IOException;


    /**
     * 更新一条直播设备数据
     *
     * @param livedeviceInfo 直播设备模型
     **/
    LiveDeviceInfo updateLiveDevice(LiveDeviceInfo livedeviceInfo) throws IOException;

    /**
     * 删除一条直播设备数据
     *
     * @param id 直播设备模型
     **/
    void deleteLiveDeviceById(int id) throws IOException;

    /**
     * 批量删除一批直播设备数据
     **/
    void deleteLiveDeviceByIdList(String idList) throws IOException;


    /**
     * 获得直播设备一条记录
     *
     * @param id id
     * @return 返回一条LiveDeviceInfo
     **/
    LiveDeviceInfo getLiveDeviceById(int id) throws IOException;

    /**
     * 通过设备编号查询直播信息
     *
     * @param deviceSN 设备编号
     * @param nowTime  当前时间
     * @return
     */
    List<LiveDeviceInfo> getCurrentLiveDeviceByDeviceSN(String deviceSN, Integer nowTime) throws IOException;

    /**
     * 获得直播设备数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回LiveDeviceInfo
     **/
    List<LiveDeviceInfo> getLiveDeviceList(Specification<LiveDeviceInfo> condition, Sort sort) throws IOException;


    /**
     * 获得直播设备数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回LiveDeviceInfo
     **/
    Page<LiveDeviceInfo> getLiveDeviceList(Integer pageSize, Integer pageNumber, Specification<LiveDeviceInfo> condition, Sort sort) throws IOException;


    //endregion 直播设备结束

    //region 直播用户

    /**
     * 获得直播用户数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getLiveUserCount(Specification<LiveUserInfo> condition) throws IOException;


    /**
     * 创建一条直播用户数据
     *
     * @param liveuserInfo 直播用户模型
     * @return 返回创建信息
     **/
    LiveUserInfo createLiveUser(LiveUserInfo liveuserInfo) throws IOException;


    /**
     * 更新一条直播用户数据
     *
     * @param liveuserInfo 直播用户模型
     **/
    LiveUserInfo updateLiveUser(LiveUserInfo liveuserInfo) throws IOException;

    /**
     * 删除一条直播用户数据
     *
     * @param id 直播用户模型
     **/
    void deleteLiveUserById(int id) throws IOException;

    /**
     * 批量删除一批直播用户数据
     **/
    void deleteLiveUserByIdList(String idList) throws IOException;


    /**
     * 获得直播用户一条记录
     *
     * @param id id
     * @return 返回一条LiveUserInfo
     **/
    LiveUserInfo getLiveUserById(int id) throws IOException;

    /**
     * 判断是否管理员
     *
     * @param liveId 房间编号
     * @param uid    uid
     * @return boolean
     */
    boolean isLiveAdmin(Integer liveId, String uid) throws IOException;

    /**
     * 判断是否绑定用户
     *
     * @param liveId 房间编号
     * @param uid    uid
     * @return boolean
     */
    boolean isLiveUser(Integer liveId, String uid) throws IOException;

    /**
     * 获得直播用户数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回LiveUserInfo
     **/
    List<LiveUserInfo> getLiveUserList(Specification<LiveUserInfo> condition, Sort sort) throws IOException;


    /**
     * 获得直播用户数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回LiveUserInfo
     **/
    Page<LiveUserInfo> getLiveUserList(Integer pageSize, Integer pageNumber, Specification<LiveUserInfo> condition, Sort sort) throws IOException;


    /**
     * 获取直播间的消息列表
     *
     * @param liveId 直播Id
     * @return
     */
    List<LiveUserInfo> getLiveUserListByLiveId(Integer liveId) throws IOException;
    //endregion 直播用户结束

    //region 直播消息

    /**
     * 获得直播消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getLiveMessageCount(Specification<LiveMessageInfo> condition) throws IOException;


    /**
     * 创建一条直播消息数据
     *
     * @param livemessageInfo 直播消息模型
     * @return 返回创建信息
     **/
    LiveMessageInfo createLiveMessage(LiveMessageInfo livemessageInfo) throws IOException;


    /**
     * 更新一条直播消息数据
     *
     * @param livemessageInfo 直播消息模型
     **/
    LiveMessageInfo updateLiveMessage(LiveMessageInfo livemessageInfo) throws IOException;

    /**
     * 删除一条直播消息数据
     *
     * @param id 直播消息模型
     **/
    void deleteLiveMessageById(int id) throws IOException;

    /**
     * 批量删除一批直播消息数据
     **/
    void deleteLiveMessageByIdList(String idList) throws IOException;


    /**
     * 获得直播消息一条记录
     *
     * @param id id
     * @return 返回一条LiveMessageInfo
     **/
    LiveMessageInfo getLiveMessageById(int id) throws IOException;

    /**
     * 获得直播消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回LiveMessageInfo
     **/
    List<LiveMessageInfo> getLiveMessageList(Specification<LiveMessageInfo> condition, Sort sort) throws IOException;


    /**
     * 获得直播消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回LiveMessageInfo
     **/
    Page<LiveMessageInfo> getLiveMessageList(Integer pageSize, Integer pageNumber, Specification<LiveMessageInfo> condition, Sort sort) throws IOException;

    /**
     * 更新显示直播二维码
     *
     * @param liveId 直播id
     */
    void updateLiveCode2(Integer liveId, String codeUrl) throws IOException;

    /**
     * 查询直播设备
     *
     * @param liveId
     * @param deviceSn
     * @return
     * @throws IOException
     */
    LiveDeviceInfo getLiveDeviceByLiveIdAndDeviceSn(Integer liveId, String deviceSn) throws IOException;


    //endregion 直播消息结束


}


