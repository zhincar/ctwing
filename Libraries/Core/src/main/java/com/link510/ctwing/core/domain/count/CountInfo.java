/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.link510.ctwing.core.domain.count;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 统计信息
 *
 * @author cqnews
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountInfo implements Serializable {

    private static final long serialVersionUID = 1342598473711993882L;

    /**
     * 名称
     */
    private String name = "";


    /**
     * 统计和
     */
    private long count = 0;

    /**
     * 描述
     */
    private String desc = "";


    public CountInfo(String name, long count) {
        this.name = name;
        this.count = count;
        this.desc = name;
    }

    public CountInfo(Integer name, long count) {
        this.name = name + "";
        this.count = count;
        this.desc = name + "";
    }
}
