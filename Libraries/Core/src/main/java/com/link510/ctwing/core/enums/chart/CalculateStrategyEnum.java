package com.link510.ctwing.core.enums.chart;

import com.link510.ctwing.core.model.SelectListItem;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public enum CalculateStrategyEnum {
    /**
     * 范围图表数据取值方式
     */
    AVG_STRATEGY("保留平均值", 1, (i) -> i.parallelStream().collect(Collectors.averagingDouble(k -> k))),
    MEDIAN_STRATEGY("保留中位数", 2, (i) -> {
        //TODO
        return 4d;
    }),
    FRIST_STRATEGY("保留第一个", 3, i -> i.get(0)),
    LAST_STRATEGY("保留最后一个", 4, i -> i.get(i.size() - 1));
    @Getter
    private String name;
    @Getter
    private Integer code;
    @Getter
    private Function<List<Double>, Double> calculateUnit;

    CalculateStrategyEnum(String name, Integer code, Function<List<Double>, Double> calculateUnit) {
        this.name = name;
        this.calculateUnit = calculateUnit;
        this.code=code;
    }

    /**
     * 查找保留策略
     *
     * @param name 策略名
     * @return CalculateStrategyEnum
     */
    public static CalculateStrategyEnum findByName(String name) {
        return Arrays.stream(CalculateStrategyEnum.values()).parallel().filter(i -> i.getName().equals(name)).findAny().orElseGet(() -> AVG_STRATEGY);
    }

    /**
     * 查找保留策略
     *
     * @param code 策略码
     * @return CalculateStrategyEnum
     */
    public static CalculateStrategyEnum findByName(Integer code) {
        return Arrays.stream(CalculateStrategyEnum.values()).parallel().filter(i -> i.getCode().equals(code)).findAny().orElseGet(() -> AVG_STRATEGY);
    }

    /**
     * 获取Select
     *
     * @return List<SelectListItem>
     */
    public static List<SelectListItem> getSelectList() {
        return Arrays.stream(CalculateStrategyEnum.values()).map(i -> new SelectListItem(false, i.getName(), i.getCode() + "")).collect(toList());
    }

    /**
     * 获取选择Select
     *
     * @param code 保留策略码
     * @return List<SelectListItem>
     */
    public static List<SelectListItem> getSelectList(Integer code) {
        return Arrays.stream(CalculateStrategyEnum.values()).map(i -> new SelectListItem(i.getCode().equals(code), i.getName(), i.getCode() + "")).collect(toList());
    }
}
