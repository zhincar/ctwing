package com.link510.ctwing.core.data.rdbs.repository.messages;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.message.MessageRecordInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MessageRecordRepository extends BaseRepository<MessageRecordInfo, Integer> {

    /**
     * 获取消息的解析列表
     *
     * @param msgId 消息id
     * @return
     */
    @Transactional
    List<MessageRecordInfo> findByMsgId(Integer msgId);


    @Query(value = "SELECT DISTINCT new MessageRecordInfo(?1) FROM MessageRecordInfo info WHERE info.token = ?2 ")
    List<String> findCloumByToken(String field, String token);

    @Query("select info from MessageRecordInfo  info where info.deviceSN=?1 and info.protocol=?2 and info.key=?3 order by info.addTime desc")
    List<MessageRecordInfo> getRecordByDeviceSnAndProtocolAndKey2(String deviceSN, String protocol, String key2);

    @Query("select info from MessageRecordInfo info where info.deviceSN=?1 and info.protocol=?2 and info.key=?3 and info.addTime >?4 and info.addTime<?5 order by info.addTime desc ")
    List<MessageRecordInfo> getChartHistoryRecord(String deviceSN, String protocol, String key2, Integer startTime, Integer stopTime);

    @Query("select info from MessageRecordInfo info where info.deviceSN=?1 and info.key=?2 and info.value=?3")
    List<MessageRecordInfo> getMessageRecordByDeviceSnAndKeyValue(String dataTargetId, String key2, String value);
}
