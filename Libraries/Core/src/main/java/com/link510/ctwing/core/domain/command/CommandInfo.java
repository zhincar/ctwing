/*
 *  *  Copyright  2018.
 *  *  用于JAVA项目开发
 *  *  重庆英卡电子有限公司 版权所有
 *  *  Copyright  2018.  510Link.com Iniot All rights reserved.
 */

package com.link510.ctwing.core.domain.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 命令模型
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_command")
@DynamicUpdate
public class CommandInfo implements Serializable {


    private static final long serialVersionUID = -4436363355535325596L;
    /**
     * 命令Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commandid", nullable = false)
    private Integer commandId = 0;

    /**
     * 模块编号
     */
    @Column(name = "moduleid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer moduleId = 0;

    /**
     * 产品编号
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer productId = -1;


    /**
     * 命令名称
     */
    @Column(name = "name", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String name = "";

    /**
     * 命令(正则)
     */
    @Column(name = "command", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String command = "";

    /**
     * 命令描述
     */
    @Column(name = "description", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String description = "";

    /**
     * 命令过期时间NBIOT)
     */
    @Column(name = "expiretime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer expireTime = 0;

    /**
     * 最大重试次数)
     */
    @Column(name = "maxretransmit", nullable = false)
    @ColumnDefault(value = "5")
    private Integer maxRetransmit = 5;

    /**
     * 服务Id
     */
    @Column(name = "serviceid", nullable = false, length = 16)
    @ColumnDefault(value = "''")
    private String serviceId = "reportData";

    /**
     * 方法名(NBIOT)
     */
    @Column(name = "method", nullable = false, length = 16)
    @ColumnDefault(value = "''")
    private String method = "SEND_CMD_DATA";


}
