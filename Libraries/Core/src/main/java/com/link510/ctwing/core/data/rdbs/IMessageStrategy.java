package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.message.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;



public interface IMessageStrategy {

    //region 消息

    /**
     * 获得消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/

    long getMessageCount(Specification<MessageInfo> condition) throws IOException;


    /**
     * 创建一条消息数据
     *
     * @param messageInfo 消息模型
     * @return 返回创建信息
     **/

    MessageInfo createMessage(MessageInfo messageInfo) throws IOException;


    /**
     * 更新一条消息数据
     *
     * @param messageInfo 消息模型
     **/

    MessageInfo updateMessage(MessageInfo messageInfo) throws IOException;

    /**
     * 删除一条消息数据
     *
     * @param id 消息模型
     **/

    void deleteMessageById(int id) throws IOException;

    /**
     * 批量删除一批消息数据
     **/

    void deleteMessageByIdList(String idList) throws IOException;


    /**
     * 获得消息一条记录
     *
     * @param id deviceId
     * @return 返回一条MessageInfo
     **/

    MessageInfo getMessageById(int id) throws IOException;

    /**
     * 获取最后一条数据
     *
     * @param deviceSN 设备编号
     * @param token    token
     * @return
     */
    MessageInfo getLastMessageByTokenAndDeviceSN(String token, String deviceSN) throws IOException;

    /**
     * 获取最后一条数据
     *
     * @param deviceSN  设备编号
     * @param protocols 协议名称
     * @return
     */
    MessageInfo getLastMessageByDeviceSNAndProtocol(String deviceSN, Integer timestamp, String... protocols) throws IOException;

    /**
     * 获得消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageInfo
     **/

    List<MessageInfo> getMessageList(Specification<MessageInfo> condition, Sort sort) throws IOException;


    /**
     * 获得消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageInfo
     **/

    Page<MessageInfo> getMessageList(Integer pageSize, Integer pageNumber, Specification<MessageInfo> condition, Sort sort) throws IOException;


    //endregion 消息结束

    //region 心跳消息

    /**
     * 获得心跳消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getHeartMessageCount(Specification<HeartMessageInfo> condition) throws IOException;


    /**
     * 创建一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     * @return 返回创建信息
     **/
    HeartMessageInfo createHeartMessage(HeartMessageInfo heartmessageInfo) throws IOException;


    /**
     * 更新一条心跳消息数据
     *
     * @param heartmessageInfo 心跳消息模型
     **/
    HeartMessageInfo updateHeartMessage(HeartMessageInfo heartmessageInfo) throws IOException;

    /**
     * 删除一条心跳消息数据
     *
     * @param msgId 心跳消息模型
     **/
    void deleteHeartMessageByMsgId(Integer msgId) throws IOException;

    /**
     * 批量删除一批心跳消息数据
     **/
    void deleteHeartMessageByMsgIdList(String msgIdList) throws IOException;


    /**
     * 获得心跳消息一条记录
     *
     * @param msgId msgid
     * @return 返回一条HeartMessageInfo
     **/
    HeartMessageInfo getHeartMessageByMsgId(Integer msgId) throws IOException;

    /**
     * 获得心跳消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回HeartMessageInfo
     **/
    List<HeartMessageInfo> getHeartMessageList(Specification<HeartMessageInfo> condition, Sort sort) throws IOException;


    /**
     * 获得心跳消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回HeartMessageInfo
     **/
    Page<HeartMessageInfo> getHeartMessageList(Integer pageSize, Integer pageNumber, Specification<HeartMessageInfo> condition, Sort sort) throws IOException;


    //endregion 心跳消息结束

    //region 消息解析记录

    /**
     * 获得消息解析记录数量
     *
     * @param condition 条件
     * @return 返回数量
     **/

    long getMessageRecordCount(Specification<MessageRecordInfo> condition) throws IOException;


    /**
     * 创建一条消息解析记录数据
     *
     * @param messagerecordInfo 消息解析记录模型
     * @return 返回创建信息
     **/

    MessageRecordInfo createMessageRecord(MessageRecordInfo messagerecordInfo) throws IOException;


    /**
     * 更新一条消息解析记录数据
     *
     * @param messagerecordInfo 消息解析记录模型
     **/

    MessageRecordInfo updateMessageRecord(MessageRecordInfo messagerecordInfo) throws IOException;

    /**
     * 删除一条消息解析记录数据
     *
     * @param recordId 消息解析记录模型
     **/

    void deleteMessageRecordByRecordId(int recordId) throws IOException;

    /**
     * 批量删除一批消息解析记录数据
     **/

    void deleteMessageRecordByRecordIdList(String recordIdList) throws IOException;


    /**
     * 获得消息解析记录一条记录
     *
     * @param recordId recordid
     * @return 返回一条MessageRecordInfo
     **/

    MessageRecordInfo getMessageRecordByRecordId(int recordId) throws IOException;


    /**
     * 获得消息解析记录数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageRecordInfo
     **/

    List<MessageRecordInfo> getMessageRecordList(Specification<MessageRecordInfo> condition, Sort sort) throws IOException;


    /**
     * 获得消息解析记录数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageRecordInfo
     **/

    Page<MessageRecordInfo> getMessageRecordList(Integer pageSize, Integer pageNumber, Specification<MessageRecordInfo> condition, Sort sort) throws IOException;

    /**
     * 获取消息的解析列表
     *
     * @param msgId 消息id
     * @return
     */

    List<MessageRecordInfo> getMessageRecordList(Integer msgId) throws IOException;

    /**
     * 统计在指定的时间内的消息数目
     *
     * @param deviceSN     设备id
     * @param productId
     * @param intervalTime 时间段
     * @return
     */

    long countAllIntervalMessage(String deviceSN, Integer productId, String protocol, Integer intervalTime) throws IOException;

    /**
     * 统计token的心跳次数
     *
     * @param token        网关
     * @param type         消息类型
     * @param intervalTime 时间段
     */
    long countTokenIntervalMessage(String token, Integer type, Integer intervalTime) throws IOException;

    /**
     * 统计在指定的时间内的消息数目
     *
     * @param msgId
     * @param productId
     * @param deviceSN     设备编号
     * @param intervalTime 时间段
     * @return
     */
    long countAllIntervalMessage(Integer msgId, Integer productId, String deviceSN, String protocol, Integer intervalTime) throws IOException;

    /**
     * 统计警告设备消息
     *
     * @return long
     */
    long countWarningAllMessage() throws IOException;

    /**
     * 让消息失效
     *
     * @param msgId 消息id
     */
    void lose(Integer msgId) throws IOException;

    /**
     * 通过tokem查询消息列表
     *
     * @param token
     * @return
     */
    List<MessageInfo> getMessageByToken(String token) throws IOException;

    /**
     * 更新消息图片
     *
     * @param msgId  消息id
     * @param litpic 图片
     */
    void updateMessageLitpic(Integer msgId, String litpic) throws IOException;

    /**
     * 获取最后一包心跳包时间
     *
     * @param token
     * @return
     */
    Integer getLastHeartMessageTimeByToken(String token) throws IOException;


    /**
     * 获取最后一包数据包时间
     *
     * @param deviceSN 设备编号
     * @return
     */
    Integer getLastDataMessageTimeByDeviceSN(String deviceSN) throws IOException;
    //endregion 消息解析记录结束



    //region 图片消息

    /**
     * 获得图片消息数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getMessageImageCount(Specification<MessageImageInfo> condition) throws IOException;


    /**
     * 创建一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     * @return 返回创建信息
     **/
    MessageImageInfo createMessageImage(MessageImageInfo messageimageInfo) throws IOException;


    /**
     * 更新一条图片消息数据
     *
     * @param messageimageInfo 图片消息模型
     **/
    MessageImageInfo updateMessageImage(MessageImageInfo messageimageInfo) throws IOException;

    /**
     * 删除一条图片消息数据
     *
     * @param imgId 图片消息模型
     **/
    void deleteMessageImageByImgId(Integer imgId) throws IOException;

    /**
     * 批量删除一批图片消息数据
     **/
    void deleteMessageImageByImgIdList(String imgIdList) throws IOException;


    /**
     * 获得图片消息一条记录
     *
     * @param imgId imgid
     * @return 返回一条MessageImageInfo
     **/
    MessageImageInfo getMessageImageByImgId(Integer imgId) throws IOException;

    /**
     * 获得图片消息数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回MessageImageInfo
     **/
    List<MessageImageInfo> getMessageImageList(Specification<MessageImageInfo> condition, Sort sort) throws IOException;


    /**
     * 获得图片消息数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回MessageImageInfo
     **/
    Page<MessageImageInfo> getMessageImageList(Integer pageSize, Integer pageNumber, Specification<MessageImageInfo> condition, Sort sort) throws IOException;

    /**
     * 更新图片库状态
     *
     * @param state     类型
     * @param type      类型
     * @param deviceSN  设备编号
     * @param shortTime 分组时间
     */
     void updateMessageImageSate(Integer state, Integer type, String deviceSN, String shortTime) throws IOException;

    //endregion

    /**
     * 获得输出日志数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getOutputCount(Specification<OutputInfo> condition) throws IOException;

    /**
     * 创建一条输出日志数据
     *
     * @param outputInfo 输出日志模型
     * @return 返回创建信息
     **/
    OutputInfo createOutput(OutputInfo outputInfo) throws IOException;

    /**
     * 更新一条输出日志数据
     *
     * @param outputInfo 输出日志模型
     **/
    OutputInfo updateOutput(OutputInfo outputInfo) throws IOException;

    /**
     * 删除一条输出日志数据
     *
     * @param logId 输出日志模型
     **/
    void deleteOutputByLogId(Integer logId) throws IOException;

    /**
     * 批量删除一批输出日志数据
     **/
    void deleteOutputByLogIdList(String logIdList) throws IOException;

    /**
     * 获得输出日志一条记录
     *
     * @param logId logid
     * @return 返回一条OutputInfo
     **/
    OutputInfo getOutputByLogId(Integer logId) throws IOException;


    /**
     * 获得输出日志数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回OutputInfo
     **/
    List<OutputInfo> getOutputList(Specification<OutputInfo> condition, Sort sort) throws IOException;

    /**
     * 获得输出日志数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回OutputInfo
     **/
    Page<OutputInfo> getOutputList(Integer pageSize, Integer pageNumber, Specification<OutputInfo> condition, Sort sort) throws IOException;


}


