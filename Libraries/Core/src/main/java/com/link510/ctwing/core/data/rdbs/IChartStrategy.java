package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.charts.ChartGroupInfo;
import com.link510.ctwing.core.domain.charts.ChartGroupTemplateInfo;
import com.link510.ctwing.core.domain.charts.ChartItemInfo;
import com.link510.ctwing.core.domain.charts.ChartTemplateInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

public interface IChartStrategy {

    //region chartitem

    /**
     * 获得chartitem数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getchartitemCount(Specification<ChartItemInfo> condition) throws IOException;


    /**
     * 创建一条chartitem数据
     *
     * @param ChartItemInfo chartitem模型
     * @return 返回创建信息
     **/
    ChartItemInfo createchartitem(ChartItemInfo ChartItemInfo) throws IOException;


    /**
     * 更新一条chartitem数据
     *
     * @param chartItemInfo chartitem模型
     **/
    ChartItemInfo updatechartitem(ChartItemInfo chartItemInfo) throws IOException;

    /**
     * 删除一条chartitem数据
     *
     * @param itemId chartitem模型
     **/
    void deletechartitemByItemId(int itemId) throws IOException;

    /**
     * 批量删除一批chartitem数据
     **/
    void deletechartitemByItemIdList(String itemIdList) throws IOException;


    /**
     * 获得chartitem一条记录
     *
     * @param itemId itemid
     * @return 返回一条ChartItemInfo
     **/
    ChartItemInfo getchartitemByItemId(int itemId) throws IOException;

    /**
     * 获得chartitem数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ChartItemInfo
     **/
    List<ChartItemInfo> getchartitemList(Specification<ChartItemInfo> condition, Sort sort) throws IOException;


    /**
     * 获得chartitem数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ChartItemInfo
     **/
    Page<ChartItemInfo> getchartitemList(Integer pageSize, Integer pageNumber, Specification<ChartItemInfo> condition, Sort sort) throws IOException;


    //endregion chartitem结束

    //region charttemplate

    /**
     * 获得charttemplate数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getcharttemplateCount(Specification<ChartTemplateInfo> condition) throws IOException;


    /**
     * 创建一条charttemplate数据
     *
     * @param ChartTemplateInfo charttemplate模型
     * @return 返回创建信息
     **/
    ChartTemplateInfo createcharttemplate(ChartTemplateInfo ChartTemplateInfo) throws IOException;


    /**
     * 更新一条charttemplate数据
     *
     * @param ChartTemplateInfo charttemplate模型
     **/
    ChartTemplateInfo updatecharttemplate(ChartTemplateInfo ChartTemplateInfo) throws IOException;

    /**
     * 删除一条charttemplate数据
     *
     * @param chartId charttemplate模型
     **/
    void deletecharttemplateByChartId(int chartId) throws IOException;

    /**
     * 批量删除一批charttemplate数据
     **/
    void deletecharttemplateByChartIdList(String chartIdList) throws IOException;


    /**
     * 获得charttemplate一条记录
     *
     * @param chartId chartid
     * @return 返回一条ChartTemplateInfo
     **/
    ChartTemplateInfo getcharttemplateByChartId(int chartId) throws IOException;

    /**
     * 获得charttemplate数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ChartTemplateInfo
     **/
    List<ChartTemplateInfo> getcharttemplateList(Specification<ChartTemplateInfo> condition, Sort sort) throws IOException;


    /**
     * 获得charttemplate数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ChartTemplateInfo
     **/
    Page<ChartTemplateInfo> getcharttemplateList(Integer pageSize, Integer pageNumber, Specification<ChartTemplateInfo> condition, Sort sort) throws IOException;

    /**
     * 停用所有该产品图表模板
     *
     * @param productId
     * @throws IOException
     */
    void stopAllDefault(Integer productId) throws IOException;


    //endregion charttemplate结束
    //region chartgrouptemplate

    /**
     * 获得chartgrouptemplate数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getchartgrouptemplateCount(Specification<ChartGroupTemplateInfo> condition) throws IOException;


    /**
     * 创建一条chartgrouptemplate数据
     *
     * @param chartgrouptemplateinfo chartgrouptemplate模型
     * @return 返回创建信息
     **/
    ChartGroupTemplateInfo createchartgrouptemplate(ChartGroupTemplateInfo chartgrouptemplateinfo) throws IOException;


    /**
     * 更新一条chartgrouptemplate数据
     *
     * @param chartgrouptemplateinfo chartgrouptemplate模型
     **/
    ChartGroupTemplateInfo updatechartgrouptemplate(ChartGroupTemplateInfo chartgrouptemplateinfo) throws IOException;

    /**
     * 删除一条chartgrouptemplate数据
     *
     * @param id chartgrouptemplate模型
     **/
    void deletechartgrouptemplateById(int id) throws IOException;

    /**
     * 批量删除一批chartgrouptemplate数据
     **/
    void deletechartgrouptemplateByIdList(String idList) throws IOException;


    /**
     * 获得chartgrouptemplate一条记录
     *
     * @param id id
     * @return 返回一条ChartGroupTemplateInfo
     **/
    ChartGroupTemplateInfo getchartgrouptemplateById(int id) throws IOException;

    /**
     * 获得chartgrouptemplate数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ChartGroupTemplateInfo
     **/
    List<ChartGroupTemplateInfo> getchartgrouptemplateList(Specification<ChartGroupTemplateInfo> condition, Sort sort) throws IOException;


    /**
     * 获得chartgrouptemplate数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ChartGroupTemplateInfo
     **/
    Page<ChartGroupTemplateInfo> getchartgrouptemplateList(Integer pageSize, Integer pageNumber, Specification<ChartGroupTemplateInfo> condition, Sort sort) throws IOException;


    //endregion chartgrouptemplate结束

    //region chartgroup

    /**
     * 获得chartgroup数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    long getchartgroupCount(Specification<ChartGroupInfo> condition) throws IOException;


    /**
     * 创建一条chartgroup数据
     *
     * @param chartgroupinfo chartgroup模型
     * @return 返回创建信息
     **/
    ChartGroupInfo createchartgroup(ChartGroupInfo chartgroupinfo) throws IOException;


    /**
     * 更新一条chartgroup数据
     *
     * @param chartgroupinfo chartgroup模型
     **/
    ChartGroupInfo updatechartgroup(ChartGroupInfo chartgroupinfo) throws IOException;

    /**
     * 删除一条chartgroup数据
     *
     * @param gourpid chartgroup模型
     **/
    void deletechartgroupByGourpid(int gourpid) throws IOException;

    /**
     * 批量删除一批chartgroup数据
     **/
    void deletechartgroupByGourpidList(String gourpidList) throws IOException;


    /**
     * 获得chartgroup一条记录
     *
     * @param gourpid gourpid
     * @return 返回一条ChartGroupInfo
     **/
    ChartGroupInfo getchartgroupByGourpid(int gourpid) throws IOException;

    /**
     * 获得chartgroup数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回ChartGroupInfo
     **/
    List<ChartGroupInfo> getchartgroupList(Specification<ChartGroupInfo> condition, Sort sort) throws IOException;


    /**
     * 获得chartgroup数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回ChartGroupInfo
     **/
    Page<ChartGroupInfo> getchartgroupList(Integer pageSize, Integer pageNumber, Specification<ChartGroupInfo> condition, Sort sort) throws IOException;

    /**
     * 查询模板支持参数
     *
     * @param chartId
     * @return
     * @throws IOException
     */
    List<ChartItemInfo> getchartitemByChartId(Integer chartId) throws IOException;

    /**
     * 删除模板包含参数
     *
     * @param chartId
     * @throws IOException
     */
    void deletechartitemByChartId(int chartId) throws IOException;

    /**
     * 删除图表组与模板关联
     *
     * @param groupId
     * @throws IOException
     */
    void deletechartgrouptemplateByGroupId(Integer groupId) throws IOException;

    /**
     * 查询所有图表模板
     *
     * @return
     * @throws IOException
     */
    List<ChartTemplateInfo> findAllChartTemplate() throws IOException;

    /**
     * 图表组关联图表ID
     *
     * @param groupId
     * @return
     */
    List<Integer> findChartTemplateIdsByGroupId(Integer groupId) throws IOException;

    /**
     * 查找模板与模板组关联
     *
     * @param groupId
     * @param chartId
     * @return
     * @throws IOException
     */
    ChartGroupTemplateInfo getChartGroupTemplateByGroupAndChartId(Integer groupId, Integer chartId) throws IOException;

    /**
     * 产品适用的模板组
     *
     * @param productId
     * @return
     * @throws IOException
     */
    List<ChartGroupInfo> getChartGroupByProductId(Integer productId) throws IOException;

    /**
     * 查询图表组所属图表
     *
     * @param groupId
     * @return
     * @throws IOException
     */
    List<Integer> getChartGroupTemplateByGroupId(Integer groupId) throws IOException;


    //endregion chartgroup结束


}
