package com.link510.ctwing.core.domain.message;

import com.alibaba.fastjson.JSON;
import com.link510.ctwing.core.exption.message.ErrorMessageException;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * 消息表
 *
 * @author cqnews
 */
@Entity
@DynamicUpdate
@Table(name = "w_messages")
public class MessageInfo extends SupperMessageInfo implements Serializable {

    private static final long serialVersionUID = 92617693659611319L;

    /**
     * 消息的组装
     *
     * @param content 内容
     * @return 消息
     */
    public static MessageInfo of(String content) throws ErrorMessageException {

        try {
            return JSON.parseObject(content, MessageInfo.class);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ErrorMessageException(ex, "消息组装失败");
        }
    }

    public MessageInfo() {
    }

    public MessageInfo(String uid, Integer agentId, String unitCode, String deviceSN, Integer productId, Integer productType, String protocol, Integer msgLevel, String protodesc, String token, String channelId, String ip, Integer port, Integer pluginId, Integer type, Integer state, String message, String content, String deviceName, String deviceCode, String topic, Integer timestamp, Integer lose) {
        super(uid, agentId, unitCode, deviceSN, productId, productType, protocol, msgLevel, protodesc, token, channelId, ip, port, pluginId, type, state, message, content, deviceName, deviceCode, topic, timestamp, lose);
    }


    @Override
    public String toString() {
        return "MessageInfo{" +
                "msgId=" + msgId +
                ", uid='" + uid + '\'' +
                ", agentId=" + agentId +
                ", unitCode='" + unitCode + '\'' +
                ", deviceSN='" + deviceSN + '\'' +
                ", productId=" + productId +
                ", productType=" + productType +
                ", protocol='" + protocol + '\'' +
                ", msgLevel=" + msgLevel +
                ", protodesc='" + protodesc + '\'' +
                ", token='" + token + '\'' +
                ", channelId='" + channelId + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", type=" + type +
                ", state=" + state +
                ", message='" + message + '\'' +
                ", content='" + content + '\'' +
                ", litpic='" + litpic + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", deviceCode='" + deviceCode + '\'' +
                ", topic='" + topic + '\'' +
                ", timestamp=" + timestamp +
                ", lose=" + lose +
                '}';
    }
}
