package com.link510.ctwing.core.data.rdbs;

import com.link510.ctwing.core.domain.device.DeviceActionInfo;
import com.link510.ctwing.core.domain.device.DeviceActionParamInfo;
import com.link510.ctwing.core.domain.device.GroupInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.io.IOException;
import java.util.List;

@SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
public interface IGroupStrategy {


    //region 分组

    /**
     * 获得分组数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getGroupCount(Specification<GroupInfo> condition) throws IOException;


    /**
     * 创建一条分组数据
     *
     * @param groupInfo 分组模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    GroupInfo createGroup(GroupInfo groupInfo) throws IOException;


    /**
     * 更新一条分组数据
     *
     * @param groupInfo 分组模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    GroupInfo updateGroup(GroupInfo groupInfo) throws IOException;

    /**
     * 删除一条分组数据
     *
     * @param groupId 分组模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteGroupByGroupId(int groupId) throws IOException;

    /**
     * 批量删除一批分组数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteGroupByGroupIdList(String groupIdList) throws IOException;


    /**
     * 获得分组一条记录
     *
     * @param groupId groupid
     * @return 返回一条GroupInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    GroupInfo getGroupByGroupId(int groupId) throws IOException;

    /**
     * 获得分组数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回GroupInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<GroupInfo> getGroupList(Specification<GroupInfo> condition, Sort sort) throws IOException;


    /**
     * 获得分组数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回GroupInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<GroupInfo> getGroupList(Integer pageSize, Integer pageNumber, Specification<GroupInfo> condition, Sort sort) throws IOException;


    //endregion 分组结束

    //region 动作参数

    /**
     * 获得动作参数数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getDeviceActionParamCount(Specification<DeviceActionParamInfo> condition) throws IOException;


    /**
     * 创建一条动作参数数据
     *
     * @param deviceactionparamInfo 动作参数模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    DeviceActionParamInfo createDeviceActionParam(DeviceActionParamInfo deviceactionparamInfo) throws IOException;


    /**
     * 更新一条动作参数数据
     *
     * @param deviceactionparamInfo 动作参数模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    DeviceActionParamInfo updateDeviceActionParam(DeviceActionParamInfo deviceactionparamInfo) throws IOException;

    /**
     * 删除一条动作参数数据
     *
     * @param paramId 动作参数模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteDeviceActionParamByParamId(int paramId) throws IOException;

    /**
     * 批量删除一批动作参数数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteDeviceActionParamByParamIdList(String paramIdList) throws IOException;


    /**
     * 获得动作参数一条记录
     *
     * @param paramId paramid
     * @return 返回一条DeviceActionParamInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    DeviceActionParamInfo getDeviceActionParamByParamId(int paramId) throws IOException;

    /**
     * 获得动作参数数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回DeviceActionParamInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<DeviceActionParamInfo> getDeviceActionParamList(Specification<DeviceActionParamInfo> condition, Sort sort) throws IOException;


    /**
     * 获得动作参数数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回DeviceActionParamInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<DeviceActionParamInfo> getDeviceActionParamList(Integer pageSize, Integer pageNumber, Specification<DeviceActionParamInfo> condition, Sort sort) throws IOException;


    //endregion 动作参数结束

    //region 设备发出动作

    /**
     * 获得设备发出动作数量
     *
     * @param condition 条件
     * @return 返回数量
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    long getDeviceActionCount(Specification<DeviceActionInfo> condition) throws IOException;


    /**
     * 创建一条设备发出动作数据
     *
     * @param deviceactionInfo 设备发出动作模型
     * @return 返回创建信息
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    DeviceActionInfo createDeviceAction(DeviceActionInfo deviceactionInfo) throws IOException;


    /**
     * 更新一条设备发出动作数据
     *
     * @param deviceactionInfo 设备发出动作模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    DeviceActionInfo updateDeviceAction(DeviceActionInfo deviceactionInfo) throws IOException;

    /**
     * 删除一条设备发出动作数据
     *
     * @param actionId 设备发出动作模型
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteDeviceActionByActionId(int actionId) throws IOException;

    /**
     * 批量删除一批设备发出动作数据
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    void deleteDeviceActionByActionIdList(String actionIdList) throws IOException;


    /**
     * 获得设备发出动作一条记录
     *
     * @param actionId actionid
     * @return 返回一条DeviceActionInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    DeviceActionInfo getDeviceActionByActionId(int actionId) throws IOException;

    /**
     * 获得设备发出动作数据列表
     *
     * @param condition 条件
     * @param sort      排序
     * @return 返回DeviceActionInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<DeviceActionInfo> getDeviceActionList(Specification<DeviceActionInfo> condition, Sort sort) throws IOException;


    /**
     * 获得设备发出动作数据列表
     *
     * @param pageSize   每页数
     * @param pageNumber 当前页数
     * @param condition  条件
     * @param sort       排序
     * @return 返回DeviceActionInfo
     **/
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Page<DeviceActionInfo> getDeviceActionList(Integer pageSize, Integer pageNumber, Specification<DeviceActionInfo> condition, Sort sort) throws IOException;

    /**
     * 获取设备的联系动作
     *
     * @param deviceId 设备id
     * @param protocol
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<DeviceActionInfo> getDeviceActionList(Integer deviceId, String protocol) throws IOException;


    /**
     * 获取设备的关联动作
     *
     * @param groupId 分组id
     * @return
     */
    List<DeviceActionInfo> getDeviceActionListByGroupId(Integer groupId) throws IOException;


    /**
     * 获得动作参数数据列表
     *
     * @param actionId 动作id
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<DeviceActionParamInfo> getDeviceActionParamList(Integer actionId) throws IOException;

    /**
     * 获取分组列表条件信息
     *
     * @param name 分组名称
     * @param uid
     * @return
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    Specification<GroupInfo> getGroupListCondition(String name, String uid) throws IOException;


    /**
     * 获取设备id的分组列表信息
     *
     * @param deviceId 设备id
     * @return 分组列表
     */
    @SuppressWarnings("AlibabaAbstractMethodOrInterfaceMethodMustUseJavadoc")
    List<GroupInfo> getGroupListByDeviceId(Integer deviceId) throws IOException;

    /**
     * 获取设备id的分组列表信息
     *
     * @param deviceId 设备id
     * @param uid      uid
     * @return 分组列表
     */
    List<GroupInfo> getGroupListByDeviceIdAndUid(Integer deviceId, String uid) throws IOException;

    /**
     * 通过产品类型获取分组列表
     *
     * @param productId 产品id
     * @return
     */
    List<GroupInfo> getGroupListByProductId(Integer productId) throws IOException;

    /**
     * 通过产品类型获取分组列表
     *
     * @param productId 产品id
     * @return
     */
    List<GroupInfo> getAgentGroupListByProductId(Integer productId, Integer agentId) throws IOException;

    /**
     * 通过产品类型获取分组列表
     *
     * @param productId 产品id
     * @param uid       uid
     * @return
     */
    List<GroupInfo> getGroupListByProductIdAndUid(Integer productId, String uid) throws IOException;
    //endregion 设备发出动作结束
}
