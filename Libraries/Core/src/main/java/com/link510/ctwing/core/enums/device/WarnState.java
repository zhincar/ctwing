package com.link510.ctwing.core.enums.device;


import com.link510.ctwing.core.helper.TypeHelper;
import com.link510.ctwing.core.model.SelectListItem;

import java.util.ArrayList;
import java.util.List;

public enum WarnState {


    /**
     * 危险等级
     */
    Normal(0, "正常", "green"),
    Remind(1, "预警", "blue"),
    Warning(2, "警告", "orange"),
    Danger(3, "危险", "red");

    private Integer level;

    private String name;

    private String color;

    WarnState(Integer level, String name, String color) {
        this.level = level;
        this.name = name;
        this.color = color;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "WarnState{" +
                "level=" + level +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    /**
     * 获取中文名称
     *
     * @param level 等级
     */
    public String getName(Integer level) {

        for (WarnState state : values()) {
            if (level.equals(state.getLevel())) {
                return state.getName();
            }
        }
        return Normal.getName();
    }

    /**
     * 获取警告颜色
     *
     * @param level 等级
     */
    public String getColor(Integer level) {

        for (WarnState state : values()) {
            if (level.equals(state.getLevel())) {
                return state.getColor();
            }
        }
        return Normal.getColor();
    }

    /**
     * 获取警告等级
     *
     * @param level 等级
     * @return
     */
    public WarnState getWarnSate(Integer level) {

        for (WarnState state : values()) {
            if (level.equals(state.getLevel())) {
                return state;
            }
        }
        return Normal;
    }

    /**
     * 获取枚举的列表
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem() {
        return getSelectListItem(1);
    }


    /**
     * 获取枚举的列表
     * index 默认选择项目
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem(Integer level) {
        List<SelectListItem> selectListItemList = new ArrayList<SelectListItem>();

        for (WarnState state : values()) {

            SelectListItem item = new SelectListItem();

            item.setText(state.getName());
            item.setValue(TypeHelper.intToString(state.getLevel()));

            if (state.getLevel().equals(level)) {
                item.setSelected(true);
            }

            selectListItemList.add(item);
        }

        return selectListItemList;
    }


}
