package com.link510.ctwing.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 协议和数据
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProtocolAndDataModel implements Serializable {

    /**
     * 协议
     */
    private String protocol = "";

    /**
     * 数据
     */
    private String data = "";
}
