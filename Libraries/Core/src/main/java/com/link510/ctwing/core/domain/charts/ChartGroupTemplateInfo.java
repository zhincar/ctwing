package com.link510.ctwing.core.domain.charts;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author cqnews
 */ //chartgrouptemplate
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicUpdate
@Entity
@Table(name = "w_chart_group_template")
public class ChartGroupTemplateInfo implements Serializable {


    private static final long serialVersionUID = 4488210713209125893L;
    /**
     * ID
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id = 0;
    /**
     * 组ID
     */
    @Column(name = "groupid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer groupId = 0;
    /**
     * 模板ID
     */
    @Column(name = "chartid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer chartId = 0;

    public ChartGroupTemplateInfo(Integer groupId, Integer chartId) {
        this.groupId = groupId;
        this.chartId = chartId;
    }
}