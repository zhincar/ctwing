package com.link510.ctwing.core.domain.message;


import com.link510.ctwing.core.helper.UnixTimeHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 图片组包
 *
 * @author cqnews
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "w_messages_image")
public class MessageImageInfo implements Serializable {

    private static final long serialVersionUID = -6971146900573508291L;

    /**
     * 消息id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "imgid", nullable = false)
    private Integer imgId = -1;


    /**
     * 图片解析类型
     */
    @Column(name = "type", nullable = false)
    @ColumnDefault(value = "0")
    private Integer type = 0;

    /**
     * 设备编号
     */
    @Column(name = "devicesn", nullable = false, length = 512)
    @ColumnDefault(value = "''")
    private String deviceSN = "";

    /**
     * 数据
     */
    @Column(name = "data", nullable = false, length = 512)
    @ColumnDefault(value = "''")
    private String data = "";

    /**
     * 发包时间
     */
    @Column(name = "shorttime", nullable = false)
    @ColumnDefault(value = "''")
    private String shortTime = "";

    /**
     * 包的顺序
     */
    @Column(name = "sort", nullable = false)
    @ColumnDefault(value = "0")
    private Integer sort = 0;

    /**
     * 包的长度
     */
    @Column(name = "total", nullable = false)
    @ColumnDefault(value = "0")
    private Integer total = 0;

    /**
     * 时间戳
     */
    @Column(name = "timestamp", nullable = false)
    @ColumnDefault(value = "0")
    private Integer timestamp = 0;


    /**
     * 是否过期
     */
    @Column(name = "state", nullable = false)
    @ColumnDefault(value = "0")
    private Integer state = 0;


    public MessageImageInfo(Integer type, String deviceSN, String data, String shortTime, Integer sort, Integer total) {
        this.type = type;
        this.deviceSN = deviceSN;
        this.data = data;
        this.shortTime = shortTime;
        this.sort = sort;
        this.total = total;
        this.timestamp = UnixTimeHelper.getUnixTimeStamp();
        this.state = 0;
    }
}
