/*
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.link510.ctwing.core.domain.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 命令模型
 *
 * @author cqnews
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "w_command_records")
public class CommandRecordInfo implements Serializable {


    private static final long serialVersionUID = -4436363355535325596L;

    /**
     * 命令Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "recordId", nullable = false)
    private Integer recordId = 0;

    /**
     * 命令Id
     */
    @Column(name = "commandid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer commandId = 0;

    /**
     * 模块编号
     */
    @Column(name = "moduleid", nullable = false)
    @ColumnDefault(value = "0")
    private Integer moduleId = 0;

    /**
     * 产品ID
     */
    @Column(name = "productid", nullable = false)
    @ColumnDefault(value = "-1")
    private Integer productId = -1;

    /**
     * 设备编号
     */
    @Column(name = "devicesn", nullable = false)
    @ColumnDefault(value = "''")
    private String deviceSN = "";

    /**
     * token,nb设备token为相应平台设备ID
     */
    @Column(name = "token", nullable = false)
    @ColumnDefault(value = "''")
    private String token = "";

    /**
     * 命令(正则)
     */
    @Column(name = "content", nullable = false, length = 500)
    @ColumnDefault(value = "''")
    private String content = "";

    /**
     * 发送时间)
     */
    @Column(name = "sendtime", nullable = false)
    @ColumnDefault(value = "0")
    private Integer sendTime = 0;

    /**
     * 平台命令ID
     */
    @Column(name = "platformid", nullable = false, length = 120)
    @ColumnDefault(value = "''")
    private String platformId = "";

    /**
     * 命令下发状态
     * @see com.link510.iniot.core.enums.nb.NbCommandStatusEnum
     */
    @Column(name = "commandstatus", nullable = false, length = 8)
    @ColumnDefault(value = "2")
    private Integer commandStatus = 2;

}
