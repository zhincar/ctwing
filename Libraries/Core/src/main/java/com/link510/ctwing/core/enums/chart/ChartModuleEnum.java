package com.link510.ctwing.core.enums.chart;

import com.link510.ctwing.core.model.SelectListItem;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public enum ChartModuleEnum {
    /**
     * 图表模块
     */
    CHART_MESSAGE(1, "消息"),
    CHART_HEART(2, "心跳"),
    CHART_COMMAND(3, "命令"),
    CHART_USER(4, "用户"),
    CHART_OTHER(5, "其他类型");
    @Getter
    private Integer id;
    @Getter
    private String name;

    ChartModuleEnum(Integer id, String name) {
        this.id = id;
        this.name = name;
    }


    public static List<SelectListItem> getSelectList(Integer id) {
        return Arrays.stream(ChartModuleEnum.values()).map(i -> new SelectListItem(i.getId().intValue() == id, i.getName(), i.getId() + "")).collect(toList());
    }
}
