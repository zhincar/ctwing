package com.link510.ctwing.core.enums.message;


import com.link510.ctwing.core.model.SelectListItem;

import java.util.ArrayList;
import java.util.List;

public enum MsgPlugin {

    /**
     * 消息类型
     */
    OPen(0, "开放接口"),
    GateWay(1, "公司网关"),
    SMSGateWay(2, "短信网"),
    TangTian(3, "唐天科技"),
    NBIoT(4, "NBIot"),
    NewNBIoT(5, "New NBIoT");


    MsgPlugin(Integer index, String name) {
        this.index = index;
        this.name = name;
    }


    private Integer index;

    private String name;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MsgPlugin{" +
                "index=" + index +
                ", name='" + name + '\'' +
                '}';
    }

    /**
     * 通过index获取枚举名称
     *
     * @param index
     * @return
     */
    public String getName(Integer index) {

        MsgPlugin[] msgPlugins = MsgPlugin.values();
        for (MsgPlugin plugin : msgPlugins) {
            if (plugin.getIndex().equals(index)) {
                return plugin.getName();
            }
        }

        return null;
    }

    /**
     * 通过index获取枚举
     *
     * @param index
     * @return
     */
    public static MsgPlugin getBankAction(Integer index) {

        MsgPlugin[] msgPlugins = MsgPlugin.values();
        for (MsgPlugin plugin : msgPlugins) {
            if (plugin.getIndex().equals(index)) {
                return plugin;
            }
        }
        return null;
    }

    /**
     * 获取枚举的列表
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem() {
        return getSelectListItem(1);
    }


    /**
     * 获取枚举的列表
     * index 默认选择项目
     *
     * @return
     */
    public static List<SelectListItem> getSelectListItem(Integer index) {
        List<SelectListItem> selectListItemList = new ArrayList<SelectListItem>();

        MsgPlugin[] msgPlugins = MsgPlugin.values();

        for (MsgPlugin plugin : msgPlugins) {

            SelectListItem item = new SelectListItem();

            item.setText(plugin.getName());
            item.setValue(plugin.index.toString());

            if (plugin.getIndex().equals(index)) {
                item.setSelected(true);
            }

            selectListItemList.add(item);
        }

        return selectListItemList;
    }
}
