package com.link510.ctwing.core.enums.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum FormatType {

    /**
     * 数据格式
     */
    formdta(0),
    json(1),
    onenet(2);

    @Getter
    private Integer code;


}
