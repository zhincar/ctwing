package com.link510.ctwing.core.data.rdbs.repository.base;

import com.link510.ctwing.core.data.rdbs.repository.BaseRepository;
import com.link510.ctwing.core.domain.base.BannedIPInfo;

/**
 * @author cqnews
 */
public interface BannedIPRepository extends BaseRepository<BannedIPInfo, Integer> {
}
